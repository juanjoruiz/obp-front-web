(function(){
  var app = angular.module('isbanWebApp');

  app.factory('aplicacion', aplicacion);

  function aplicacion ($http, localStorageService, $q, $rootScope){
    let aplicacion = {};
    console.log("aplicacion ya inicio");

     aplicacion.getConf = function() {
       var sendData = {
         url: '/personas/config.json',
         method: 'GET',
         headers: {
           'Content-Type': 'application/json;charset=UTF-8'
         }
       }
      return $http(sendData);


    }

    aplicacion.getVersion = function() {
      let version;
      let defer = $q.defer();
      setTimeout(function(){
          version = localStorageService.get('configuracion').version; // || $rootScope.configuracion.version;
          defer.resolve(version);
      },100);

      return defer.promise;
    }

    aplicacion.getEnv = function() {
      let env;
      let defer = $q.defer();
      setTimeout(function(){
          env = localStorageService.get('configuracion').ENV_VAR; // || $rootScope.configuracion.version;
          console.log(defer.resolve(env));
          defer.resolve(env);
      },100);
      return defer.promise;
    }

    aplicacion.universidadesBloqueado = function(){
      let bloqueado = false;
      let defer = $q.defer();
      setTimeout(function(){
          bloqueado = localStorageService.get('configuracion').flujo_universidades_bloqueado;
          defer.resolve(bloqueado);
      },10);
      return defer.promise;
    }



    return aplicacion;
  }

  aplicacion.$inject = ['$http', 'localStorageService', '$q'];
})();
