(function(){
  angular.module('isbanWebApp')
    .service('alertaService', alertaService);

  function alertaService($uibModal, $location, localStorageService){

    let modalDefaults = {
      backdrop: true,
      keyboard: false,
      modalFade: true,
      templateUrl: 'js/shared/alerta/alerta.view.html'
    }

    let modalOptions = {
      closeButtonText: '',
      actionButtonText: '',
      headerText: 'Título de mensaje',
      bodyText: 'Texto de cuerpo',
      uiref: 'home'
    }



    this.showModal = function(customModalDefaults, customModalOptions){
      if(!customModalDefaults) customModalDefaults = {};
      customModalDefaults.backdrop = 'static';
      return this.show(customModalDefaults, customModalOptions);
    };

    this.show = function(customModalDefaults, customModalOptions){
      let tempModalDefaults = {};
      let tempModalOptions = {};

      angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);
      angular.extend(tempModalOptions, modalOptions, customModalOptions);

      if(!tempModalDefaults.controller){
        tempModalDefaults.controller = function($scope, $uibModalInstance){
          $scope.modalOptions = tempModalOptions;
          $scope.modalOptions.ok = function(result){
            $uibModalInstance.close(result);
          };
          $scope.modalOptions.close = function(result){
            $uibModalInstance.dismiss('cancel');
            return true;
          };
        }
      }
      //
      return $uibModal.open(tempModalDefaults).result;
    };

  }

  alertaService.$inject = ["$uibModal", "$location", "localStorageService"];
})();
