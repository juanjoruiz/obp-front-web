(function(){
  angular.module('isbanWebApp')
    .factory('httpSpinnerInterceptor', httpSpinnerInterceptor);

    function httpSpinnerInterceptor($q, $rootScope, $log){
      let numLoadings = 0;
      return{
        request: function(config){
          numLoadings++;
          //Show loader
          $rootScope.$broadcast("loader_show");
          return config || $q.when(config);
        },
        response: function(response){
          if((--numLoadings) === 0){
            //Hide loader
            $rootScope.$broadcast("loader_hide");
          }
          if(response.status == 200){
            $rootScope.$broadcast(response.status);
          }
          return response || $q.when(response);
        },
        responseError: function(response){
          if(!(--numLoadings)){
            //Hide loader
            $rootScope.$broadcast("loader_hide");
          }

          $rootScope.$broadcast(response.status);
          return $q.reject(response);
        }
      }
    }
})();
