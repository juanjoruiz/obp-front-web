var app = angular.module('isbanWebApp');

app.service('modalService', ['$uibModal', '$location', function($uibModal, $location){
  var modalDefaults = {
    backdrop: true,
    keyboard: true,
    modalFade: true,
    templateUrl:'js/shared/modal/modal.view.html'
  };

  var modalOptions = {
    closeButtonText: '',
    actionButtonText: '',
    headerText: 'Continuar?',
    bodyText: 'Texto de cuerpo'
  };

  this.showModal = function(customModalDefaults, customModalOptions){
    if(!customModalDefaults) customModalDefaults = {};
    customModalDefaults.backdrop = 'static';
    return this.show(customModalDefaults, customModalOptions);
  };

  this.show = function(customModalDefaults, customModalOptions){
    var tempModalDefaults = {};
    var tempModalOptions = {};

    angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);
    angular.extend(tempModalOptions, modalOptions, customModalOptions);


    if(!tempModalDefaults.controller){
      tempModalDefaults.controller = function($scope, $uibModalInstance){
        $scope.modalOptions = tempModalOptions;
        $scope.modalOptions.ok = function(result){
          $uibModalInstance.close(result);
        };
        $scope.modalOptions.close = function(result){
          $location.path('/');
          $uibModalInstance.dismiss('cancel');
        };
      }
    }

    return $uibModal.open(tempModalDefaults).result;
  };

}]);
