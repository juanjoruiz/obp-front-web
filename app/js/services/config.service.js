var app = angular.module('isbanWebApp');

app.factory('config', ['localStorageService', 'alertaService', '$state', '$http', '$rootScope', '$window', '$q', '$crypto', '$log', 'connection', '$sce',
              function(localStorageService, alertaService, $state, $http, $rootScope, $window, $q, $crypto, $log, connection, $sce){
  let config = {};

  config.iconos = {
    error: 'images/icons/icono-error-validacion.png',
    info: 'images/icons/icono-info.png',
    paloma: 'images/icons/paloma.png'
  }

  config.mensajes = {
    general : 'Por el momento el servicio no está disponible, favor de intentarlo más tarde.',
    solicitudEditable : "Tiene una solicitud en proceso, ¿desea continuar con ella o crear una nueva?",
    solicitudNoEditable : "Tiene una solicitud pendiente.",
    solicitudBloqueada: 'Existe una solicitud en proceso, por favor intenta nuevamente más tarde.'

  }

  config.defaultModalOptions = {
    actionButtonText: 'Continuar',
    closeButtonText: 'Cancelar',
    showBoth: false,
    showOk: false,
    showClose: false,
    headerText: 'Información',
    modalIcon: config.iconos.info,
    bodyText: 'Texto'
  };

  const getBasicConf = () => {
    var sendData = {
      url: 'js/services/config.json',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }

    return $http(sendData)
              .then(function(response){
                localStorageService.set('flux', response.data.datos);
                return response.data.datos;
              })
              .catch(function(response){
                return $q.reject(response);
              })
              .finally(function(){
                configurationLog.info("Se cargó la configuración de flujos");

              });
  }

  //Configuraciones

  const showAlertMessage = options => {
    return alertaService.showModal({}, options);
  }

  config.generalMessages = message => {
    modalOptions = config.defaultModalOptions;
    switch (message.id) {
      case 1:
        modalOptions.actionButtonText = "Continuar";
        modalOptions.closeButtonText = "Nuevo";
        modalOptions.showOk = false;
        modalOptions.showBoth = true;
        modalOptions.showClose = false;
        modalOptions.headerText = message.titulo;
        modalOptions.bodyText = message.descripcion;
        showAlertMessage(modalOptions).then(function(response){

        }, function(response){

        });
        break;
      case 2:
        modalOptions.actionButtonText = "Salir";
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = message.titulo;
        modalOptions.bodyText = $sce.trustAsHtml(message.descripcion);
        showAlertMessage(modalOptions).then(function(response){
          localStorageService.clearAll();
          $window.location.href = 'http://www.santander.com.mx/';
        }, function(respone){

        });
        break;
      case 3:
        modalOptions.closeButtonText = message.botonCerrado;
        modalOptions.showOk = false;
        modalOptions.showBoth = false;
        modalOptions.showClose = true;
        modalOptions.modalIcon = config.iconos[message.tipo];
        modalOptions.headerText = message.titulo;
        modalOptions.bodyText = message.descripcion;
        showAlertMessage(modalOptions);
        break;
      case 4:
        modalOptions.actionButtonText = "Continuar";
        modalOptions.closeButtonText = "Cancelar";
        modalOptions.showOk = false;
        modalOptions.showBoth = true;
        modalOptions.showClose = false;
        modalOptions.headerText = message.titulo;
        modalOptions.bodyText = message.descripcion;
        return showAlertMessage(modalOptions);
      case 100:
        modalOptions.actionButtonText = "ACEPTAR";
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = "Re-envio exitoso";
        modalOptions.bodyText = "Hemos enviado un nuevo código de confirmación por mensaje SMS";
        modalOptions.modalIcon = config.iconos.paloma;
        showAlertMessage(modalOptions);
        break;
      case 200:
          modalOptions.actionButtonText = "Aceptar";
          modalOptions.showOk = true;
          modalOptions.showBoth = false;
          modalOptions.showClose = false;
          modalOptions.headerText = "";
          modalOptions.bodyText = "Esta acción no está permitida, no es posible regresar al paso anterior.";
          showAlertMessage(modalOptions);
          break;
      case 998:
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.actionButtonText = "Aceptar";
        modalOptions.headerText = message.titulo;
        modalOptions.bodyText = message.descripcion;
        showAlertMessage(modalOptions);
        break;
      case 999:
        modalOptions.showOk = false;
        modalOptions.showBoth = true;
        modalOptions.showClose = false;
        modalOptions.actionButtonText = "Aceptar";
        modalOptions.closeButtonText = "Cancelar";
        modalOptions.headerText = message.titulo;
        modalOptions.bodyText = message.descripcion;
        return showAlertMessage(modalOptions);
      case 1000:
        modalOptions.actionButtonText = "Continuar";
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = message.titulo
        modalOptions.bodyText = message.descripcion;
        showAlertMessage(modalOptions).then(function(response){
          $state.go('home');
        });
        break;
    }
  }

  config.generalErrors = error => {
    modalOptions = config.defaultModalOptions;
    if(error.tipo == "info"){
      modalOptions.modalIcon = config.iconos.info;
    }else if (error.tipo == "error") {
      modalOptions.modalIcon = config.iconos.error;
    }else if (error.tipo == "paloma") {
      modalOptions.modalIcon = config.iconos.paloma;
    }
    try{
      let temp = error.descripcion;        
      error.descripcion = $crypto.decrypt(error.descripcion);
      error.descripcion = error.descripcion === "" ? temp : error.descripcion;
    }
    catch (err){ 
      error.descripcion = error.descripcion;
    }
    switch(error.status){
      case -5:
        modalOptions.actionButtonText = 'Ok';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = error.titulo;
        modalOptions.bodyText = error.descripcion ? error.descripcion : config.mensajes.solicitudBloqueada;
        showAlertMessage(modalOptions);
        break;
      case -4:
        modalOptions.actionButtonText = error.actionButtonText;
        modalOptions.closeButtonText = error.closeButtonText;
        modalOptions.showOk = error.showOk;
        modalOptions.showBoth = error.showBoth;
        modalOptions.showClose = error.showClose;
        modalOptions.headerText = error.titulo;
        modalOptions.bodyText = error.descripcion
        if(error.descripcion !== null){
          error.descripcion = config.mensajes.solicitudEditable;
        }else{
          error.descripcion = config.mensajes.solicitudNoEditable;
        }
        modalOptions.errorCode = error.errorCode; 
        modalOptions.bodyText = error.descripcion ?  error.descripcion : config.mensajes.general;
        return showAlertMessage(modalOptions);
      case -3:
        
        modalOptions.actionButtonText = 'Salir';
        
        modalOptions.showOk = true;
        
        modalOptions.showBoth = false;
        
        modalOptions.showClose = false;
        
        modalOptions.headerText = error.titulo;
     

        modalOptions.bodyText = error.descripcion;
        modalOptions.errorCode = error.errorCode;
        showAlertMessage(modalOptions).then(function(response){
          
          localStorageService.clearAll();
          
          $state.go('home');
        
        });
       
        break;
      case -2:
        modalOptions.actionButtonText = 'Volver a intentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = error.titulo;

        modalOptions.errorCode = error.errorCode;
        modalOptions.bodyText = error.descripcion !== "" || error.descripcion ? error.descripcion : config.mensajes.general;
        showAlertMessage(modalOptions);
       break;
      case -1:
        modalOptions.actionButtonText = 'Volver a intentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = error.titulo;
        modalOptions.bodyText = error.descripcion;
        modalOptions.errorCode = error.errorCode;
        showAlertMessage(modalOptions).then(function(response){});
        break;
      case 0:
        modalOptions.actionButtonText = "Continuar";
        modalOptions.closeButtonText = "Cerrar";
        modalOptions.showOk = false;
        modalOptions.showBoth = true;
        modalOptions.showClose = false;
        modalOptions.modalIcon = config.iconos.info;
        modalOptions.headerText = error.titulo;
        modalOptions.bodyText = 'No se encontraron sucursales en esta ubicación, favor de buscar por dirección de la sucursal que desees.';
        return showAlertMessage(modalOptions);
      case 1:
        modalOptions.actionButtonText = 'Volver a intentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = error.titulo;
        modalOptions.bodyText = 'Los campos se encuentran vacíos';
        showAlertMessage(modalOptions);
        break;
      case 2:
        modalOptions.actionButtonText = 'Intentar con otra cuenta';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = error.descripcion;
        showAlertMessage(modalOptions);
        break;
      case 3:
        modalOptions.actionButtonText = 'Intentar con otros datos';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = error.descripcion;
        showAlertMessage(modalOptions);
        break;
      case 4:
        modalOptions.actionButtonText = 'Volver a intentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = error.descripcion;
        showAlertMessage(modalOptions)
          .then((response) => {
            localStorageService.clearAll();
            $state.go('main');
          });
        break;
      case 5:
        modalOptions.actionButtonText = 'Intentar con otros datos';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = error.descripcion;
        showAlertMessage(modalOptions);
        break;
      case 6:
        modalOptions.actionButtonText = 'Finalizar';
        modalOptions.showOk = false;
        modalOptions.showBoth = false;
        modalOptions.showClose = true;
        modalOptions.bodyText = error.descripcion;
        showAlertMessage(modalOptions)
          .then(function(response){

          }, function(){
            localStorageService.clearAll();
            $state.go('main');
          });
        break;
      case 401:
        modalOptions.actionButtonText = 'Reintentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = "Hubo un problema con la autenticación entre el servidor y la aplicación, reinicia la aplicación.";
        showAlertMessage(modalOptions);
        break;
      case 404:
        modalOptions.actionButtonText = 'Reintentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = error.data.descripcion;
        showAlertMessage(modalOptions);
        break;
      case 500:
        modalOptions.actionButtonText = 'Reintentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = "Hubo un problema con la conexión al servidor, vuelve a intentarlo más tarde.";
        showAlertMessage(modalOptions);/*.then(function(response){
          localStorageService.clearAll();
          location.reload();
        });*/
        break;
      case -100:
        modalOptions.actionButtonText = 'Volver a intentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.headerText = error.titulo;

        modalOptions.errorCode = error.errorCode;
        modalOptions.bodyText = error.descripcion !== "" || error.descripcion ? error.descripcion : config.mensajes.general;
        showAlertMessage(modalOptions).then(function(){
          window.location.reload();
        });
        break;
      default:
        modalOptions.actionButtonText = 'Reintentar';
        modalOptions.showOk = true;
        modalOptions.showBoth = false;
        modalOptions.showClose = false;
        modalOptions.bodyText = error.descripcion;
        showAlertMessage(modalOptions);
        break;
    }
  }

  const error = response => config.generalErrors(response);
  //config.basic = getBasicConf();

  config.nextStep = (plusSteps) => {
    console.log("flujoStep");
    let flux = localStorageService.get('flux');
    let step = localStorageService.get('fluxStep') + plusSteps;
    localStorageService.set('fluxStep', step);
    console.log(flux.flujo.length, " ", step, " ", flux.flujo.length > step ? flux.flujo[localStorageService.get('fluxStep')] : flux.flujo[localStorageService.get('fluxStep') - flux.flujo.length]);
    if(flux.flujo.length > step){
        $state.go(flux.flujo[localStorageService.get('fluxStep')].pantalla);
    }else if (flux.flujo.length <= step) {
      console.log(flux.flujo.length + flux.flujo2.length);
      if((flux.flujo.length + flux.flujo2.length) <= step) {
        $state.go('calificarexperiencia');
      }else{
        // if(flux.flujo[localStorageService.get('fluxStep') - flux.flujo.length].pantalla == "beneficios"){
        //  localStorageService.set('fluxStep', (step + 1));
        // }
        console.log(flux.flujo2[localStorageService.get('fluxStep') - flux.flujo.length].pantalla);
        $state.go(flux.flujo2[localStorageService.get('fluxStep') - flux.flujo.length].pantalla);
      }
    }
  }



  return config;
}]);
