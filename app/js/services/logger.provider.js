(function(){
  angular.module('isbanWebApp')
    .provider('logger', logger);

    function logger(){
      this.loggingPattern = '%s - %s: ';
      this.$get = () => {
        let loggingPattern = this.loggingPattern;
        return{
          enhanceLog : function($log) {
            $log.enabledContexts = [];
            $log.getInstance = function(context){
              return{
                log : enhanceLogging($log.log, context, loggingPattern),
                info : enhanceLogging($log.info, context, loggingPattern),
                warn : enhanceLogging($log.warn, context, loggingPattern),
                debug : enhanceLogging($log.debug, context, loggingPattern),
                error : enhanceLogging($log.error, context, loggingPattern),
                enableLogging : function(enable){
                  //if(baseConfig.ENV == 'pre'){
                    //$log.enabledContexts[context] = false;
                  //}else{
                    $log.enabledContexts[context] = enable;
                  //}
                }
              };
            };

            function enhanceLogging(loggingFunc, context, loggingPattern){
              return function(){
                let contextEnabled = $log.enabledContexts[context];
                if(contextEnabled === undefined || contextEnabled){
                  let modifiedArguments = [].slice.call(arguments);
                  modifiedArguments[0] = [sprintf(loggingPattern, moment().format("dddd h:mm:ss a"), context)] + modifiedArguments[0];
                  loggingFunc.apply(null, modifiedArguments);
                }
              };
            }
          }
        };
      };
    }
})();
