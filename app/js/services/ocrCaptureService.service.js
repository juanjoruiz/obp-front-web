var app = angular.module('isbanWebApp');

app.service('ocrCaptureService', ['$uibModal', '$location', 'localStorageService', function($uibModal, $location, localStorageService){
  var modalDefaults = {
    backdrop: true,
    keyboard: false,
    modalFade: true,
    templateUrl:'js/shared/ocrModal/ocrmodal.view.html'
  };

  var modalOptions = {
    closeButtonText: '',
    actionButtonText: '',
    headerText: 'Continuar?',
    bodyText: 'Texto de cuerpo',
    uiref: 'home'
  };

  this.showModal = function(customModalDefaults, customModalOptions){
    if(!customModalDefaults) customModalDefaults = {};
    customModalDefaults.backdrop = 'static';
    return this.show(customModalDefaults, customModalOptions);
  };

  this.show = function(customModalDefaults, customModalOptions){
    var tempModalDefaults = {};
    var tempModalOptions = {};

    angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);
    angular.extend(tempModalOptions, modalOptions, customModalOptions);

    if(!tempModalDefaults.controller){
      tempModalDefaults.controller = function($scope, $uibModalInstance){
        $scope.modalOptions = tempModalOptions;
        $scope.modalOptions.ok = function(result){
          $uibModalInstance.close(result);
        };
        $scope.modalOptions.close = function(result){
          $uibModalInstance.dismiss('cancel');
          return true;
        };
      }
    }
    return $uibModal.open(tempModalDefaults).result;
  };

}]);
