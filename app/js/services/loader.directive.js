(function(){
  angular.module('isbanWebApp')
    .directive('loader', loader);

    function loader($rootScope){
      //let spinnerDirective = $log.getInstance('spinner-directive');
      return function($scope, element, attrs){
        $scope.$on("loader_show", function(){

          if (element.hasClass("hidden")) {
            element.removeClass("hidden")
          }
        });
        return $scope.$on("loader_hide", function(){
          
          if (!element.hasClass("hidden")) {
            element.addClass("hidden")
          }
        });
      }
    }
})();
