var app = angular.module('isbanWebApp');

app.service('avisoService', ['$uibModal', '$location', '$sce', function($uibModal, $location, $sce){

  var avisoDefaults = {
    backdrop: true,
    keyboard: true,
    modalFade: true,
    templateUrl:'js/shared/aviso/aviso.view.html'
  };

  var avisoOptions = {
    actionButtonText: '',
    headerText: 'Aviso de Privacidad',
    bodyText: '<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>'
  };

  this.showModal = function(customModalDefaults, customModalOptions){
    if(!customModalDefaults) customModalDefaults = {};
    customModalDefaults.backdrop = 'static';
    return this.show(customModalDefaults, customModalOptions);
  };

  this.show = function(customModalDefaults, customModalOptions){
    var tempModalDefaults = {};
    var tempModalOptions = {};

    angular.extend(tempModalDefaults, avisoDefaults, customModalDefaults);
    angular.extend(tempModalOptions, avisoOptions, customModalOptions);


    if(!tempModalDefaults.controller){
      tempModalDefaults.controller = function($scope, $uibModalInstance){
        $scope.avisoOptions = tempModalOptions;
        $scope.avisoOptions.ok = function(result){
          $uibModalInstance.close(result);
          $uibModalInstance.dismiss('cancel');
        };
        $scope.avisoOptions.close = function(result){
          //$location.path('/');
          $uibModalInstance.dismiss('cancel');
        };
      }
    }

    return $uibModal.open(tempModalDefaults).result;
  };

}]);
