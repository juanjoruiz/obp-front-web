(function(){
  angular.module('isbanWebApp')
    .controller('formcontactoController', formcontactoController);

    function formcontactoController($rootScope, $scope, localStorageService, $state){
      let vm = this;
      $rootScope.$broadcast('contacto');
      vm.contactoVisible = true;
      vm.smsVisible = false;
      localStorageService.set('initsr', false);

      $scope.$on('smsVisible', function(){
        if($scope.settings.desktop || $scope.settings.tablet){
          vm.contactoVisible = true;
          vm.smsVisible = true;
        }else if ($scope.settings.mobile) {
          vm.contactoVisible = false;
          vm.smsVisible = true;
        }
      });

      $scope.$on('cambiarTelefono', function(){
        if($scope.settings.desktop || $scope.settings.tablet){
          vm.contactoVisible = true;
          vm.smsVisible = false;
        }else if ($scope.settings.mobile) {
          vm.contactoVisible = true;
          vm.smsVisible = false;
        }
      });

      $scope.$on('steptwo', function(){
        switch(localStorageService.get('tipoDeMercado')){
          case '1':
            console.log('Universidades');
            break;
          case '2':
            console.log('Nomina');
            $state.go('sucursal');
            break;
          case '3':
            console.log('Mercado abierto');
            break;
        }
      });
    }

    formcontactoController.$inject = ['$rootScope', '$scope', 'localStorageService', '$state'];
})();
