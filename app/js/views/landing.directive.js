var app = angular.module('isbanWebApp');

app.directive('landing', function(){
  return{
    restrict: 'A',
    templateUrl: 'js/views/landing.view.html',
    controllerAs: 'landing',
    controller: ['localStorageService', 'aplicacion', '$state', '$rootScope', function(localStorageService, aplicacion, $state, $rootScope){
      var landing = this;
      landing.logoImage = "images/icons/logoSantander.png";
      landing.universidadesBloqueado = $rootScope.flujo_universidades_bloqueado;
      landing.nominaBloqueado = $rootScope.flujo_nomina_bloqueado;
      landing.mercadoBloqueado = $rootScope.flujo_mercado_abierto_bloqueado;
      //landing.nominaBloqueado =
      //landing.mercadoBloqueado = conf.datos.flujo_mercado_abierto_bloqueado;

      if($rootScope.ENV_VAR == 'pro'){
        utag.view({page:'/'});
      }


      landing.universidadesFlux = () => {
        if(landing.universidadesBloqueado){
          console.log("bloqueado");
        }else{
          $state.go('beneficios', {tipo:1});
        }
      }

      landing.nominaFlux = () => {
        if(landing.universidadesBloqueado){
          console.log("bloqueado");
        }else{
          $state.go('beneficios', {tipo:2});
        }
      }

      landing.mercadoFlux = () => {
        if(landing.universidadesBloqueado){
          console.log("bloqueado");
        }else{
          if(window.innerWidth < 760){
            $state.go('carrusel', {tipo:3});
          }else{
            $state.go('beneficios', {tipo:3});
          }
        }
      }


      if(window.innerWidth < 760){
        landing.btnUniversidad = "btn_home_universidad.jpg";
        landing.btnNomina = "btn_home_nomina.jpg";
        landing.btnMercado = "btn_home_mercado.jpg";
      } else {
        landing.btnUniversidad = "imageHomeWeb3.png";
        landing.btnNomina = "imageHomeWeb2.png";
        landing.btnMercado = "imageHomeWeb1.png";
      }

      landing.btnUniversidad = "bgUniversidades.png";
      landing.btnNomina = "bgEmpresas.png";
      landing.btnMercado = "bgMercadoAbierto.png";

    }]
  }
});
