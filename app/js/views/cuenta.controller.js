(function(){
  angular.module('isbanWebApp')
    .controller('CuentaController', CuentaController);

  function CuentaController($rootScope, $scope, localStorageService){
    let vm = this;
    let saveandretrieveshowresumen = localStorageService.get('saveandretriveshowresumen');
    if(saveandretrieveshowresumen){
      $rootScope.$broadcast('aceptacionDeContrato');
      vm.showContrato = false;
      vm.showResumen = true;
    }else{
      vm.showContrato = true;
      vm.showResumen = false;
    }


    $scope.$on('aceptacionDeContrato', function(){
      vm.showContrato = false;
      vm.showResumen = true;
    });

  }

  CuentaController.$inject = ["$rootScope","$scope", "localStorageService"];
})();
