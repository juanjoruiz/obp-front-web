(function(){
  angular.module('isbanWebApp')
    .directive('formcontacto', formcontacto);

    function formcontacto(){
      return{
        restrict: 'EA',
        templateUrl: 'js/views/formcontacto.view.html',
        controllerAs: 'formcontactoCtrl',
        controller: 'formcontactoController'
      }
    }
})();
