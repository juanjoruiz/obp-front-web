(function(){
  angular.module('isbanWebApp')
    .controller('beneficiosCondicionesController', beneficiosCondicionesController);

  function beneficiosCondicionesController($rootScope, $scope, $stateParams, localStorageService, connection, config, $log, $state){
    let vm = this;
    localStorageService.set('solicitudRetomada', false);


    //$scope.$on('loggedIn', function(){

    if($stateParams.tipo == 1 && $rootScope.flujo_universidades_bloqueado){
      $state.go('home');
    }else if ($stateParams.tipo == 2 && $rootScope.flujo_nomina_bloqueado) {
      $state.go('home');
    }else if($stateParams.tipo == 3 && $rootScope.flujo_mercado_abierto_bloqueado){
      $state.go('home');
    }else if ($stateParams.tipo == 4 && $rootScope.flujo_mercado_abierto_bloqueado) {
      $state.go('home');
    }else{
      var valueMercado = 0
      if($stateParams.tipo == 4){
        valueMercado = 3;
      }else{
        valueMercado = $stateParams.tipo;
      }

      connection.getCopies(valueMercado)
        .then(function(response){
          console.log(response);
          if(response.estatus !== 0){
            let error = {
              id: response.estatus,
              descripcion: response.descripcion,
              codigo: response.codigo || "",
              titulo: response.titulo,
              tipo: response.tipo,
              datos: response.datos || "",
            }
            config.generalErrors(error);
          }
        });

      connection.getBeneficios()
        .then(function(response){
          console.log(response);
          if(response.estatus === 0){
            localStorageService.set('benycond', response.datos);
            console.info('copies de beneficios cargadas');
            $rootScope.$broadcast('benycondLoaded');
          }else{
            let error = {
              status: response.estatus,
              descripcion: response.descripcion,
              errorCode: response.codigo,
              titulo: response.titulo,
              tipo: response.tipo,
              datos: response.datos,
            }
            config.generalErrors(error);
          }

        });
    }


    if((localStorageService.get('tipoDeMercado') == 3 || localStorageService.get('tipoDeMercado') == 4 ) && window.innerWidth < 760 ){
      vm.beneficiosVisible = false;
      vm.condicionesVisible = true;
    }else if($scope.settings.desktop || $scope.settings.tablet){
      $scope.bannerStyle = {'margin-top':'40px'}
      vm.beneficiosVisible = true;
      vm.condicionesVisible = true;
    }else if ($scope.settings.mobile) {
      $scope.bannerStyle = {'margin-top':'0px'}
      vm.beneficiosVisible = true;
      vm.condicionesVisible = false;
    }

    $scope.$on('showCondiciones', function(){
      vm.beneficiosVisible = false;
      vm.condicionesVisible = true;
    });
  }

  beneficiosCondicionesController.$inject = ['$rootScope', '$scope', '$stateParams', 'localStorageService', 'connection', 'config', '$log', '$state']
})();
