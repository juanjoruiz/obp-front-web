(function(){
  angular.module('isbanWebApp')
    .directive('seleccionasucursal', seleccionasucursal);

    function seleccionasucursal(){
      return{
        restrict: 'EA',
        templateUrl: 'js/views/seleccionasucursal.view.html',
        controllerAs: 'sucursalCtrl',
        controller: 'SeleccionaSucursalController'
      }
    }
})();
