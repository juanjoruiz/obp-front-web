(function(){
  angular.module('isbanWebApp')
    .directive('calificarexperiencia', calificarexperiencia);

  function calificarexperiencia(){
    return{
      restrict: 'A',
      templateUrl: 'js/views/calificarexperiencia.view.html',
      controllerAs: 'calificarExpCtrl',
      controller: 'CalificarExperienciaController'
    }
  }
})();
