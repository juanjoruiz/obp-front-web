(function(){
  angular.module('isbanWebApp')
    .directive('bancaDigital', bancaDigital);

  function bancaDigital(){
    return{
      restrict: 'A',
      templateUrl: 'js/views/bancadigital.view.html',
      controllerAs: 'bancaCtrl',
      controller: 'BancaDigitalController'
    }
  }
})();
