(function(){
  angular.module('isbanWebApp')
    .directive('muestratarjeta', muestratarjeta);

  function muestratarjeta(){
    return{
      restrict: 'A',
      templateUrl: 'js/views/muestratarjeta.view.html',
      controllerAs: 'muestratrajetaCtrl',
      controller: 'MuestraTarjetaController'
    }
  }

})();
