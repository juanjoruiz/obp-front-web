(function(){
  angular.module('isbanWebApp')
    .directive('seleccionatarjeta', seleccionatarjeta);

  function seleccionatarjeta(){
    return{
      restrict: 'A',
      templateUrl: 'js/views/seleccionatarjeta.view.html',
      controllerAs: 'tarjetaCtrl',
      controller: 'SeleccionaTarjetaController'
    }
  }
})();
