(function(){
  angular.module('isbanWebApp')
    .directive('benycond', benycond);

  function benycond(){
    return{
      restrict: 'EA',
      templateUrl: 'js/views/benycond.view.html',
      controllerAs: 'bcCtrl',
      controller: 'beneficiosCondicionesController'
    }
  }
})();
