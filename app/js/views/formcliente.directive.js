(function(){
  angular.module('isbanWebApp')
    .directive('formcliente', formcliente);

  function formcliente() {
    return{
      restrict: 'A',
      templateUrl: 'js/views/formcliente.view.html',
      controllerAs: 'clienteCtrl',
      controller: 'FormClienteController'
    }
  }

})();
