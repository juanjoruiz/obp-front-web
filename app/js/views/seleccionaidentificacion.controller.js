(function(){
  angular.module('isbanWebApp')
    .controller('SeleccionaIdentificacionController', SeleccionaIdentificacionController);

  function SeleccionaIdentificacionController($rootScope, $scope, $stateParams, localStorageService, connection){
    let vm = this;
    vm.showOcr = false;
    vm.showDoc = true;

    let isChrome = !!window.chrome && !!window.chrome.webstore;
    let isFirefox = typeof InstallTrigger !== 'undefined';
    let isAndroid = navigator.userAgent.indexOf("Android") != -1;

    console.log(localStorageService.get('tipoDeMercado'));
    if((isChrome || isFirefox || isAndroid ) && localStorageService.get('tipoDeMercado') != 4){
      vm.showOcr = true;
      vm.showDoc = false;
    } else {
      vm.showOcr = false;
      vm.showDoc = true;
    }

    $rootScope.$broadcast('initMap');
  }

  SeleccionaIdentificacionController.$inject = ['$rootScope', '$scope', '$stateParams', 'localStorageService', 'connection']
})();
