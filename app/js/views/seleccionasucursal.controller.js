(function(){
  angular.module('isbanWebApp')
    .controller('SeleccionaSucursalController', SeleccionaSucursalController);

  function SeleccionaSucursalController($rootScope, $scope, $stateParams, localStorageService, connection){
    let vm = this;
    $rootScope.$broadcast('initMap');
    if($scope.settings.desktop || $scope.settings.tablet){

    }else if ($scope.settings.mobile) {

    }

    if(localStorageService.get('tipoDeMercado') == 4) $scope.showBranch = false;
    else $scope.showBranch = true;
  }

  SeleccionaSucursalController.$inject = ['$rootScope', '$scope', '$stateParams', 'localStorageService', 'connection']
})();
