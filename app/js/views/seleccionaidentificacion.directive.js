(function(){
  angular.module('isbanWebApp')
    .directive('seleccionaidentificacion', seleccionaidentificacion);

    function seleccionaidentificacion(){
      return{
        restrict: 'EA',
        templateUrl: 'js/views/seleccionaidentificacion.view.html',
        controllerAs: 'identificacionCtrl',
        controller: 'SeleccionaIdentificacionController'
      }
    }
})();
