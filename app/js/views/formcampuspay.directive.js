(function(){
  angular.module('isbanWebApp')
    .directive('formcampuspay', formcampuspay);

  function formcampuspay(){
    return{
      restrict: 'EA',
      templateUrl: 'js/views/formcampuspay.view.html'
    }
  }
})();
