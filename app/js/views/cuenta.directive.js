(function(){
  angular.module('isbanWebApp')
    .directive('cuenta', cuenta);

  function cuenta(){
    return{
      restrict: 'A',
      templateUrl: 'js/views/cuenta.view.html',
      controllerAs: 'cuentaCtrl',
      controller: 'CuentaController'
    }
  }


})();
