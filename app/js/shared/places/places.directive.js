(function(){
  angular.module('isbanWebApp')
    .directive('placeAutocomplete', placeAutocomplete);

  function placeAutocomplete(){
    return{
      templateUrl: 'js/shared/places/places.view.html',
      restrict: 'EA',
      scope:{
        'ngModel':'='
      },
      controller: function($rootScope, $scope, $q){
        if(!google || !google.maps){
          throw new Error('Google Maps no se ha podido cargar.');
        }else if(!google.maps.places){
          throw new Error('Google Maps no cuenta con los módulos de Places.');
        }

        let autocompleteService = new google.maps.places.AutocompleteService();
        let map = new google.maps.Map(document.createElement('div'));
        let placeService = new google.maps.places.PlacesService(map);

        $scope.ngModel = {};

        const getResults = (address) => {
          let deferred = $q.defer();
          autocompleteService.getQueryPredictions({
            input: address
          }, function(data) {
            deferred.resolve(data);
          });
          return deferred.promise;
        };

        const getDetails = (place) => {
          let deferred = $q.defer();
          placeService.getDetails({
            'placeId': place.place_id
          }, function(details){
            deferred.resolve(details);
          });
          return deferred.promise;
        };

        $scope.search = function(input){
          if(!input){
             return;
          }
          return getResults(input).then(function(places){
            console.log(places[0].description);
            return places ? places : [];
          });
        }

        $scope.getLatLng = function(place){
          console.log(place)
          if(!place){
            $scope.ngModel = {};
            return;
          }
          getDetails(place).then(function(details){
            // $scope.ngModel = {
            //   'name': place.description,
            //   'latitude': details.geometry.location.lat(),
            //   'longitude': details.geometry.location.lng()
            // };
            console.log(place.description);
            $rootScope.$broadcast('startGeoSearch');
            $scope.ngModel = place.description;
          });
        }
      }
    }
  }
})();
//
// (function(){
//   angular.module('isbanWebApp')
//     .directive('places', places);
//
//   function places(){
//     return {
//       restrict: 'A',
//       require: 'ngModel',
//       scope: {
//         googleplaceAutocompletePlace: '=?',
//         googleplaceAutocomplete: '=',
//       },
//       link: function postLink(scope, element, attrs, model) {
//         var options = scope.googleplaceAutocomplete;
//         var autocomplete = new google.maps.places.Autocomplete(element[0], options);
//
//         google.maps.event.addListener(autocomplete, 'place_changed', function () {
//           scope.$apply(function () {
//             scope.googleplaceAutocompletePlace = autocomplete.getPlace();
//             model.$setViewValue(element.val());
//           });
//         });
//
//         scope.$on('$destroy', function () {
//           google.maps.event.clearInstanceListeners(element[0]);
//         });
//       }
//     };
//   }
// })();
