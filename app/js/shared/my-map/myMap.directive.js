var app = angular.module('isbanWebApp');

app.directive('myMap', function(){
  var link = function(scope, element, attrs){

    scope.sucursalId = "";

    scope.setMarker = function(map, position,title, content, type){
      var marker;
      var markerOptions = {};
      if(type=="normal"){
        markerOptions = {
          clickable: false,
          icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
          shadow: null,
          position: position,
          map: map
        };
      }else if(type=="branch"){
        let icon = {
          url: 'images/icons/logoFlama.png',
          scaledSize: new google.maps.Size(32, 30),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(0, 30)
        }
        markerOptions = {
          position: position,
          map: map,
          title: content.id,
          more: content.dir2,
          //icon: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png'
          icon: icon
        }
      }

      marker = new google.maps.Marker(markerOptions);
      scope.markers.push(marker);
      if(type=="branch"){ scope.numbers.push(marker); }

      google.maps.event.addListener(marker, 'click', function(){
        //console.log("click en ", marker.title);
        scope.sucursalId = marker.title;
        scope.$apply();
      });
    };

    scope.markerSetting = function(){
      angular.forEach(scope.branches, function(branch){
        scope.setMarker(scope.map, new google.maps.LatLng(branch.latitude, branch.longitude), 'Santander', branch, "branch");
      });
    };

    scope.render = function(){
      //scope.loader = true;
      var infoWindow;
      scope.markers = [];
      var selected = scope.tipoBusqueda;
      //var branches = scope.getBranches();

      if(navigator.geolocation && selected == 'ubicacion'){
        navigator.geolocation.getCurrentPosition(function(position){
          scope.$apply(function(){

            var mapOptions = {
              center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
              zoom:15,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false
            };


            function initMap(){
              scope.map = new google.maps.Map(element[0], mapOptions);
              scope.loader = false;
            }

            scope.loader = true;
            initMap();
            //console.log(position);

            scope.setMarker(scope.map, new google.maps.LatLng(position.coords.latitude, position.coords.longitude), '', {}, "normal");
            var branches = scope.getBranches(position.coords.latitude, position.coords.longitude);
            // console.log(branches);
            //scope.$watch('branches', function(newValue))
            angular.forEach(branches, function(branch){
            //   console.log(branch);
              scope.setMarker(scope.map, new google.maps.LatLng(branch.latitude, branch.longitude), 'Santander',  branch, "branch");
            });

          });
        });
      }else if(navigator.geolocation && selected == 'direccion'){
        navigator.geolocation.getCurrentPosition(function(position){
          var mapOptions = {
            center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
            zoom:15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
          };

          var initMap = function(){
            //if(map == void 0){
              scope.map = new google.maps.Map(element[0], mapOptions);
              initialize();
            //}
          }

          var initialize = function() {
            var geocoder = new google.maps.Geocoder();
            if(scope.searchAddress === undefined ) return;
            geocoder.geocode({ 'address': scope.searchAddress }, function(results, status){
              if(status === google.maps.GeocoderStatus.OK){
                scope.map.setCenter(results[0].geometry.location);
                scope.map.setZoom(15);
                scope.setMarker(scope.map, results[0].geometry.location, '', {}, "normal");

                var branches = scope.getBranches(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                angular.forEach(branches, function(branch){
                  scope.setMarker(scope.map, new google.maps.LatLng(branch.latitude, branch.longitude), 'Santander',  branch, "branch");
                });
              }else{
                alert('Geocode was not successful for the following reason: ' + status);
              }
            });
          }
          initMap();


        });

      }
      scope.loader = false;
    }

    scope.$on('initMap', function(ev, data){
      console.log("iniciando mapa");
      scope.render();
    })

    scope.$on('branchesChanged', function(ev, data){
      scope.markerSetting();
    });

    scope.$on('somethingChangedInFirstDirective', function(ev, data){
      scope.selectedCard = [];
      scope.branches=[];
      scope.render();
    });

    scope.$on('reloadMap', function(ev, data){
      console.log('recargando');
      scope.reload = false;
      scope.render();
    });

    scope.$on('theBranchIsVisible', function(ev, data){
      scope.render();
    });

    scope.$watch('sucursalId', function(newValue, oldValue){
      if(!newValue|| angular.equals(newValue, oldValue)){
        return;
      }
      scope.$emit('clickedOnMarker', scope.sucursalId);
    });



    scope.render();
  };

  return{
    restrict: 'A',
    template: '<div id="gmaps"></div>',
    replace: true,
    link: link
  };
})
