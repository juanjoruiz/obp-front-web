var app = angular.module('isbanWebApp');

app.directive('testServices', function(){
  return{
    restrict: 'E',
    templateUrl: 'js/shared/testServices/testServices.view.html',
    controllerAs: 'testservice',
    controller: function($scope, connection, localStorageService, $crypto, $base64){

      localStorageService.clearAll();
      var testservice = this;

      connection.login().then(function(response){
        localStorageService.set('token', response.data);
        console.log(response);
      },function(response){
        console.log(response);
      })

      $scope.payment = 'Selecctiona';
      $scope.individuals = [
        '1',
        '2',
        '3',
        '4'
      ]

      $scope.selectEntity = function(value){
        $scope.payment = value;
      }





      var plainText = "X3pQSj2/LNBbsdwHGA37+8pyZESXpPirBAwe7cb9xRiHTqfb+96ls7WyCBixYVe6eCCvA2gDuTYt7m/POR31ng==";
      console.log(CryptoJS.enc.Utf8.parse(plainText));

      var hola = "4c5TGapEpSZchG2hP+zyfVzC3A4TbTBFVmVY6hzBDzM=";
      //var hola = plainText;
      //console.log(plainText); //Plano
      var cText = $crypto.encrypt(hola);
      var dText = $crypto.decrypt(hola);
      alert(cText);
      alert(dText);
      console.log(cText);
      console.log(dText);
      console.log(plainText);
      console.log($crypto.decrypt(plainText));


      //console.log($crypto.decrypt($crypto.encrypt(hola)));

      //testservice.cypherText = $crypto.encrypt(hola);
      //  console.log(testservice.cypherText); //AES
      //console.log($base64.encode(testservice.cypherText));
      //console.log($base64.decode($crypto.decrypt(testservice.cypherText))); //desencriptado

      // //var respuesta = $crypto.encrypt(hola);
      // //console.log(respuesta);
      // plainText = $base64.decode(plainText);
      // console.log(plainText.toString());
      //testservice.originalText = $crypto.decrypt(plainText);
      //var key = aesjs.util.convertStringToBytes("f39280eb313fbff925839b259b3928cb");
      //var iv = aesjs.util.convertStringToBytes("9ff13b51db5353b3");

      testservice.getBeneficios = function(){
        connection.getBeneficios()
          .then(function(response){
            testservice.responseBeneficios = response.data;
            testservice.responseBeneficios.status = response.status;
          },function(response){
            console.log(response.status);
          });
      }

      testservice.getContrato = function(){
        connection.getContratoPlain()
          .then(function(response){
            testservice.responseContrato = response.data.datos.datos;
            testservice.responseContrato.status = response.status
          },function(response){
            console.log(response.data);
          });
      }

      testservice.sendDatosContacto = function(){
        var compañia = { id: "IU", company: "IUSACELL" };
        localStorageService.set('telefonoContacto', '5588997766');
        localStorageService.set('companiaContacto', compañia);
        localStorageService.set('emailContacto', 'jjge@me.com');
        connection.sendDatosContacto()
          .then(function(response){
            localStorageService.set('idSolicitud', response.data.datos.idSolicitud);
            localStorageService.set('idNoCliente', response.data.datos.idNoCliente);
            localStorageService.set('smsToken', response.data.datos.token);
            testservice.responseDatosContacto = response.data;
            testservice.responseDatosContacto.status = response.status;

          },function(response){
            testservice.responseDatosContacto.status = response.status;
          });
      }

      testservice.sendToken = function(){
        connection.sendSmsToken()
          .then(function(response){
            testservice.responseToken = response.data;
            testservice.responseToken.status = response.status;
          },function(response){
            testservice.responseToken.status = response.status;
          });
      }


      testservice.sendPan = function(){
        setDatosPan();
        connection.sendPanData()
          .then(function(response){
            testservice.responsePan = response.data;
            testservice.responsePan.status = response.status;
          }, function(response){
            testservice.responsePan.status = response.status;
          });
      }

      testservice.sendDocumento = function(){
        setDocumento();
        connection.sendIdentification()
          .then(function(response){
            testservice.responseDocumento = response.data;
            testservice.responseDocumento.status = response.status;
          }, function(response){
            testservice.responseDocumento.status = response.status;
          });
      }

      testservice.sendDatosPersona = function(){
        setBeneficiario();

      }

      testservice.sendAcepto = function(){
        connection.sendAcepto()
          .then(function(response){
            testservice.responseContrato = response.data;
            testservice.responseContrato.status = response.status;
          }, function(response){
            testservice.responseContrato.status = response.status;
          });
      }

      testservice.getAccount = function(){
        connection.getAccountData()
          .then(function(response){
            testservice.responseCuenta = response.data;
            testservice.responseCuenta.status = response.status;
          }, function(response){
            testservice.responseCuenta.status = response.status;
          });
      }

      testservice.sendPassword = function(){
        setPassword();
        connection.sendPass()
          .then(function(response){
            testservice.responsePassword = response.data;
            testservice.responsePassword.status = response.status;
          }, function(response){
            testservice.responsePassword.status = response.status;
          });
      }

      testservice.sendEncuesta = function(){
        setEncuesta();
        connection.sendEncuesta()
          .then(function(response){
            testservice.responseEncuesta = response.data;
            testservice.responseEncuesta.status = response.status;
          }, function(response){
            testservice.responseEncuesta.status = response.status;
          })
      }

      var setBeneficiario = function(){
        localStorageService.set('zipcodebeneficiario', '01210');
        connection.sendCp(localStorageService.get('zipcodebeneficiario'))
          .then(function(response){
            testservice.cpResponse2 = response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;
            testservice.selectedColoniaBeneficiario = testservice.cpResponse2[testservice.cpResponse2.length - 1];
            localStorageService.set('delegacionbeneficiario', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
            localStorageService.set('ciudadbeneficiario', response.data.datos.lstEstado[0].ciudades[0].ciudad);
            localStorageService.set('estadobeneficiario', response.data.datos.lstEstado[0].estado);
            localStorageService.set('coloniabeneficiario', testservice.selectedColoniaBeneficiario);
            localStorageService.set('fechanacimientobeneficiario', '1990-08-08');
            localStorageService.set('apellidomaternobeneficiario', 'martinez');
            localStorageService.set('beneficiarioMismoDomicilio', true);
            localStorageService.set('nacionalidadbeneficiario', '052' );
            localStorageService.set('nombrebeneficiario', 'Roberto');
            localStorageService.set('numeroexteriorbeneficiario', '12');
            localStorageService.set('numerointerior', '');
            localStorageService.set('apellidopaternobeneficiario', 'morales');
            localStorageService.set('callebeneficiario', 'valle sol');
            setDatosBasicos();

            console.log(localStorageService.get('coloniabeneficiario'));
          }, function(response){
            console.log(response);
          });

      };

      var setDatosBasicos = function(){
        var rol = { id: "Estudiante", actividadGenerica:"08W", actividadEspecifica:"09900904" };
        var entidad = { id: "BC", descripcion: "BAJA CALIFORNIA" };
        localStorageService.set('roluniversitario', rol);
        localStorageService.set('apellidomaterno', 'Bovulle');
        localStorageService.set('apellidopaterno', 'Ruman');
        localStorageService.set('entidadnacimiento', entidad);
        localStorageService.set('fechanacimiento', '1990-07-07');
        localStorageService.set('nombre', 'Adrian');
        localStorageService.set('gender', 'H');
        setDomicilio();

      };

      var setDomicilio = function(){
        //var asentamiento = { nombre: 'nombre', asentamiento: 'asentamiento'}
        localStorageService.set('zipcode', '76140');
        connection.sendCp(localStorageService.get('zipcode'))
          .then(function(response){
            testservice.cpResponse = response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;

            testservice.selectedColonia = testservice.cpResponse[testservice.cpResponse.length - 1];
            localStorageService.set('delegacion', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
            localStorageService.set('ciudad', response.data.datos.lstEstado[0].ciudades[0].ciudad);
            localStorageService.set('estado', response.data.datos.lstEstado[0].estado);
            console.log(localStorageService.get('estado'));
            localStorageService.set('colonia', testservice.selectedColonia );
            console.log(localStorageService.get('colonia').asentamiento);
            localStorageService.set('calle', 'valle sol');
            localStorageService.set('numeroexterior', '12');
            localStorageService.set('numerointerior', '');
            sendPersona();
          }, function(response){
            console.log(response);
          });

      };

      testservice.getBranch = function(){
        connection.getBranches('19.361327', '-99.2782142')
          .then(function(response){
            console.log($crypto.decrypt(response.data.descripcion));
          }, function(response){
            console.log(response);
          })
      }

      var sendPersona = function(){
        connection.sendDatosPersona()
          .then(function(response){
            testservice.responseDatosPersona = response.data;
            testservice.responseDatosPersona.status = response.status;
          },function(response){
            testservice.responseDatosPersona = {};
            testservice.responseDatosPersona.status = response.status;
          });
      };

      var setDatosPan = function(){
        localStorageService.set('pan', testservice.pan);
        localStorageService.set('mes', testservice.mes);
        localStorageService.set('anio', testservice.anio);
        localStorageService.set('ccv', testservice.ccv);
      };

      var setDocumento = function(){
        localStorageService.set('ineAnverso', testservice.anverso);
        localStorageService.set('ineReverso', testservice.reverso);
        localStorageService.set('typeId', 'ine');
      };

      var setPassword = function(){
        localStorageService.set('password', 'Password3');
      };

      var setEncuesta = function(){
        localStorageService.set('rate', 4);
        localStorageService.set('comments', 'Estupenda aplicación!');
      }
    }
  }
});
