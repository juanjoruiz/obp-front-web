var app = angular.module('isbanWebApp');

app.directive('complexPassword', function($rootScope) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(password) {
        //Contadores
        let mayusCounter = 0;
        let minusCounter = 0;
        let numberCounter = 0;
        let specialCharCounter = 0;
        let letrasConsecutivas = 0;
        let consecutiveCounter = 0;
        let sameCharCounter = 0;
        //Reglas
        const mayusRule = {
          min : 1,
          max : 6
        }
        let isMayusRuleValid = false;
        const minusRule = {
          min : 1,
          max : 6
        }
        let isMinusRuleValid = false;
        const numbersRule = {
          min : 1,
          max : 6
        }
        let isNumbersRuleValid = false;
        let isSpecialCharRuleValid = false;
        const letterConsecutiveRule = {
          min : 0,
          max : 2
        }
        let isConsecutiveRuleValid = false;
        let isSameCharRuleValid = false;
        //variables temporales de validación
        let temp = '';
        let next = '';
        let previous = '';
        //Password array
        var passwordValidity = password.split('');

        angular.forEach(passwordValidity, function(caracter){


          if(/^[a-zA-Z0-9]*$/.test(caracter) === false) specialCharCounter++;
          else if(caracter === caracter.toUpperCase() && isNaN(parseInt(caracter))) mayusCounter++;
          else if (caracter === caracter.toLowerCase() && isNaN(parseInt(caracter))) minusCounter++;
          else if (!isNaN(parseInt(caracter))) numberCounter++;
          //console.log("Codigo: ", caracter, caracter.charCodeAt(0));
          if(temp === ''){
            temp = caracter.toLowerCase().charCodeAt(0);
            next = temp+1;
            previous = temp-1;
          }
          else if (temp === caracter.toLowerCase().charCodeAt(0)){
            letrasConsecutivas = 0;
            sameCharCounter++;
            //console.log("Caracteres iguales ", temp, caracter);
            temp = caracter.toLowerCase().charCodeAt(0);
            next = temp+1;
            previous = temp-1;
          }else if (next == caracter.toLowerCase().charCodeAt(0)){
            letrasConsecutivas += 2;
            if(letrasConsecutivas > 3){
              consecutiveCounter++;
            }
            //consecutiveCounter++;
            //console.log("Caracteres consecutivos ascendentes ", temp, caracter);
            temp = caracter.toLowerCase().charCodeAt(0);
            next = temp+1;
            previous = temp-1;
          }else if (previous == caracter.toLowerCase().charCodeAt(0)){
            letrasConsecutivas += 2;
            if(letrasConsecutivas > 3){
              consecutiveCounter++;
            }
            //console.log("Caracteres consecutivos descendentes ", temp, caracter);
            temp = caracter.toLowerCase().charCodeAt(0);
            next = temp+1;
            previous = temp-1;
          }else{
            letrasConsecutivas = 0;
            temp = caracter.toLowerCase().charCodeAt(0);
            next = temp+1;
            previous = temp-1;
          }
          //console.log("valor", temp);
          //console.log("siguiente ", next);
          //console.log("anterior", previous);

          //**************************
          // Validación de reglas
          //**************************

          //Validación de regla de mayúsculas
          if(mayusCounter >= mayusRule.min && mayusCounter <= mayusRule.max) {
            isMayusRuleValid = true;
            $rootScope.$broadcast("mayus_rule_valid");
          }else{
            isMayusRuleValid = false;
            $rootScope.$broadcast("mayus_rule_invalid");
          }

          //Validación de regla de minúsculas
          if(minusCounter >= minusRule.min && minusCounter <= minusRule.max){
            isMinusRuleValid = true;
            $rootScope.$broadcast("minus_rule_valid");
          }else{
            isMinusRuleValid = false;
            $rootScope.$broadcast("minus_rule_invalid");
          }

          //Validación de regla de números
          if(numberCounter >= numbersRule.min && numberCounter <= numbersRule.max){
            isNumbersRuleValid = true;
            $rootScope.$broadcast("num_rule_valid");
          }else{
            isNumbersRuleValid = false;
            $rootScope.$broadcast("num_rule_invalid");
          }

          //Checa que no tenga Special Chars
          if(specialCharCounter === 0){
            isSpecialCharRuleValid = true;
            $rootScope.$broadcast("special_rule_valid");
          }else{
            isSpecialCharRuleValid = false;
            $rootScope.$broadcast("special_rule_invalid");
          }

          //Checa que no tenga más de 2 consecutivos
          if(letrasConsecutivas >= letterConsecutiveRule.min && letrasConsecutivas <= letterConsecutiveRule.max && consecutiveCounter === 0){
            isConsecutiveRuleValid = true;
            $rootScope.$broadcast("consecutive_rule_valid");
          }else{
            isConsecutiveRuleValid = false;
            $rootScope.$broadcast("consecutive_rule_invalid");
          }

          //Checa que no tenga 2 letras iguales continuas
          if(sameCharCounter === 0 ){
            isSameCharRuleValid = true;
            $rootScope.$broadcast("iguales_rule_valid");
          }else{
            isSameCharRuleValid = false;
            $rootScope.$broadcast("iguales_rule_invalid");
          }

          let characterGroupCount = isMayusRuleValid + isMinusRuleValid + isNumbersRuleValid + isSameCharRuleValid + isConsecutiveRuleValid + isSpecialCharRuleValid

          //Valida que tenga 8 caracteres
          if(password.length === 8){
            $rootScope.$broadcast("length_rule_valid");
            if(characterGroupCount < 6){
              $rootScope.$broadcast("password_strength_0");
            }else{
              if(mayusCounter > 1 && minusCounter > 1 || minusCounter > 1 && numberCounter > 1 || numberCounter > 1 && mayusCounter > 1) $rootScope.$broadcast("password_strength_2");
              else $rootScope.$broadcast("password_strength_1");
            }
          }else{
            $rootScope.$broadcast("length_rule_invalid");
            $rootScope.$broadcast("password_strength_0");
          }
          //Niveles de contraseña


          //Consola de errores
          //console.log("Mayus ", isMayusRuleValid, mayusCounter);
          //console.log("Minus ", isMinusRuleValid, minusCounter);
          //console.log("Numbers ", isNumbersRuleValid, numberCounter);
          //console.log("Special ", isSpecialCharRuleValid, specialCharCounter);
          //console.log("Same ", isSameCharRuleValid, sameCharCounter);
          //console.log("Consectivo ", isConsecutiveRuleValid, letrasConsecutivas);


        });
        let characterGroupCount = isMayusRuleValid + isMinusRuleValid + isNumbersRuleValid + isSameCharRuleValid + isConsecutiveRuleValid + isSpecialCharRuleValid //= regex + sameChar + noConsecutivo + noUnderScore;
        //console.log(characterGroupCount);
        //Verifica que sea mayor a 8
        if ((password.length >= 8) && (characterGroupCount == 6)) {
          ctrl.$setValidity('complexity', true);
          return password;
        }
        else {
          ctrl.$setValidity('complexity', false);
          return undefined;
        }
      });
    }
  }
});
