var app = angular.module('isbanWebApp');


app.directive('noSpecialChar',
    function noSpecialChar() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function(inputValue) {
          if (inputValue === undefined) return '';
          if(attrs.id == "TXT_DatosPersonales_Nombre"){
            cleanInputValue = inputValue.replace(/[^\w\s\ñ\.]/gi, '');
          }else{
            cleanInputValue = inputValue.replace(/[^\w\s\ñ]/gi, '');
          }
          cleanInputValue = cleanInputValue.replace(/[\_]/gi, '');
          if (cleanInputValue != inputValue) {
            modelCtrl.$setViewValue(cleanInputValue);
            modelCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    }
  });
