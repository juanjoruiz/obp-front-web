var app = angular.module('isbanWebApp');

app.directive('spinner', [function(){
  return {
    restrict: 'E',
    templateUrl: 'js/shared/spinner/spinner.view.html',
    controllerAs: 'spinner',
    controller: function(){
      var spinner = this;
    }
  }
}]);
