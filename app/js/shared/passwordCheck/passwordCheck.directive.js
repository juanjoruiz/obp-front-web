var app = angular.module('isbanWebApp');

app.directive('passwordCheck', ['$parse',function($parse){
  return{
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl){
      scope.$watch(function(){
        return $parse(attr.passwordCheck)(scope) === ctrl.$modelValue;
      }, function(currentValue){
        ctrl.$setValidity('passwordMatch', v);
      });
    }
  }
}]);
