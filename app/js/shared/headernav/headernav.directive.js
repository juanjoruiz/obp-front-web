var app = angular.module('isbanWebApp');

app.directive('headernav', function(){
  return {
    restrict: 'EA',
    templateUrl: 'js/shared/headernav/headernav.view.html',
    controller: function($scope){
      $scope.mensaje = "Los datos personales aquí recabados se encuentran protegidos de conformidad con lo establecido en el aviso de privacidad."
    }
  };
});
