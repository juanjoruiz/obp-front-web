var app = angular.module('isbanWebApp');

app.directive('match', ['$parse', function($parse) {
  return {
    require: 'ngModel',
    scope: {
      otherModelValue: "=match"
    },
    link: function(scope, elem, attrs, ngModel) {
      ngModel.$validators.mismatch = function(modelValue, viewValue){

        if((viewValue === undefined || viewValue === "") && (scope.otherModelValue === undefined || scope.otherModelValue === "")) {

          return true;
        }
        else if(viewValue === scope.otherModelValue && (viewValue !== undefined || viewValue !== "")){

          return true;
        }
        else {
          
          return false;
        }
      }

      scope.$watch("otherModelValue", function(value){

        ngModel.$validate();

      });

    }
  };
}]);
