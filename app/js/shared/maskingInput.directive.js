(function(){
  angular.module('isbanWebApp')
    .directive('masking', masking);

  function masking() {
           return {
             //require: 'ngModel',
             restrict: "A",
             link: function(scope, elem, attrs, ctrl) {
                  let number = null;
                 elem.bind("blur", function() {
                   number = elem.val();
                     elem.val(elem.val().slice(0,14).replace(/[0-9]/g, '*') + number.slice(14));
                     console.log(ctrl)
                     ctrl.$setValidity('parse', false);
                 });
                 elem.bind("focus", function() {
                   console.log(number.slice(0,14).replace(/[0-9]/g, '*') + number.slice(14));
                     elem.val(number.slice(0,14).replace(/[0-9]/g, '*') + number.slice(14));
                });


            }
        };
     }
})();

(function(){
  angular.module('isbanWebApp')
    .directive('maskingcvv', maskingcvv);

  function maskingcvv() {
           return {
           restrict: "A",
           link: function(scope, elem, attrs, ctrl) {
             let value = null;
               elem.bind("blur", function() {
                 value = elem.val();
                   elem.val(elem.val().slice(0,3).replace(/[0-9]/g, '*'));
                   ctrl.$setValidity('parse', false);
               });
               elem.bind("focus", function() {
                 elem.val(elem.val().slice(0,3).replace(/[0-9]/g, '*'));
                   elem.val(value.slice(0,3).replace(/[0-9]/g, '*'));
              });


          }
        };
     }
})();
