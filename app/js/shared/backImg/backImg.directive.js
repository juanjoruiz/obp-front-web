var app = angular.module('isbanWebApp');

app.directive('backImg',
    function backImg() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, modelCtrl) {
        var url = attrs.backImg;
        element.css({
            'background-image': 'url(' + url +')',
            'background-size' : 'cover'
        });
      }
    }
  });
