(function(){
  angular.module('isbanWebApp')
    .factory('saveandretrieve', saveandretrieve);

  function saveandretrieve(alertaService, config, $log, localStorageService, connection){
    let saveandretrieve;
    //let saverLog = $log.getInstance('Save and retrive Factory');

    const getDomInfo = zipcode => {
      connection.sendCp(zipcode)
        .then((response) => {
          if(response.estatus !== 0){
            showErrorMessage(response.estatus, response.descripcion);
            identificacion.cpResponse = [];
          }else{
            let cpResponse = response.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;

            let selectedColonia = cpResponse[cpResponse.length - 1];
            localStorageService.set('delegacion', response.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
            localStorageService.set('ciudad', response.datos.lstEstado[0].ciudades[0].ciudad);
            localStorageService.set('estado', response.datos.lstEstado[0].estado);
            localStorageService.set('colonia', cpResponse[cpResponse.length -1] );
          }
        });
    }

    const getEntidad = (nacId, entidadId) => {
      let entidad = {};
      connection.getPaises('Nacimiento_.json')
        .then(function(response){
          if(nacId == '052'){
             angular.forEach(response.datos.estados, (estado) => {
               if(entidadId == estado.id) {
                 entidad = estado
               }
             });
           } else {
             angular.forEach(response.datos.paises, (pais) => {
               if(entidadId == pais.id) {
                  entidad = pais
                }
              });
           }
        }, errorConnection);
        return entidad;
    }

    let datosBasicos = (datos) => {
      //saverLog.info("Cargando información básica");
      let entFedNac = {};
      if(datos.entFedNac){
        entFedNac.id = datos.entFedNac;
      }
      localStorageService.set("nombre", datos.nombrePersona ? datos.nombrePersona : "");
  		localStorageService.set("apellidomaterno", datos.apellidoMaterno ? datos.apellidoMaterno : "");
  		localStorageService.set("apellidopaterno",datos.apellidoPaterno ? datos.apellidoPaterno : "");
  		localStorageService.set("fechanacimiento", datos.fechaNacimiento ? datos.fechaNacimiento : "");
  		localStorageService.set("gender", datos.sexo ? datos.sexo : "");
  		localStorageService.set("paisDeNacimiento", datos.paisNacimiento ? datos.paisNacimiento : "");
  		localStorageService.set("fechanacimiento", datos.fechaNacimiento ? datos.fechaNacimiento : "");
  		localStorageService.set("roluniversitario", { actEspecifica: datos.actEspecifica ? datos.actEspecifica : "" , actGenerica: datos.actGenerica ? datos.actGenerica : "" });
  		localStorageService.set("entidadnacimiento", entFedNac );
  		localStorageService.set("email", datos.mail ? datos.mail : "");
    }
    

    let datosDomicilio = (domicilio) => {
      if(domicilio.codPostal !== undefined){
        localStorageService.set('zipcode', domicilio.codPostal);
        getDomInfo(domicilio.codPostal);
      }
      localStorageService.set('calle', domicilio.nombreVia ? domicilio.nombreVia : "");
      localStorageService.set('numeroexterior', domicilio.numeroExterior ? domicilio.numeroExterior : "");
      localStorageService.set('numerointerior', domicilio.numeroInterior ? domicilio.numeroInterior : "");
      localStorageService.set('retrieveasentamiento', domicilio.asentamiento ? domicilio.asentamiento : "");
    }

    let setData = (datos) => {
      localStorageService.set('nuevaContratacion', false);
      localStorageService.set('idSolicitud', datos.idSolicitud);
      localStorageService.set('idNoCliente', datos.idNocliente);
      localStorageService.set('clienteEditable', datos.datosBasicosEditable);
      //saverLog.info("Los datos del cliente son editables: ", datos.datosBasicosEditable);
      localStorageService.set('beneficiarioEditable', datos.beneficiarioEditable);
      localStorageService.set('branch', datos.idSucursal ? datos.idSucursal : "");
      localStorageService.set('sucursal', datos.nombreSucursal ? datos.nombreSucursal : "");
      localStorageService.set('solicitudRetomada', true);
      if(datos.codigoPersona !== undefined){
        let datosProspecto = {
          codigoPersona: datos.codigoPersona
        }
        localStorageService.set('datosProspecto', datosProspecto);
      }

      if(angular.equals(datos.datosBasicos, {})) console.log("Datos básicos vacíos");
      else datosBasicos(datos.datosBasicos);
      if(datos.domicilio === undefined || angular.equals(datos.domicilio, {})) console.log("Datos de domicilio vacíos");
      else datosDomicilio(datos.domicilio);
      if(datos.paso === 5 || datos.paso === 6) setDatosResumen();
      localStorageService.set('srstepretrive', datos.paso);
      setScreenStepInFlux(getScreenFromStep(datos.paso));

    }

    let setScreenStepInFlux = (pantalla) => {
      let flux = localStorageService.get('flux');
      let savedStep = getScreenIndex(flux.flujo, pantalla) === undefined ? getScreenIndex(flux.flujo2, pantalla) + flux.flujo.length - 1 : getScreenIndex(flux.flujo, pantalla) - 1;
      console.info("Se asignará el paso: ", savedStep);
      localStorageService.set('fluxStep', savedStep);
    }

    let getScreenIndex = (flujo, pantalla) => {
      let step;
      angular.forEach(flujo, function(fluxStep){
        if(fluxStep.pantalla == pantalla) step = flujo.indexOf(fluxStep);
      });
      return step;
    }
    /*
     * getScreenFromStep : Obtiene el paso en la respuesta del back, dado un paso numerico selecciona la pantalla
     * de acuerdo al tipo de mercado, retorna la cadena del $state utilizado para hacer el cambio de pantalla y de paso
     * de acuerdo al flujo de configuración.
     */
    let getScreenFromStep = (step) => {
      switch(step){
        case 1:
          if(localStorageService.get('tipoDeMercado') == '1') return "tarjeta";
          else return "sucursal";
          break;
        case 2:
          if(localStorageService.get('tipoDeMercado') == '1') return "tarjeta";
          else return "sucursal";
          break;
        case 3:
          if(localStorageService.get('tipoDeMercado') == '1') return "tarjeta";
          else return "sucursal";
          break;
        case 4:
          return "perfil";
        case 5:
          if(localStorageService.get('tipoDeMercado') == '1') return "cuenta";
          else if(localStorageService.get('tipoDeMercado') == '3' && localStorageService.get('srdata').vinculacionTarjeta == 'N') return "tarjeta";
          else if(localStorageService.get('srdata').vinculacionTarjeta == 'D' || localStorageService.get('srdata').vinculacionTarjeta == 'S' || localStorageService.get('srdata').vinculacionTarjeta == 'F') return "cuenta";
          else return "tarjeta";
          break;
        case 6:
          localStorageService.set('saveandretriveshowresumen', true);
          return "cuenta";
      }
    }

    let setDatosResumen = () => {
      let srdatos = localStorageService.get('srdata');
      let datosProspecto = {
        nombrePersona: srdatos.nombrePersona ? srdatos.nombrePersona : "" + " " + srdatos.apellidoPaterno ? srdatos.apellidoPaterno : "" + " " + srdatos.apellidoMaterno ? srdatos.apellidoMaterno : "",
        codigoPersona: srdatos.codigoPersona ? srdatos.codigoPersona : ""
      }
      let contratoAceptado = {
        clabe: srdatos.clabe ? srdatos.clabe : "",
        pan: srdatos.pan ? srdatos.pan : "",
        fechaCaducidadTarjeta: srdatos.fechaCaducidadTarjeta ? srdatos.fechaCaducidadTarjeta : "",
        numeroCuenta: srdatos.numeroCuenta ? srdatos.numeroCuenta : ""
      }
      localStorageService.set('datosProspecto', datosProspecto);
      localStorageService.set('contratoAceptado', contratoAceptado);
      localStorageService.set('envioTarjeta', srdatos.vinculacionTarjeta || 'N');
    }

    return {
      //alertar: alertar,
      setData: setData,
      getScreenFromStep: getScreenFromStep
    };

  }

  saveandretrieve.$inject = ["alertaService", "config", "$log", "localStorageService", "connection"];

})();
