var app = angular.module('isbanWebApp');

app.factory('BearerAuthInterceptor', ['$window', '$q', 'localStorageService', function ($window, $q, localStorageService){
    return {
        request: function(config) {
            config.headers = config.headers || {};
            if (localStorageService.get('token') && !localStorageService.get('isOCR')) {
              // may also use sessionStorage
                var token = localStorageService.get('token').access_token;
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config || $q.when(config);
        },
        response: function(response) {
            if (response.status === 401) {
                //  Redirect user to login page / signup Page.
            }
            return response || $q.when(response);
        }
    };
}]);
