(function(){
  var app = angular.module('isbanWebApp');

  app.factory('connection', Connection);



  function Connection($http, localStorageService, aplicacion, $location, $rootScope, $log, $q){
    var connection = {};

    aplicacion.getConf().then(function(response){
      var localfile = 'js/services/';
      var contratoUrl = urlBase + "/OPB/";
      var contratoFin = "/CONTRATOPDF";

      var timeout = 5000;
      let connectionLog = $log.getInstance('Connection Factory');
      var urlBase = ""

      var conExt = response.data.datos.ENV_CON;
      var host = $location.host();
      if(host.match(/.com/g)){
        conExt = 'com';
      }else if(host.match(/.corp/g)){
        conExt = 'corp';
      }else{
        conExt = 'com';
      }


      console.log(response.data.datos.ENV_VAR);

      urlBase = 'https://particulares-gw-obparticularesmx-'+response.data.datos.ENV_VAR+'.appls.cto2.paas.gsnetcloud.'+conExt;
      var cid = {};
      switch(response.data.datos.ENV_VAR){
        case 'dev':
          cid = { client_id : '1034564f-0424-4bc2-abe5-36391ac10693' };
          break;
        case 'pre':
          cid = { client_id : 'b63dae8e-3dc5-4652-a1c1-cb3f3c2b4a29' };
          break;
        case 'pro':
          cid = { client_id : 'bac1590e-688b-48d2-a857-3c30f5cdf10c' };
          break;
      }
      var secUrl = "https://seguridad-obparticularesmx-"+response.data.datos.ENV_VAR+".appls.cto2.paas.gsnetcloud."+ conExt;



      connection.getCopies = (tipoDeMercado) => {
      connectionLog.info('Flujo cargado:', tipoDeMercado);
      let sendData = {
        url: urlBase + "/OPB/copy/" + tipoDeMercado,
        method: 'GET',
        headers: {
          'Content-Type':'application/json;charset=UTF-8'
        }
      }
      return $http(sendData)
              .then(function(response){
                localStorageService.set('copies', response.data.datos);
                connectionLog.info('Se guardaron copies generales');
                connection.getBasicConf('config.json');
                $rootScope.$broadcast('copiesLoaded');
                return response.data;
              })
              .catch(function(error){
                connectionLog.error('Error: ', error);
                return error;
              })
              .finally(function(){
                let thatTime = new Date().getTime() / 1000;
                connectionLog.debug("Terminó de recuperar copies - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
              });
    }
      connection.getSaveRetrieve = function(file){
        var sendData = {
          url: localfile + file,
          method: 'GET',
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }
        return $http(sendData);
      }

      connection.getConf = () => {
        var sendData = {
          url: '/config.json',
          method: 'GET',
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }

        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                .then(function(response){
                  return response.data.datos;
                })
                .catch(function(error){
                  console.error(error);
                })
                .finally(function(){
                  console.log(">>>Fin de configuración");
                });
      }

      connection.getConf = () => {
        var sendData = {
          url: '/config.json',
          method: 'GET',
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }

        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                .then(function(response){
                  return response.data.datos;
                })
                .catch(function(error){
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                })
      }

      connection.getBasicConf = (file) => {
        var sendData = {
          url: localfile + file,
          method: 'GET',
          headers: {
            'Content-Type' : 'application/json;charset=UTF-8'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                .then(function(response){
                  localStorageService.set('flux', response.data.datos);
                  connectionLog.info('Se cargó configuración de flujos');
                  return response.data.datos;
                })
                .catch(function(error){
                  connectionLog.error('Error en configuración básica ', error);
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug('Terminó de cargar configuración base - Tiempo ', (thisTime - thatTime).toFixed(2), ' segundos');
                });
      }

      connection.getButtons = function(file){
        var sendData = {
          url: localfile + file,
          method: 'GET',
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }
        return $http(sendData);
      }

      connection.getContratoPlain = function(){
        var sendData = {
          url: urlBase + "/OPB/CONTRATO/",
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        }
        return $http(sendData);
      }

      connection.login = function(){
        var sendLoginData = {
          url: secUrl + "/seguridad/",
          method: "POST",
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          },
          data: cid
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendLoginData)
                .then(function(response){
                  localStorageService.set('token', response.data);
                  $rootScope.$broadcast('loggedIn');
                })
                .catch(function(error){
                  connectionLog.error('Error en login: ', error);
                  //$rootScope.$broadcast('errorLogin');
                  return error;
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de hacer login oAuth - Timpo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.getActividades = function(file){
        var sendData = {
          url: localfile + file,
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        }

        return $http(sendData);
      }

      connection.getPaises = (file) => {
        var sendData = {
          url: localfile + file,
          method: 'GET',
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                  .then(function(response){
                    return response.data;
                  })
                  .catch(function(error){
                    connectionLog.error('Error en recuperar paises: ', response);
                    return error;
                  })
                  .finally(function(){
                    let thatTime = new Date().getTime() / 1000;
                    connectionLog.debug("Terminó de recuperar paises - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                  });
      }

      connection.getActividad = function(actividad){
        var sendData = {
          url: urlBase + '/OPB/ACTIVIDADES/' + actividad,
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        }
        return $http(sendData);
      }


      connection.getContrato = function(value){
        var sendingData = {
          url: contratoUrl + value + contratoFin,
          method: 'GET',
          responseType: 'arraybuffer'
        }
        return $http(sendingData);
      }

      connection.getBeneficios = function(){
        if(localStorageService.get('tipoDeMercado') !== null) tipoDeMercado = localStorageService.get('tipoDeMercado') == 4 ? 3 : localStorageService.get('tipoDeMercado');

        let sendData = {
          url: urlBase + '/OPB/contrato/'+tipoDeMercado+'/beneficiosCond',
          method: 'GET',
          headers: {
            'Content-Type':'application/json;charset=UTF-8'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                .then(function(response){
                  //return response.data.datos;
                  return response.data;
                })
                .catch(function(error){
                  connectionLog.error('Error en getBeneficios: ', error);
                  return error;
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de recuperar los beneficios y condiciones - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.getBranches = function(lat, lng){
        var sendingData = {
          url: urlBase + '/OPB/obtenerSucursales/'+lat+'/'+lng+'/',
          method: 'GET',
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingData)
                  .then(function(response){
                    return response;
                  })
                  .catch(function(error){
                    connectionLog.error('Error en getBranches: ', error);
                    return error;
                  })
                  .finally(function(){
                    let thatTime = new Date().getTime() / 1000;
                    connectionLog.debug("Terminó de recuperar las sucursales - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                  });
      }

      connection.sendConBuro = function(){
        let data = {
          autorizaConsulta: localStorageService.get('autorizaConsulta'),
          creditoAutomotriz: localStorageService.get('creditoAutomotriz'),
          creditoHipotecario: localStorageService.get('creditoHipotecario'),
          numeroTarjeta: localStorageService.get('numeroTarjeta'),
          tarjetasCredito: localStorageService.get('tarjetasCredito')

        }
        let sendData = {
          url: urlBase + '/OPB/BURO/' + localStorageService.get('idSolicitud'),
          method: 'POST',
          data: data,
          headers: {
            'Content-Type': 'application/json;charset=UTF-8'
          }
        }
        return $http(sendData);
      }

      connection.contactoCliente = () => {
        let esNuevaContratacion = localStorageService.get('nuevaContratacion');
        let solicitudRetomada = localStorageService.get('initsr');
        let sendData = {
          url: urlBase + '/OPB/SOLICITUDES',
          method: 'POST',
          data: {
            ciaTelefono: localStorageService.get('companiaTelefono').id,
            email: localStorageService.get('email'),
            telefono: localStorageService.get('numeroTelefono'),
            producto: localStorageService.get('tipoDeMercado') == 4 ? 3 : localStorageService.get('tipoDeMercado'),
            subProducto: '00'
          },
          headers: {
            'Content-Type' : 'application/json; charset=UTF-8'
          }
        }
        if(esNuevaContratacion) sendData.data.crearNuevaSolicitud = true;
        if(solicitudRetomada) sendData.data.solicitudARetomar = localStorageService.get('idSolicitud');
        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(error){
                  connectionLog.error("Error en envío de contacto: ", error);
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar contacto - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.envioOtroTelefono = () => {
        let sendData = {
          url: urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/' + localStorageService.get('solicitudRetomada') + '/CELULARES',
          method: 'POST',
          data: {
            ciaTelefono: localStorageService.get('companiaTelefono').id,
            telefono: localStorageService.get('numeroTelefono')
          },
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                  .then(function(response){
                    return response.data;
                  })
                  .catch(function(error){
                    connectionLog.error("Error en envio a otro telefono ", response);
                    return $q.reject(error);
                  })
                  .finally(function(){
                    let thatTime = new Date().getTime() / 1000;
                    connectionLog.debug("Terminó de enviar otro telefono - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                  })
      }

      connection.envioMismoTelefono = () => {
        let sendData = {
          url: urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/' + localStorageService.get('solicitudRetomada') + '/CELULARES',
          method: 'POST',
          data: {
            ciaTelefono: localStorageService.get('companiaTelefono').id,
            telefono: localStorageService.get('numeroTelefono'),
            producto: localStorageService.get('tipoDeMercado') == 4 ? 3 : localStorageService.get('tipoDeMercado')
          },
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        }
        let thisTime = new Date().getTime()/ 1000;
        return $http(sendData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(error){
                  connectionLog.error("Error en envío al mismo teléfono ", response);
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar sms al mismo telefono - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                })
      }

      connection.sendSmsToken = () => {
        var sendData = {
          url:urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/CELULARES',
          method:'PUT',
          data:{
            token: localStorageService.get('smsToken'),
            telefono: localStorageService.get('numeroTelefono'),
            solicitudRetomada: localStorageService.get('solicitudRetomada') ? String(localStorageService.get('solicitudRetomada')) : "false"
          },
          headers:{
            'Content-Type': 'application/json;charset=utf-8;'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(response){
                  connectionLog.error("Error en Envío de SMS ", response);
                  return $q.reject(response);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar sms para validarlo - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.sendPanData = function(){
        let tipoDeMercado = localStorageService.get('tipoDeMercado') == 4 ? 3 : localStorageService.get('tipoDeMercado');
        var ext = '/OPB/tarjeta/' + tipoDeMercado + '/' + localStorageService.get('idSolicitud');

        var sendingPanData = {
          url: urlBase + ext,
          method:'POST',
          data:{
            pan: localStorageService.get('pan') ? localStorageService.get('pan') : "",
            fechaVencimiento: localStorageService.get('mes') ? localStorageService.get('mes') + "" + localStorageService.get('anio') : "",
            ccv: localStorageService.get('ccv') ? localStorageService.get('ccv') : "",
            idCentroCosto: localStorageService.get('branch'),
            nombreSucursal: localStorageService.get('sucursal')
          },
          headers:{
            'Content-Type': 'application/json;charset=utf-8;'
          }
        }
        if(tipoDeMercado == 2 || tipoDeMercado == 3 || tipoDeMercado == 4){
          sendingPanData.data.idCentroCosto = localStorageService.get('branch');
        }else if (tipoDeMercado == 1 ) {
          sendingPanData.data.idCentroCosto = "NA";
          sendingPanData.data.nombreSucursal = "NA";
          //delete(sendingPanData.data.idCentroCosto);
          //delete(sendingPanData.data.nombreSucursal);
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingPanData)
                  .then(function(response){
                    return response.data;
                  })
                  .catch(function(response){
                    connectionLog.error("Error en envío de datos ", response);
                    return $q.reject(response);
                  })
                  .finally(function(){
                    let thatTime = new Date().getTime() / 1000;
                    connectionLog.debug("Terminó de enviar datos - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                  });
      }

      connection.createPanData = function(){
        let tipoDeMercado = localStorageService.get('tipoDeMercado') == 4 ? 3 : localStorageService.get('tipoDeMercado');
        var sendingPanData = {
          url: urlBase + '/OPB/tarjeta/' + tipoDeMercado + '/' + localStorageService.get('idSolicitud'),
          method:'POST',
          data:{
            pan: '',
            fechaVencimiento: '',
            ccv: '',
            idCentroCosto: localStorageService.get('branch'),
            nombreSucursal: localStorageService.get('sucursal')
          },
          headers:{
            'Content-Type': 'application/json;charset=utf-8;'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingPanData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(response){
                  connectionLog.error("Error en la creación de tarjeta ", response);
                  return $q.reject(response);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar los datos de creación de tarjeta - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.sendIdentification = function(idFrente, idAtras){
        var data = {};
        var typeId = localStorageService.get('typeId');
        var file = '';
        switch(typeId){
          case 'ine':
            var anverso = idFrente;
            var reverso = idAtras;
            file = localStorageService.get('typeId') + '.png';
            data = {
              imagenes:[
                { base64: anverso.base64 },
                { base64: reverso.base64 }
              ],
              nombre: file
            }
            break;
          case 'pasaporte':
            var pasaporte = idFrente;
            file = localStorageService.get('typeId') + '.png';
            data = {
              imagenes:[
                { base64: pasaporte.base64 }
              ],
              nombre: file
            }
            break;
          default:
            break;
        }
        var sendingIdData = {
          url: urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/DOCUMENTOS',
          method: 'POST',
          data: data,
          headers:{
            'Content-Type': 'application/json;charset=utf-8;'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingIdData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(response){
                  connectionLog.error('Hubo un error en el envío de la identificación:', response);
                  return $q.reject(response);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar el archivo identificación - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.sendCp = function(cp){
        var sendCpData = {
          method:'GET',
          url: urlBase + '/OPB/' + cp,
          headers: {
              'Content-Type': "application/json;charset=UTF-8"
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendCpData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(response){
                  connectionLog.error("Hubo un error recuperando las poblaciones: ", response);
                  return $q.reject(response);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug('Terminó de recuperar poblaciones - Tiempo: ', (thatTime - thisTime).toFixed(2), " segundos");
                });
      }


  connection.sendAcepto = () =>{
    //var tipoDeMercado = ;
    var ligaConexion = '';
    if(localStorageService.get('tipoDeMercado') == 1){
      ligaConexion = urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/NA/N/0/CONTRATO';
    }else if (localStorageService.get('tipoDeMercado') == 2 || localStorageService.get('tipoDeMercado') == 3 || localStorageService.get('tipoDeMercado') == 4) {
      ligaConexion = urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/' + localStorageService.get('branch') +'/' + localStorageService.get('envioTarjeta') + '/0/CONTRATO'
    }
    var sendingData = {
      url: ligaConexion,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json;charset=utf-8;'
      }
    }
    let thisTime = new Date().getTime() / 1000;
    return $http(sendingData)
            .then(function(response){
              return response.data;
            })
            .catch(function(error){
              console.error('Hubo un error al dar de alta el contrato: ', error);
              return $q.reject(error);
            })
            .finally(function(){
              let thatTime = new Date().getTime() / 1000;
              console.debug('Terminó de enviar la aceptación del contrato - Tiempo: ', (thatTime - thisTime).toFixed(2), " segundos");
            });
  }

  connection.sendPass = function(){
    var sendingData = {
      url: urlBase + '/OPB/afiliacionSupermovil',
      method: 'POST',
      data: {
        idSolicitud: localStorageService.get('idSolicitud'),
        idNocliente: localStorageService.get('idNoCliente'),
        nipProvisional: localStorageService.get('password')
      },
      headers: {
          'Content-Type': 'application/json'
      }
    }
    let thisTime = new Date().getTime() / 1000;
    return $http(sendingData)
              .then(function(response){
                return response.data;
              })
              .catch(function(error){
                console.error('Hubo un problema al enviar el password ', error);
                return $q.reject(error);
              })
              .finally(function(){
                let thatTime = new Date().getTime() / 1000;
                console.debug('Terminó de enviar la información para la preafiliación - Tiempo: ', (thatTime - thisTime).toFixed(2), " segundos");
              });
  }
      connection.sendDatosPersona = function(){
        var data = [];
        var rolUniversitario = localStorageService.get('roluniversitario');
        var entidadNacimiento = localStorageService.get('entidadnacimiento');
        var mismoDomicilio = localStorageService.get('beneficiarioMismoDomicilio');

        data = setNoBeneficiario();

        if(localStorageService.get('addBeneficiario') == 's'){
          data = setBeneficiario(data, mismoDomicilio);
        }

        var sendingData = {
          url: urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/CLIENTES',
          method:'POST',
          data: data,
          headers: {
            'Content-Type': 'application/json;charset=utf-8;'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(error){
                  connectionLog.error("Hubo un error al enviar datos de cliente, Error: ", error);
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar datos de cliente - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      var setBeneficiario = function(data, mismoDomicilio){
        var calle = '';
        var ciudad = '';
        var codigo_postal = '';
        var colonia = '';
        var estado = [];
        var delegacion = '';
        var nacionalidad = [];
        var numero_ext = '';
        var numero_int = '';
        var paisDeNacimientoBeneficiario = localStorageService.get('paisDeNacimientoBeneficiario');

        if(localStorageService.get('beneficiarioMismoDomicilio')){
          calle = localStorageService.get('calle').toUpperCase();
          colonia = localStorageService.get('colonia');
          ciudad = localStorageService.get('ciudad');
          codigo_postal = localStorageService.get('zipcode');
          delegacion = localStorageService.get('delegacion');
          numero_ext = localStorageService.get('numeroexterior');
          numero_int = localStorageService.get('interiorbeneficiario') !== null || localStorageService.get('interiorbeneficiario') !== undefined ? localStorageService.get('interiorbeneficiario') : "";
        }else{
          calle = localStorageService.get('callebeneficiario');
          colonia = localStorageService.get('coloniabeneficiario');
          ciudad = localStorageService.get('ciudadbeneficiario');
          codigo_postal = localStorageService.get('zipcodebeneficiario');
          delegacion = localStorageService.get('delegacionbeneficiario');
          numero_ext = localStorageService.get('exteriorbeneficiario');
          numero_int = localStorageService.get('interiorbeneficiario') !== null || localStorageService.get('interiorbeneficiario') !== undefined ? localStorageService.get('interiorbeneficiario') : "" ;
        }

        if(paisDeNacimientoBeneficiario == 'M') {
          estado = localStorageService.get('entidadnacimientobeneficiario');
          nacionalidad.id = '052';
        }else{
          estado.id = 'NE';
          nacionalidad = localStorageService.get('entidadnacimientobeneficiario');
        }

  connection.sendEncuesta = function(){
    var sendingData = {
      url: urlBase + '/OPB/ENCUESTA',
      method: 'POST',
      data: {
        idSolicitud: localStorageService.get('idSolicitud'),
        comentario: localStorageService.get('comments'),
        calificacion: localStorageService.get('rate')
      },
      headers: {
        'Content-Type': 'application/json'
      }
    }
    let thisTime = new Date().getTime() / 1000;
    return $http(sendingData)
            .then(function(response){
              return response.data;
            })
            .catch(function(error){
              console.error('Error al enviar encuesta: ', error);
              return $q.reject(error);
            })
            .finally(function(){
              let thatTime = new Date().getTime() / 1000;
              console.debug("Terminó de enviar la encuesta - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
            });
  }

        var beneficiario = {
          calle: calle ,
          ciudad: ciudad,
          codigo_postal: codigo_postal,
          colonia: colonia.asentamiento,
          estado: estado.id,
          delegacion: delegacion,
          fechaNacimiento: localStorageService.get('fechanacimientobeneficiario'),
          materno: localStorageService.get('apellidomaternobeneficiario'),
          mismoDomicilioTitular: localStorageService.get('beneficiarioMismoDomicilio'),
          nacionalidad: nacionalidad.id,
          nombre: localStorageService.get('nombrebeneficiario'),
          numero_ext: numero_ext,
          numero_int: numero_int ? numero_int : "",
          paterno: localStorageService.get('apellidopaternobeneficiario')
        }

        data.beneficiario = beneficiario;

        return data;
      }

      var setNoBeneficiario = function(){
        var lada = localStorageService.get('numeroTelefono').toString().substring(0,3);
        var telefono = localStorageService.get('numeroTelefono').toString().substring(3);
        var entidad = [];
        var pais = [];
        var paisDeNacimiento = localStorageService.get('paisDeNacimiento');
        if(paisDeNacimiento == 'M' || paisDeNacimiento == '052'){
          pais.id = '052';
          entidad = localStorageService.get('entidadnacimiento');
        }else{
          entidad.id = 'NE';
          pais = localStorageService.get('entidadnacimiento');
        }
        let apellidoMat = localStorageService.get('apellidomaterno');
        if(apellidoMat === undefined || apellidoMat === null ) {
          apellidoMat = "";
          localStorageService.set('apellidomaterno', "");
        }

        let datosBasicosNode = {};
        if(localStorageService.get('email') === undefined || localStorageService.get('email') === null || localStorageService.get('email') === ""){
          datosBasicosNode = {
            actEspecifica: localStorageService.get('roluniversitario').actEspecifica,
            actGenerica: localStorageService.get('roluniversitario').actGenerica,
            apellidoMaterno: apellidoMat ? apellidoMat : "",
            apellidoPaterno: localStorageService.get('apellidopaterno').toUpperCase(),
            entFedNac: entidad.id, //codigo del pais
            fechaNacimiento: localStorageService.get('fechanacimiento'),
            mail: "",
            nacionalidad: '052',
            nombrePersona: localStorageService.get('nombre').toUpperCase(),
            paisNacimiento: pais.id, //nacionalidad
            sexo: localStorageService.get('gender').toUpperCase()
          }
        }else{
          datosBasicosNode = {
            actEspecifica: localStorageService.get('roluniversitario').actEspecifica,
            actGenerica: localStorageService.get('roluniversitario').actGenerica,
            apellidoMaterno: apellidoMat, //? apellidoMat : "",
            apellidoPaterno: localStorageService.get('apellidopaterno').toUpperCase(),
            entFedNac: entidad.id, //codigo del pais
            fechaNacimiento: localStorageService.get('fechanacimiento'),
            mail: localStorageService.get('email'),
            nacionalidad: '052',
            nombrePersona: localStorageService.get('nombre').toUpperCase(),
            paisNacimiento: pais.id, //nacionalidad
            sexo: localStorageService.get('gender').toUpperCase()
          }
        }
        var data = {
          datosBasicos: datosBasicosNode,
          domicilio: {
            asentamiento: localStorageService.get('colonia').nombre.toUpperCase(),
            ciudad: localStorageService.get('ciudad'),
            codPostal: localStorageService.get('zipcode'),
            delegacion: localStorageService.get('delegacion'),
            estadoMX: localStorageService.get('estado'),
            nombreVia: localStorageService.get('calle').toUpperCase(),
            numeroExterior: localStorageService.get('numeroexterior'),
            numeroInterior: localStorageService.get('numerointerior') ? localStorageService.get('numerointerior') : "",
            tipoAsentamiento: localStorageService.get('colonia').asentamiento
          },
          esUniversitario: "N",
          sucursal: localStorageService.get('branch') ? localStorageService.get('branch') : "",
          telefono: {
            ciaTelefono: localStorageService.get('companiaTelefono').id,
            telefono: telefono,
            telefonoLada: lada
          }
        }
        return data;
      }

      connection.sendAcepto = () =>{
        //var tipoDeMercado = ;
        var ligaConexion = '';
        if(localStorageService.get('tipoDeMercado') == 1){
          ligaConexion = urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/NA/N/0/CONTRATO';
        }else if (localStorageService.get('tipoDeMercado') == 2 || localStorageService.get('tipoDeMercado') == 3 || localStorageService.get('tipoDeMercado') == 4) {
          ligaConexion = urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/' + localStorageService.get('branch') +'/' + localStorageService.get('envioTarjeta') + '/0/CONTRATO'
        }
        var sendingData = {
          url: ligaConexion,
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json;charset=utf-8;'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(error){
                  connectionLog.error('Hubo un error al dar de alta el contrato: ', error);
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug('Terminó de enviar la aceptación del contrato - Tiempo: ', (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.sendPass = function(){
        var sendingData = {
          url: urlBase + '/OPB/afiliacionSupermovil',
          method: 'POST',
          data: {
            idSolicitud: localStorageService.get('idSolicitud'),
            idNocliente: localStorageService.get('idNoCliente'),
            nipProvisional: localStorageService.get('password')
          },
          headers: {
              'Content-Type': 'application/json'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingData)
                  .then(function(response){
                    return response.data;
                  })
                  .catch(function(error){
                    connectionLog.error('Hubo un problema al enviar el password ', error);
                    return $q.reject(error);
                  })
                  .finally(function(){
                    let thatTime = new Date().getTime() / 1000;
                    connectionLog.debug('Terminó de enviar la información para la preafiliación - Tiempo: ', (thatTime - thisTime).toFixed(2), " segundos");
                  });
      }

      connection.getImagesData = () => {
        const sendingData = {
          url: urlBase + '/OPB/NIP/Imagenes/' + localStorageService.get('idSolicitud') + '/',
          method: 'GET',
          headers: {
            'Content-Type' : 'application/json;charset=utf-8;'
          }
        }

        return $http(sendingData);
      }

      connection.saveNip = () => {
        const sendingData = {
          url: urlBase + '/OPB/NIP/',
          method: 'POST',
          headers: {
            'Content-Type' : 'application/json;charset=utf-8;'
          },
          data: {
            idSolicitud: localStorageService.get('idSolicitud'),
            pin: localStorageService.get('selectedNumbers')
          }
        }
        return $http(sendingData);
      }

      connection.sendEncuesta = function(){
        var sendingData = {
          url: urlBase + '/OPB/ENCUESTA',
          method: 'POST',
          data: {
            idSolicitud: localStorageService.get('idSolicitud'),
            comentario: localStorageService.get('comments'),
            calificacion: localStorageService.get('rate')
          },
          headers: {
            'Content-Type': 'application/json'
          }
        }
        let thisTime = new Date().getTime() / 1000;
        return $http(sendingData)
                .then(function(response){
                  return response.data;
                })
                .catch(function(error){
                  connectionLog.error('Error al enviar encuesta: ', error);
                  return $q.reject(error);
                })
                .finally(function(){
                  let thatTime = new Date().getTime() / 1000;
                  connectionLog.debug("Terminó de enviar la encuesta - Tiempo: ", (thatTime - thisTime).toFixed(2), " segundos");
                });
      }

      connection.getAccountData = function(){
        var sendingData = {
          url: urlBase + '/OPB/' + localStorageService.get('idSolicitud') + '/' + localStorageService.get('idNoCliente') + '/CONTRATO',
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        }
        return $http(sendingData);
      }
    })
    .catch(function(){
      console.error("Error al recuperar configuración para conexiones.");
    })
    .finally(function(){
      console.debug("Terminó de correr la carga de configuración.");
    });
    return connection;


  }

  Connection.$inject = ['$http', 'localStorageService', 'aplicacion', '$location', '$rootScope', '$log', '$q' ];
})();
