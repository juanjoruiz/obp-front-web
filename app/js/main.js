var app = angular.module('isbanWebApp', ['ngAria','ui.router','ngMaterial', 'ngMessages', 'LocalStorageModule', 'naif.base64', 'ui.bootstrap', 'angular-momentjs', 'tbImageProcessor', 'mdo-angular-cryptography', 'base64', 'ui.mask']);

//app.run(getConf);
//app.run(disableRefresh);
app.run(logEnhancer);
app.run(disableBack);

function getConf($rootScope, aplicacion){
  aplicacion.getConf().then(function(response){
    console.log(response.data.datos);
    $rootScope.version = response.data.datos.version;
    $rootScope.ENV_VAR = response.data.datos.ENV_VAR;
  });
}

function disableRefresh($rootScope, $location, config){
  window.onbeforeunload = function () {
    return "Some of the text in the popup. The rest is controlled by the browser"
  };


}

function disableBack($rootScope, $location, config){
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    if(toState.name == 'home'){
      $rootScope.previousState = undefined;
    }else if(toState.name == $rootScope.previousState) {
      let message = {
        id: 200,
        evento: event
      }
      config.generalMessages(message);
      event.preventDefault();
    }else{
      $rootScope.previousState = fromState.name;
    }
  });
}

function getConf(aplicacion){
  aplicacion.getConf();
}

function logEnhancer($log, logger, $state, $window, $rootScope, $location, localStorageService, aplicacion){
  logger.enhanceLog($log);


  $rootScope.$on('$locationChangeSuccess', function() {
       $rootScope.actualLocation = $location.path();
   });

  $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
       if($rootScope.actualLocation === newLocation) {
         let flux = localStorageService.get('flux');
         localStorageService.set('fluxStep', 1);
         let step = localStorageService.get('fluxStep');
       }
   });
}

app.config(["localStorageServiceProvider","$stateProvider", "$urlRouterProvider", "$mdThemingProvider", "$httpProvider", "$cryptoProvider", "loggerProvider", "$provide",
function(localStorageServiceProvider, $stateProvider, $urlRouterProvider, $mdThemingProvider, $httpProvider, $cryptoProvider, loggerProvider, $provide){
  //Paleta de color institucional
  var redPalette = $mdThemingProvider.extendPalette('red', {
    '500': '#ec0000'
  });

  $cryptoProvider.setCryptographyKey('obparticulares');

  $mdThemingProvider.definePalette('redPalette', redPalette);
  $mdThemingProvider.theme('default').primaryPalette('redPalette');
  //routing
  $urlRouterProvider.when('', '/');
  $stateProvider.state('home', {
    url: '/',
    template: '<div landing>',
    resolve: {
      clearAll: function(localStorageService){
        return localStorageService.clearAll();
      },
      configuracion: function(clearAll, $rootScope, aplicacion){
        aplicacion.getConf().then(function(response){
          console.log(response.data.datos);
          $rootScope.version = response.data.datos.version;
          $rootScope.ENV_VAR = response.data.datos.ENV_VAR;
          $rootScope.flujo_universidades_bloqueado = response.data.datos.flujo_universidades_bloqueado;
          $rootScope.flujo_nomina_bloqueado = response.data.datos.flujo_nomina_bloqueado;
          $rootScope.flujo_mercado_abierto_bloqueado = response.data.datos.flujo_mercado_abierto_bloqueado;
        });
      }
    }
  })
  .state('carrusel',{
    url: '/beneficios/:tipo',
    template: '<div carrusel>',
    params: {
      tipo: undefined
    }
  })
  .state('campuspay', {
    url: '/campuspay/:data',
    params: {
      data: undefined
    },
    template: '<div formcampuspay>'
  })
  .state('beneficios', {
    url: '/condiciones/:tipo',
    params: {
      tipo: undefined
    },
    template: '<div benycond>',
    resolve: {
      login: function($rootScope, connection, localStorageService, $stateParams){
        localStorageService.set('tipoDeMercado', $stateParams.tipo);
        if($stateParams.tipo == '2') $rootScope.$broadcast('fbpixelon');
        if(localStorageService.get('config')){
          connection.getConf()
            .then(function(response){
              localStorageService.set('config', response);
            })
        }
        return connection.login();
      }
    }
  })
  .state('contacto',{
    url: '/contacto',
    template: '<div formcontacto>',
    resolve: {
      flujo: function(connection, localStorageService){
        let file = "config" + (localStorageService.get('tipoDeMercado') == 1 ? 'U.json' : localStorageService.get('tipoDeMercado') == 2 ? 'N.json' : localStorageService.get('tipoDeMercado') == 3 ? 'M.json' : localStorageService.get('tipoDeMercado') == 4 ? 'A.json' : '.json') ;
        return connection.getBasicConf(file);
      }
    },
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('sucursal', {
    url: '/sucursal',
    template: '<div seleccionasucursal style="height:100%;">',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('identificacion', {
    url: '/identificacion',
    template: '<div seleccionaidentificacion>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('tarjeta', {
    url: '/tarjeta',
    template: '<div seleccionatarjeta>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('perfil', {
    url: '/perfil',
    template: '<div formcliente>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('cuenta', {
    url: '/cuenta',
    template: '<div cuenta>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('tarjetaDigital', {
    url: '/tarjetaDigital',
    template: '<div muestratarjeta>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('bancaDigital', {
    url: '/bancaDigital',
    template: '<div banca-digital>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('calificarexperiencia', {
    url: '/encuesta',
    template: '<div calificarexperiencia>',
    onEnter: function($state, localStorageService){
      if(!localStorageService.get('termsAccepted')){
        $state.go('home');
      }
    }
  })
  .state('datosPersonales',{
    url:'/datos-personales',
    params: {
      momentum: undefined
    },
    template: '<personales>'
  })
  .state('terminosCondiciones',{
    url:'/terminos-condiciones',
    template: '<terminos-condiciones>'
  })
  .state('testServices', {
    url: ':mercado/test-services',
    template: '<test-services>'
  })
  .state('descargaContrato',{
    url:'/descarga-contrato',
    template:'<sharecontrato>'
  })
  .state('mapa',{
    url:'/mapa',
    template:'<branch-locator>'
  })
  .state('buroCredito',{
    url: '/buro-credito',
    template: '<buro-credito>'
  });
  loggerProvider.loggingPattern = '%s:[%s]: ';
  $httpProvider.interceptors.push('BearerAuthInterceptor');
  $httpProvider.interceptors.push('httpSpinnerInterceptor');
  localStorageServiceProvider
    .setPrefix('isbanWebAppStorage')
    .setDefaultToCookie(true)
    .setStorageCookie(45, '/', true)
    .setStorageCookieDomain(window.location.href)
    .setNotify(true, true);
  $provide.constant('$MD_THEME_CSS', '/**/');
}]);

app.constant("baseConfig", {
    "version" : "2.1.5",
    "ENV" : "pro",
    "ENV_CON" : "com",
    "mobileWidth" : '414', //iPhone6 Plus
    "mobileHeight" : '736',
    "tabletLandscapeWidth" : '1024', //iPad Pro
    "tabletLandscapeHeight" : '1366',
    "tabletPortraitWidth" : '1366',
    "tabletPortraitHeight" : '1024',
    "desktopWidth" : '1366', //Standard 2016
    "desktopHeight" : '768',
    "smsVisible" : false,
    "mobile" : false,
    "tablet" : false,
    "desktop" : false
  });

app.controller('MainController', MainController);

  function MainController($rootScope, $scope, baseConfig, $window, config, $log, localStorageService, aplicacion, $location, $state, $stateParams){

    $scope.$on("$stateChangeSuccess", function () {
      setTimeout( function(){
        console.log($location.path());
        if($location.path() == "/beneficios/" + $stateParams.tipo ){
          if($window.innerWidth > 414 || !$stateParams.tipo == 3){
            let args = {
              pantalla: "carrusel",
              opcion: "saltar"
            }
            $rootScope.$broadcast('CarruselCall', args);
          }
        }

      },500);
    });




    function switchOnGeolocation() {
          if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(pos){
              let message = {}
              if(pos.coords.latitude === null){
                 message = {
                  id:998,
                  descripcion: "Necesitas activar tu GPS"
                }
              } /*else {
                message = {
                  id:998,
                  descripcion: "GPS activado"
                }
              }*/
              config.generalMessages(message);
            }, showError);
          }else{
            let message = {
              id:998,
              descripcion: "Tu navegador no soporta el GPS"
            }
            config.generalMessages(message);
          }
        }

        function showError(error) {
          let message = {};
          switch(error.code) {
            case error.PERMISSION_DENIED:
              message = {
                id:998,
                descripcion: "El usuario negó el permiso de geolocalización."
              }
              break;
            case error.POSITION_UNAVAILABLE:
              message = {
                id:998,
                descripcion: "La información de geolocalización no está disponible."
              }
              break;
            case error.TIMEOUT:
              message = {
                id:998,
                descripcion: "Expiró el tiempo de espera de la geolocalización."
              }
              break;
            case error.UNKNOWN_ERROR:
              message = {
                id:998,
                descripcion: "Expiró el tiempo de espera de la geolocalización."
              }
              break;
          }
          config.generalMessages(message);
        }

    switchOnGeolocation();

    localStorageService.set('isOCR', false);

    $rootScope.$on('$locationChangeSuccess', function() {
         $rootScope.actualLocation = $location.path();
     });

      let mainLog = $log.getInstance('');
      $scope.showfbpixel = false;

      //$scope.$on('fbpixelon', function(){
      if(localStorageService.get('tipoDeMercado') == '2'){
        console.log('nomina');
        $scope.showfbpixel = true;
      }
      //});

      $window.addEventListener("load",function() {
        setTimeout(function(){
            $window.scrollTo(0, 1);
        }, 0);
      });

      angular.element($window).bind('resize', function(){
        setDevice($window.innerWidth);
        setSpace($window.innerWidth, $window.innerHeight);
        $scope.$digest();
      });

      var setSpace = function(width, height){
        $('body').css("max-width", width);
        $('body').css("height", height);
      }

      $scope.$on('CarruselCall', function(event, args){
        console.log("Cambio de pantalla ", args);
        $state.go('beneficios', {tipo:$stateParams.tipo});
      });

      $scope.$on('BackToHome', function(event, args){

        console.log(args);
        $state.go('home');
      })


      $scope.$on('errorLogin', function(){
        let error = {
          status: 401,
          descripcion: "",
          errorCode: "401",
          titulo: "Login"
        }
        config.generalErrors(error);
      });

      $scope.$on('500', function(){
        let error = {
          status: 500,
          descripcion: "",
          errorCode: "500",
          tipo: "alerta",
          titulo: "Error de conexión"
        }
        config.generalErrors(error);
      });

      $scope.$on('-1', function(){
        let error = {
          status: 500,
          descripcion: "",
          errorCode: "503",
          tipo: "alerta",
          titulo: "Error de conexión"
        }
        config.generalErrors(error);
      });

      $scope.$on('isMobile', function(){
        $rootScope.settings = {
          mobile: true,
          tablet: false,
          desktop: false
        }
      });


      $scope.$on('isTablet', function(){
        $rootScope.settings = {
          mobile: false,
          tablet: true,
          desktop: false
        }
      });

      $scope.$on('isDesktop', function(){
        $rootScope.settings = {
          mobile: false,
          tablet: false,
          desktop: true
        }
      });

      //Setting de tamaño de pantalla
      function setDevice(width){

        if(width <= baseConfig.mobileWidth){
          $rootScope.$broadcast('isMobile');
        }else if (width <= baseConfig.tabletLandscapeWidth) {
          $rootScope.$broadcast('isTablet');
        }else if (width <= baseConfig.tabletPortraitWidth) {
          $rootScope.$broadcast('isTablet');
        }else if (width > baseConfig.desktopWidth) {
          $rootScope.$broadcast('isDesktop');
        }else{
          $rootScope.$broadcast('isDesktop');
        }
      }

      setDevice($window.innerWidth);
      setSpace($window.innerWidth, $window.innerHeight);

        aplicacion.getConf().then(function(response){
          console.log(response.data.datos);
          $rootScope.version = response.data.datos.version;
          $rootScope.ENV_VAR = response.data.datos.ENV_VAR;
          $rootScope.extraccionEncendida = response.data.datos.extraccion_encendida;
        });

        let userAgent = window.navigator.userAgent;
        let ieReg = /msie|Trident.*rv[ :]*11\./gi;
        let ie = ieReg.test(userAgent);
        if(ie){
          let message = {
            id: 2,
            titulo: "Experimenta al máximo la creación de tu cuenta.",
            descripcion: "Aprovecha las funciones que tenemos para ti, abre la aplicación en <a href='https://www.google.com.mx/chrome/browser/desktop/index.html' target='_blank'>Google Chrome</a>, <a href='https://www.mozilla.org/es-MX/firefox/new/' target='_blank'>Firefox</a> o en tu celular."
          }
          config.generalMessages(message);
        }
}

MainController.$inject = ['$rootScope','$scope', 'baseConfig', '$window', 'config', '$log', 'localStorageService', 'aplicacion', '$location', "$state", "$stateParams"];
