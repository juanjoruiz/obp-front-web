(function(){
  angular.module('isbanWebApp')
    .controller('condicionesController', condicionesController);

  function condicionesController($scope, $stateParams, localStorageService, connection, avisoService, $state, $log, baseConfig, $rootScope){

    let vm = this;
    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_Condiciones_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_Condiciones_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_Condiciones_MA_'+env});
          break;
        case '4':
          utag.view({page:'/OBP_Condiciones_MA_'+env});
          break;
      }
    }



    vm.exceptions = [];

    $scope.$on('copiesLoaded', function(){
      let copies = localStorageService.get('copies');
      vm.condicionesTitle = copies.condiciones.title;
      vm.otrasCondiciones = copies.condiciones.title2;
      vm.condiciones = copies.condiciones.subtitle;
      newException(copies.condiciones.notaPrivacidad, undefined);

      vm.showAvisoPrivacidad = () => {
        console.info('Se mostró el aviso de privacidad');
        let options = {
          actionButtonText: 'Aceptar y continuar',
          showOk: true,
          headerText: copies.avisoPrivacidad.title,
          bodyText: copies.avisoPrivacidad.item1
        }
        return avisoService.showModal({}, options)
                  .then(function(response){
                    vm.termsAcceptance();
                  }, function(response){
                    console.log();
                  });
      }
    });

    $scope.$on('benycondLoaded', function(){
      let condiciones = localStorageService.get('benycond');
      vm.condicionesList = condiciones.condicionesLista;
      vm.otraList = condiciones.condicionesOtrasLista;
    });

    vm.termsAcceptance = () => {
      let flux = localStorageService.get('flux');
      localStorageService.set('termsAccepted', true);
      console.info('Se aceptaron términos y condiciones');
      localStorageService.set('fluxStep', 1);
      $state.go(flux.flujo[localStorageService.get('fluxStep')].pantalla);
    }

    const newException = (desc, img) => {
      let exception = {
        desc: desc,
        img: img
      }

      vm.exceptions.push(exception);
    }

  }

  condicionesController.$inject = ['$scope', '$stateParams', 'localStorageService', 'connection', 'avisoService', '$state', '$log', 'baseConfig', '$rootScope'];
})();
