(function(){
  angular.module('isbanWebApp')
    .directive('condiciones', condiciones);

  function condiciones(){
    return{
      restrict: 'EA',
      templateUrl: 'js/components/condiciones/condiciones.view.html',
      controllerAs: 'condicionesCtrl',
      controller: 'condicionesController'
    }
  }
})();
