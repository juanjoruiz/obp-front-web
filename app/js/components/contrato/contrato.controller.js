(function(){
  angular.module('isbanWebApp')
    .controller('ContratoController', ContratoController);

  function ContratoController($rootScope, $log, connection, localStorageService, config, baseConfig){
    let vm = this;
    let copies = localStorageService.get('copies');

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_Contrato_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_Contrato_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_Contrato_MA_'+env});
          break;
        case '4':
          utag.view({page:'/OBP_Contrato_MA_'+env});
          break;
      }
    }

    vm.contratoTitle = copies.contrato.title;
    vm.contratoSubtitle = copies.contrato.subtitle;
    vm.terminos = copies.contrato.terminos;

    connection.getContratoPlain()
      .then(function(response){
        vm.texto = response.data.datos;
      }, function(response){
      });


    vm.next = (value) => {
      if(value === 'aceptado'){
        connection.sendAcepto()
          .then(function(response){
            if(response.estatus === 0){
              localStorageService.set('contratoAceptado', response.datos);
              $rootScope.$broadcast('aceptacionDeContrato');
            }else{
              let error = {
                status: response.estatus,
                descripcion: response.descripcion,
                errorCode: response.codigo,
                tipo: response.tipo,
                titulo: response.titulo
              }
              config.generalErrors(error);
            }
          })
          .catch(function(error){
            let error2 = {
              status: "-2",
              descripcion: "Por el momento el servicio no está disponible, por favor intente de nuevo más tarde"
            }
            config.generalErrors(error2);
          })
          .finally(function(){
            connectionLog.debug("Por el momento el servicio no está disponible, por favor intente de nuevo más tarde");
          });
      }
    }
  }

  ContratoController.$inject = ["$rootScope", "$log", "connection", "localStorageService", "config", "baseConfig"];


})();
