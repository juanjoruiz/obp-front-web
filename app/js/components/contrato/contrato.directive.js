var app = angular.module('isbanWebApp');

app.directive('contrato', function(){
  return {
    restrict: 'A',
    templateUrl: 'js/components/contrato/contrato.view.html',
    controllerAs: "contratoCtrl",
    controller: 'ContratoController'
  };
});
