var app = angular.module('isbanWebApp');

app.directive('resumen', function(){
  return{
    restrict:'A',
    templateUrl: 'js/components/resumen/resumen.view.html',
    controllerAs: 'resumenCtrl',
    controller: 'ResumenController'
  }
});
