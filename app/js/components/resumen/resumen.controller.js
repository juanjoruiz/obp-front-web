(function(){
  angular.module('isbanWebApp')
    .controller('ResumenController', ResumenController);

  function ResumenController($rootScope, $scope, localStorageService, $state, baseConfig, config){
    let vm = this;
    let copies = localStorageService.get('copies');
    let saveandretriveshowresumen = localStorageService.get('saveandretriveshowresumen');

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_CuentaCreada_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_CuentaCreada_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_CuentaCreada_MA_'+env});

          break;
        case '4':
            utag.view({page:'/OBP_CuentaCreada_MA_'+env});

            break;
      }
    }
    if(localStorageService.get('envioTarjeta') == "F") vm.showCard = false;
    else vm.showCard = true;


    vm.resumenTitle = copies.confirmation.title;
    vm.resumenSubtitle = copies.confirmation.subtitle;
    vm.resumenNombre = "NOMBRE DE CLIENTE";
    vm.resumenCodigo = copies.confirmation.codigo;
    vm.resumenTarjeta = copies.confirmation.tarjeta;
    vm.resumenCuenta = copies.confirmation.cuenta;
    vm.resumenClabe = copies.confirmation.clabe;
    vm.resumenSucursal = copies.confirmation.sucursal;

    let setDatos = () => {
      console.log("aceptacion de contrato");
      vm.datosProspecto = localStorageService.get('datosProspecto');
      if(!vm.datosProspecto.hasOwnProperty('nombrePersona')){
        vm.datosProspecto.nombrePersona = localStorageService.get('nombre') + " " + localStorageService.get('apellidopaterno') + " " + localStorageService.get('apellidomaterno');
      }
      vm.datos = localStorageService.get('contratoAceptado');
      //vm.cuenta = vm.datos.clabe.substring(6,17);
      vm.cuenta = vm.datos.numeroCuenta.substring(9,20);
      vm.clabe = vm.datos.clabe;
      vm.card = vm.datos.pan;
      vm.sucursal = localStorageService.get('sucursal');


      vm.mercado = localStorageService.get('tipoDeMercado');
      vm.showSucursal = true;
      if(localStorageService.get('tipoDeMercado') == '1' || localStorageService.get('tipoDeMercado') == '4'){
        vm.showSucursal = false;
      }
    }

    if(saveandretriveshowresumen){
      setDatos();
    }

    $scope.$on('aceptacionDeContrato', function(){
      setDatos();
    });

    vm.screenshot = () => {
      let screenshotView = document.getElementById('VIEW_dataaccount');
      let image = screenshotView.getContext('2d');

      let fileName = "numeroDeCuenta.jpg";

      let link = document.createElement("a");
      link.setAttribute("href", document.getElementById('VIEW_dataaccount').toDataURL());
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();

    }

    vm.generarCsv = () => {
      let fileName = $scope.datosProspecto.codigoPersona + ".csv";
      console.log("Generación de CSV");
      let data = [
        ["",""],
        ["NOMBRE DE CLIENTE", "NUMERO DE CLIENTE", "NUMERO DE TARJETA", "NUMERO DE CLABE", "NUMERO DE CUENTA", "SUCURSAL"],
        [$scope.datosProspecto.nombrePersona, "'"+$scope.datosProspecto.codigoPersona, "'"+$scope.card, "'"+$scope.datos.clabe, "'"+$scope.clabe , $scope.sucursal ]
      ];

      let csvContent = "";
      data.forEach(function(infoArray, index){
        dataString = infoArray.join(",");
        csvContent += index < data.length ? dataString + "\n" : dataString;
      });

      let encodeUri = encodeURI(csvContent);
      let link = document.createElement("a");
      link.setAttribute("href", "data:text/csv;charset=utf-8,%EF%BB%BF" + encodeUri);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
    }

    vm.next = (value) => {
      if(value === 'datosrevisados'){
        //let plusSteps = localStorageService.get('envioTarjeta') == 'F' ? 2 : 1;
        let plusSteps = 1
        config.nextStep(plusSteps);
      }
    }
  }

  ResumenController.$inject = ["$rootScope", "$scope", "localStorageService", "$state", "baseConfig", "config"];
})();
