var app = angular.module('isbanWebApp');

app.directive('passwordchange', function(){
  return{
    restrict: 'A',
    templateUrl: 'js/components/password/password.view.html',
    controllerAs: 'passwordchange',
    controller: 'PasswordChangeController'
  }
});
