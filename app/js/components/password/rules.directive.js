var app = angular.module('isbanWebApp');

app.directive('rules', function(){
  return {
    restrict: 'EA',
    templateUrl: 'js/components/password/rules.view.html',
    controllerAs: 'rules',
    controller: function($scope, localStorageService){
      let vm = this;
      let copies = localStorageService.get('copies');
      console.log(copies);

      vm.classes = {
        check : 'check',
        wrong : 'wrong',
        idle : 'idle'
      }

      vm.list = [
        {
          id: "rule6",
          text: copies.cuenta.item1,
          valid: false,
          invalid: false,
          idle: true
        },
        {
          id: "rule0",
          text: copies.cuenta.item2,
          valid: false,
          invalid: false,
          idle: true
        },
        {
          id: "rule1",
          text: copies.cuenta.item3,
          valid: false,
          invalid: false,
          idle: true
        },
        {
          id: "rule2",
          text: copies.cuenta.item4,
          valid: false,
          invalid: false,
          idle: true
        },
        {
          id: "rule3",
          text: copies.cuenta.item5,
          valid: false,
          invalid: false,
          idle: true
        },
        {
          id: "rule4",
          text: copies.cuenta.item6,
          valid: false,
          invalid: false,
          idle: true
        },
        {
          id: "rule5",
          text: copies.cuenta.item7,
          valid: false,
          invalid: false,
          idle: true
        }
      ]

      $scope.$on('mayus_rule_valid', function(){
        vm.list[0].valid = true;
        vm.list[0].invalid = false;
        vm.list[0].idle = false;
      });
      $scope.$on('mayus_rule_invalid', function(){
        vm.list[0].valid = false;
        vm.list[0].invalid = true;
        vm.list[0].idle = false;
      });
      $scope.$on('minus_rule_valid', function(){
        vm.list[1].valid = true;
        vm.list[1].invalid = false;
        vm.list[1].idle = false;
      });
      $scope.$on('minus_rule_invalid', function(){
        vm.list[1].valid = false;
        vm.list[1].invalid = true;
        vm.list[1].idle = false;
      });
      $scope.$on('num_rule_valid', function(){
        vm.list[2].valid = true;
        vm.list[2].invalid = false;
        vm.list[2].idle = false;
      });
      $scope.$on('num_rule_invalid', function(){
        vm.list[2].valid = false;
        vm.list[2].invalid = true;
        vm.list[2].idle = false;
      });
      $scope.$on('special_rule_valid', function(){
        vm.list[3].valid = true;
        vm.list[3].invalid = false;
        vm.list[3].idle = false;
      });
      $scope.$on('special_rule_invalid', function(){
        vm.list[3].valid = false;
        vm.list[3].invalid = true;
        vm.list[3].idle = false;
      });
      $scope.$on('consecutive_rule_valid', function(){
        vm.list[4].valid = true;
        vm.list[4].invalid = false;
        vm.list[4].idle = false;
      });
      $scope.$on('consecutive_rule_invalid', function(){
        vm.list[4].valid = true;
        vm.list[4].invalid = false;
        vm.list[4].idle = false;
      });
      $scope.$on('iguales_rule_valid', function(){
        vm.list[5].valid = true;
        vm.list[5].invalid = false;
        vm.list[5].idle = false;
      });
      $scope.$on('iguales_rule_invalid', function(){
        vm.list[5].valid = false;
        vm.list[5].invalid = true;
        vm.list[5].idle = false;
      });
      $scope.$on('password_strength_0', function(){
        vm.list[6].valid = false;
        vm.list[6].invalid = true;
        vm.list[6].idle = false;
      });
      $scope.$on('password_strength_1', function(){
        vm.list[6].valid = true;
        vm.list[6].invalid = false;
        vm.list[6].idle = false;
      });





    }
  }
});
