(function(){
  angular.module('isbanWebApp')
    .controller('PasswordChangeController', PasswordChangeController);

  function PasswordChangeController($rootScope, localStorageService, $sce, $scope, connection, $state, config, baseConfig){
    var vm = this;

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_Contrasena_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_Contrasena_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_Contrasena_MA_'+env});
          break;
        case '4':
            utag.view({page:'/OBP_Contrasena_MA_'+env});
            break;
      }
    }

    $scope.level0 = false;
    $scope.level1 = false;
    $scope.level2 = false;
    $scope.rule1Check = false;
    $scope.rule1Wrong = false;
    $scope.rule2Check = false;
    $scope.rule2Wrong = false;
    $scope.rule3Check = false;
    $scope.rule4Wrong = false;
    $scope.logo = "images/icons/semaforo-debil.png"
    $scope.instructions = "<p>Para iniciar genera una contraseña de acceso, misma que debe cumplir con las siguientes condiciones</p>";
    vm.passRegex = "(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";
    vm.tooltip = 'Le recomendamos evidar el uso de datos personales como la fecha de nacimiento, número de tarjeta, código de cliente, número telefónico, etcétera.';

    if(localStorageService.get('tipoDeMercado') == 2){
      $scope.instructions = "<p>Para ingresar a Super<span class='bold red'>Net</span> y Super<span class='bold red'>Móvil</span>, genera una contraseña que cumpla con las siguientes condiciones:";
    }
    vm.renderHtml = function(html_code){
      return $sce.trustAsHtml(html_code);
    }
    vm.savePassword = function(){
      localStorageService.set('password', vm.pass);
    }

    $scope.$on("password_strength_0", function(){
      vm.level0 = true;
      vm.level1 = false;
      vm.level2 = false;
    });
    $scope.$on("password_strength_1", function(){
      vm.level0 = false;
      vm.level1 = true;
      vm.level2 = false;
    });
    $scope.$on("password_strength_2", function(){
      vm.level0 = false;
      vm.level1 = false;
      vm.level2 = true;
    });

    vm.remainingChars = (value) => {
      if(value === undefined){
        return 8;
      }else{
        return 8 - value;
      }
    }

    vm.next = (value) => {
      if(value === 'sendPassword'){
        connection.sendPass()
          .then(function(response){
            if(response.estatus === 0){
              let flux = localStorageService.get('flux');
              let step = localStorageService.get('fluxStep') + 1;
              localStorageService.set('fluxStep', step);
              if(flux.flujo.length > step){
                $state.go(flux.flujo[localStorageService.get('fluxStep')].pantalla);
              }else if (flux.flujo.length < step) {
                if((flux.flujo.length + flux.flujo2.length) <= step) $state.go('calificarexperiencia');
                else $state.go(flux.flujo2[localStorageService.get('fluxStep') - flux.flujo.length].pantalla);
              }
            }else{
              let error = {
                status: response.estatus,
                descripcion: response.descripcion,
                errorCode: response.codigo,
                tipo: response.tipo,
                titulo: response.titulo
              }
              config.generalErrors(error);
            }
          })
      }
    }
  }

  PasswordChangeController.$inject = ["$rootScope","localStorageService", "$sce", "$scope", "connection", "$state", "config", "baseConfig"]
})();
