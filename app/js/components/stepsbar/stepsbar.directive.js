var app = angular.module('isbanWebApp');

app.directive('stepsbar', function(){
  return {
    restrict: 'EA',
    scope: true,
    templateUrl: 'js/components/stepsbar/stepsbar.view.html',
    controller: 'StepsBarController',
    controllerAs: 'stepsCtrl'
  };
});
