(function(){
  angular.module('isbanWebApp')
    .controller('StepsBarController', StepsBarController);

    function StepsBarController($location, $log, localStorageService, $scope, config){
      let stepsLog = $log.getInstance('Steps controller');
      let vm = this;
      stepsLog.info('Carga de barra de pasos');
      vm.separador = "images/steps/arrowGrey.png";

      vm.setFlux = (flux, step) => {
        if(step > flux.flujo.length){
          vm.flujo = 2;
          //if(localStorageService.get('envioTarjeta') == "F") flux.flujo2.splice(2,1);
          return flux.flujo2;
        }else{
          vm.flujo = 1;
          return flux.flujo.slice(1, flux.flujo.length);
        }
      }

      vm.setGrey = (step) => {

      }

      vm.getFlux = () => {
        let flux = localStorageService.get('flux');
        let step = localStorageService.get('fluxStep');
        vm.stepSet = vm.setFlux(flux, step);
        // switch(localStorageService.get('tipoDeMercado')){
        //   case "1":
        //     vm.stepSet = vm.setFlux(flux, step);//[ stepContacto, stepTarjeta, stepIdentificacion, stepPerfil ];
        //     break;
        //   case "2":
        //     vm.stepSet = vm.setFlux(flux, step);//[ stepContacto, stepSucursal, stepTarjeta, stepIdentificacion, stepPerfil ];
        //     break;
        //   case "3":
        //     vm.stepSet = vm.setFlux(flux, step); //[ stepContacto, stepSucursal, stepIdentificacion, stepPerfil, stepTarjeta ];
        //     break;
        //   case "4":
        //     vm.stepSet = vm.setFlux(flux, step);
        //
        // }
      }

      vm.getStep = () => {
        let flux = localStorageService.get('flux');
        //return localStorageService.get('fluxStep');

        if(vm.flujo === 1){
          return localStorageService.get('fluxStep');
        }else{
          return localStorageService.get('fluxStep') - flux.flujo.length;
        }
      }

      vm.getFlux();

      angular.forEach(vm.stepSet, function(step){
        if(step.paso < vm.getStep()){
          step.selectedImage = 'images/steps/' + step.completado;
        }else if (step.paso === vm.getStep()) {
          step.selectedImage = 'images/steps/' + step.activo;
        }else {
          step.selectedImage = 'images/steps/' + step.inactivo;
        }
      });

      vm.getPercentage = function(){

        var percentage = 'width:0%';
        let value = 0;
        let actualStep = vm.getStep();
        let numberOfSteps = vm.stepSet.length;

        value = (100/numberOfSteps)*actualStep;
        // if(vm.flujo == 2){
        //   if(value < 20){
        //     vm.stepSet[0].selectedImage = 'images/steps/20pct.png';
        //   }else if (value > 20 && value <= 40) {
        //     vm.stepSet[0].selectedImage = 'images/steps/40pct.png';
        //   }else if (value > 40 && value <= 60) {
        //     vm.stepSet[0].selectedImage = 'images/steps/60pct.png';
        //   }else if (value > 60 && value <= 80) {
        //     vm.stepSet[0].selectedImage = 'images/steps/80pct.png';
        //   }
        // }

        percentage = 'width:' + value + '%;';
        return percentage;
      }
    }

    StepsBarController.$inject = ['$location', '$log', 'localStorageService', '$scope', 'config'];
})();
