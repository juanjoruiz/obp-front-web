(function(){
  angular.module('isbanWebApp')
    .controller('TarjetaDigitalController', TarjetaDigitalController);

  function TarjetaDigitalController($scope, localStorageService, $state, config){
    let vm = this;
    let copies = localStorageService.get('copies');
    vm.datos = localStorageService.get('contratoAceptado');
    vm.recibirendomicilio = true;

    vm.title = "Te presentamos tu nueva Tarjeta Digital";
    vm.subtitle = "Guarda esta imagen para que tengas los datos de tu tarjeta";
    vm.pan = vm.datos.pan.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/,'');
    vm.fechaCaducidadTarjeta = vm.datos.fechaCaducidadTarjeta.substring(4) + "/" + vm.datos.fechaCaducidadTarjeta.substring(2,4);

    vm.next = () => {
      config.nextStep(1);
    }

    console.log(vm.datos);
  }

  TarjetaDigitalController.$inject = ["$scope", "localStorageService", "$state", "config"];
})();
