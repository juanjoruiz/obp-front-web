(function(){
  angular.module('isbanWebApp')
    .directive('tarjetadigital', tarjetadigital);

  function tarjetadigital(){
    return{
      restrict: 'A',
      templateUrl: 'js/components/tarjetadigital/tarjetadigital.view.html',
      controllerAs: 'tarjetaCtrl',
      controller: 'TarjetaDigitalController'
    }
  }
})();
