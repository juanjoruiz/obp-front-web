var app = angular.module('isbanWebApp');

app.directive('sharecontrato', function(){
  return{
    restrict: 'E',
    templateUrl: 'js/components/sharecontrato/sharecontrato.view.html',
    controllerAs: 'sharecontrato',
    controller: ['$location', 'connection', function($location, connection){
      var sharecontrato = this;
      sharecontrato.hash = $location.search().id;
      console.log(sharecontrato.hash);
      connection.login()
        .then(function(response){
          connection.getContrato(sharecontrato.hash)
            .then(function(response){
              console.log(response.data.datos);
              var file = new Blob([response.data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              window.open(fileURL);
            }, function(response){
              console.log(response);
            });
        }, function(response){
          console.log(response.status);
        });

    }]
  }
});
