var app = angular.module('isbanWebApp');

app.directive('buroCredito', function(){
  return{
    restrict: 'E',
    templateUrl: 'js/components/buro-credito/buro-credito.view.html',
    controllerAs: 'buro',
    controller: ['$scope', 'localStorageService', function($scope, localStorageService){
      let buro = this;
      buro.disabledFourDigits = true;
      buro.isDigitsEnabled = (value) => {
        if( value == 'true' ){
          buro.disabledFourDigits = false;
        } else{
          buro.disabledFourDigits = true;
        }
      }

      buro.saveTarjeta = () => localStorageService.set('tarjetasCredito', buro.Tarjeta);
      buro.saveCredito = () => localStorageService.set('creditoHipotecario', buro.Credito);
      buro.saveAutomotriz = () => localStorageService.set('creditoAutomotriz', buro.Automotriz);
      buro.saveDigitos = () => localStorageService.set('numeroTarjeta', buro.digitos);
      buro.saveAceptado = () => localStorageService.set('autorizaConsulta', buro.aceptado);


      buro.toogleTarjeta = (radio) => {
        console.log(buro.Tarjeta);
        console.log(radio);
        buro.Tarjeta = radio;
        console.log(buro.Tarjeta);

      }
    }]
  }
})
