(function(){
  angular.module('isbanWebApp')
    .controller('SucursalLightController', sucursalLightController);

    function sucursalLightController(localStorageService, config){
      var vm = this;

      vm.validarInput = function(){
        localStorageService.set('branch', vm.sucursalId);
        localStorageService.set('sucursal', " ");

        config.nextStep(1);
      }
    }

    sucursalLightController.$inject = ["localStorageService"];
})();
