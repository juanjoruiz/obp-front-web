var app = angular.module('isbanWebApp');

app.directive('sucursallight', function(){
  return {
    restrict: 'EA',
    scope: true,
    templateUrl: 'js/components/sucursal_light/sucursal_light.view.html',
    controller: 'SucursalLightController',
    controllerAs: 'slightCtrl'
  };
});
