(function(){
  angular.module('isbanWebApp')
    .controller('CardInfoController', CardInfoController);

  function CardInfoController($rootScope, $scope, localStorageService, alertaService, $state, config, connection, $crypto, baseConfig){
    let vm = this;
    let copies = localStorageService.get('copies');

    vm.showDate = true;

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_Tarjeta_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_Tarjeta_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_Tarjeta_MA_'+env});
          break;
        case '4':
          utag.view({page:'/OBP_Tarjeta_MA_'+env});
          break;
      }
    }

    //vm.tarjetaFisica = 'N';
    vm.aceptado = false;

    vm.buttonDisabledUniversidades = function(formInvalid){
      formInvalid = formInvalid.$invalid;
      if(formInvalid) return true;
      else return false;
    }

    vm.buttonDisabledNomina = function(formInvalid){
      formInvalid = formInvalid.$invalid;
      if(!vm.aceptado || formInvalid) return true;
      else return false;
    }

    vm.buttonDisabledMercadoAbierto = function(formInvalid){
      formInvalid = formInvalid.$invalid;
      if(formInvalid) return true;
      else return false;
    }

    vm.toggleDate = () => {
      console.log(vm.showDate);
      vm.showDate = !vm.showDate;
      return vm.showDate;
    }

    var tipoDeMercado = localStorageService.get('tipoDeMercado');
    if(tipoDeMercado == 1){
      vm.formCardSubTitle = copies.registroTarjeta.subtitle;
      vm.mensajeTooltipNumero = copies.registroTarjeta.toolTipNumTarjeta;
      vm.esUniversidades = true;
      vm.esNomina = false;
      vm.esMa = false;
      vm.buttonDisabled = vm.buttonDisabledUniversidades;
    }else if (tipoDeMercado == 2) {
      vm.formCardSubTitle = copies.registroTarjeta.subtitle2;
      vm.mensajeTooltipNumero = copies.registroTarjeta.toolTipNumTarjetaNomina;
      vm.esUniversidades = false;
      vm.esNomina = true;
      vm.esMa = false;
      vm.buttonDisabled = vm.buttonDisabledNomina;
    }else if (tipoDeMercado == 3 || tipoDeMercado == 4) {
      vm.formCardSubTitle = copies.registroTarjeta.subtitle2;
      vm.pregunta = copies.registroTarjeta.subtitle;
      vm.mensajeTooltipNumero = copies.registroTarjeta.toolTipNumTarjetaNomina;
      vm.opcionDomicilio = copies.registroTarjeta.domicilio;
      vm.opcionSucursal = copies.registroTarjeta.sucursal;
      vm.opcionExpress = copies.registroTarjeta.express;
      vm.esUniversidades = false;
      vm.esNomina = false;
      vm.esMa = true;
      vm.buttonDisabled = vm.buttonDisabledMercadoAbierto;
    }

    vm.showInputCard = () => {
      let visible = true
      if(vm.esUniversidades) visible = true;
      else if (vm.esNomina) visible = true;
      else if (vm.esMa) {
        visible = vm.preguntacreatecardrespuesta === "true" ? true : false;
      }
      return visible
    }

    vm.formCardTitle = copies.registroTarjeta.title;
    vm.formCardSubtitle = localStorageService.get('tipoDeMercado') === '1' ? copies.registroTarjeta.subtitle : copies.registroTarjeta.subtitle2;

    vm.formCardNumero = copies.registroTarjeta.numero;
    vm.formCardNumeroPlaceholder = copies.registroTarjeta.numero;
    vm.formCardPassDate = copies.registroTarjeta.fecha;
    vm.formCardCcv = copies.registroTarjeta.cvv;
    vm.tooltipCardPassDate = copies.registroTarjeta.toolTipFecVen;
    vm.tooltipCardCcv = copies.registroTarjeta.toolTipCVV;
    vm.preguntanewcard = copies.registroTarjeta.conTarjeta;
    vm.preguntaCreateCard = copies.registroTarjeta.asingarTarjeta;
    //vm.preguntaCreateCard = "";
    vm.span = copies.registroTarjeta.costoTarjeta;
    vm.confomridad = copies.registroTarjeta.conformidad;
    vm.labelAcceptance = copies.registroTarjeta.conformidad;

    vm.copyTitleMercadoAbierto = "Tarjeta de débito";
    vm.copyMercadoAbierto = "El sistema te asignará automáticamente un número de tarjeta de débito, la cuál te llegará al domicilio proporcionado.";

    vm.months = [];
    vm.years = [];
    for(i = 1; i <= 12; i++){
      vm.months.push(i);
    }

    function getYears(){
      var thisYear = new Date().getFullYear();
      var numThisYear = parseInt(thisYear.toString().substring(2));
      for(i = numThisYear; i <= numThisYear + 14; i++){
        vm.years.push(i);
      }
    }
    getYears();

    vm.saveCard = () => {
      localStorageService.set('pan', vm.pan);
    }

    vm.saveMes = () =>{
      if(vm.vencimientoMes < 10){
        localStorageService.set('mes', '0'+vm.vencimientoMes);
      }else{
        localStorageService.set('mes', vm.vencimientoMes);
      }
    }

    vm.saveAnio = function(){
      localStorageService.set('anio', vm.vencimientoAnio);
    }

    vm.saveCcv = function(){
      localStorageService.set('ccv', vm.ccv);
    }

    vm.saveTarjetaFisica = () => localStorageService.set('envioTarjeta', vm.tarjetaFisica);

    vm.validarTarjeta = () => {
      connection.sendPanData()
        .then(function(response){
          if(response.estatus === 0){
            config.nextStep(1);
          }else{
            let error = {
              status: response.estatus,
              descripcion: response.descripcion,
              errorCode: response.codigo,
              tipo: response.tipo,
              titulo: response.titulo
            }
            config.generalErrors(error);
          }
        });
    }

    vm.crearTarjeta = () => {
      if(vm.esMa){
        config.nextStep(1);
      }
    }
  }

  CardInfoController.$inject = ['$rootScope', '$scope', 'localStorageService', 'alertaService', '$state', 'config', 'connection', '$crypto', 'baseConfig'];
})();
