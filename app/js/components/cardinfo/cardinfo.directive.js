var app = angular.module('isbanWebApp');

app.directive('cardinfo', function(){
  return{
    restrict: 'A',
    templateUrl: 'js/components/cardinfo/cardinfo.view.html',
    controllerAs: 'cardinfo',
    controller: 'CardInfoController'
    // controller: ['$scope', 'localStorageService', 'alertaService', '$state', function($scope, localStorageService, alertaService, $state){
    //   var cardinfo = this;
    //   var mensajeTooltip;
    //
    //   //if($scope.cardInformationIsVisible){
    //     var tipoDeMercado = localStorageService.get('tipoDeMercado');
    //     if(tipoDeMercado == 1){
    //       cardinfo.esUniversidades = true;
    //     }else if (tipoDeMercado == 2) {
    //       cardinfo.esNomina = true;
    //     }else if (tipoDeMercado == 3) {
    //       cardinfo.esUniversidades = false;
    //       cardinfo.esNomina = false;
    //     }
    //   //}
    //
    //
    //   cardinfo.showAlert = function(item){
    //     localStorageService.set('continue', false);
    //     modalOptions = {
    //       actionButtonText: 'Continuar',
    //       closeButtonText: 'Cancelar',
    //       showBoth: true,
    //       showOk: false,
    //       showClose: false,
    //       headerText: 'IMPORTANTE',
    //       modalIcon: 'images/icons/iconoInfo.png',
    //       bodyText: 'Puedes abrir la cuenta sin tarjeta de débito y asociarla después en la sucursal.'
    //     };
    //     showAlertMessage(modalOptions)
    //       .then(function(){
    //         console.log(item);
    //         //$scope.saveData(item);
    //         //$state.go('mapa');
    //         $scope.saveData(item);
    //
    //       }, function(){
    //         console.log('paso 2');
    //         //cardinfo.preguntacreatecardrespuesta = undefined
    //       });
    //   }
    //
    //   var showAlertMessage = function(modalOptions){
    //     return alertaService.showModal({}, modalOptions);
    //   };
    //
    // }]
  }
})
