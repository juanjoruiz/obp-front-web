(function(){
  angular.module('isbanWebApp')
    .directive('beneficios', beneficios);

  function beneficios(){
    return{
      restrict: 'EA',
      templateUrl: 'js/components/beneficios/beneficios.view.html',
      controllerAs: 'beneficiosCtrl',
      controller: 'beneficiosController'
    }
  }
})();
