(function(){
  angular.module('isbanWebApp')
    .controller('beneficiosController', beneficiosController);

  function beneficiosController($scope, $stateParams, localStorageService, connection, $rootScope, $state, baseConfig){

    let vm = this;
    vm.exceptions = [];
    let flux = localStorageService.get('flux');
    let env = baseConfig.ENV.toUpperCase();

    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_Beneficios_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_Beneficios_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_Beneficios_MA_'+env});
          break;
        case '4':
          utag.view({page:'/OBP_Beneficios_MA_'+env});
          break;
      }
    }

    $scope.$on('copiesLoaded', function(){
      let copies = localStorageService.get('copies');
      vm.beneficiosTitle = copies.beneficios.title;
      vm.beneficios = copies.beneficios.subtitle;


      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          newException(copies.beneficios.ipab, "images/data/ipab.png");
          break;
        case '2':
          newException(copies.beneficios.ipab, "images/data/ipab.png");
          newException(copies.beneficios.disclaimer, undefined);
          break;
        case '3':
          newException(copies.beneficios.ipab, "images/data/ipab.png");
          break;
        case '4':
          newException(copies.beneficios.ipab, "images/data/ipab.png");
          break;
      }
    });

    $scope.$on('benycondLoaded', function(){
      let beneficios = localStorageService.get('benycond');
      vm.beneficiosList = beneficios.beneficiosLista;
    });

    const newException = (desc, img) => {
      let exception = {
        desc: desc,
        img: img
      }
      vm.exceptions.push(exception);
    }

    vm.showCondiciones = () =>{
      $rootScope.$broadcast('showCondiciones');
    }
  }
  beneficiosController.$inject = ['$scope', '$stateParams', 'localStorageService', 'connection', '$rootScope', '$state', 'baseConfig'];
})();



/*

var app = angular.module('isbanWebApp');

app.directive('beneficios', function(){
  return {
    restrict: 'EA',
    templateUrl: 'js/components/beneficios/beneficios.view.html',
    controller: ['$scope', 'connection', 'localStorageService', '$uibModal', '$location', '$anchorScroll', '$crypto','modalService', 'avisoService', '$sce', '$stateParams', 'config',
    function($scope, connection, localStorageService, $uibModal, $location, $anchorScroll, $crypto, modalService, avisoService, $sce, $stateParams, config){
      localStorageService.clearAll();
      $scope.beneficiosList = {};
      $scope.condicionesList = {};
      $scope.exceptions = [];

      //let copies = undefined;

      const generalErrorNotFound = () => showErrorMessage(404, "Hubo un Error con el servidor.");
      const generalErrorTimeOut = () => showErrorMessage(500, "El servidor tardó en responder.");
      const generalErrorUnkonwn = status => showErrorMessage(status, "Ocurrió un problema en el servidor.");
      const respGetCopies = response => {
        localStorageService.set('copies', response.data.datos);
        setCopies($stateParams.tipo);
      }
      const errorConnection = response => generalErrors();
      const getCopies = () => {
        $scope.loader = true;
        connection.getCopies().then(respGetCopies, errorConnection);
      }
      const respGetBeneficios = response => {
        $scope.beneficiosList = response.data.datos.beneficiosLista;
        $scope.condicionesList = response.data.datos.condicionesLista.concat(response.data.datos.condicionesOtrasLista);
      }

      const generalErrors = config.generalErrors;



      const setCopies = mercado => {
        let copies = localStorageService.get('copies');
        console.log(copies);
        $scope.beneficiosTitle = copies.beneficios.title;
        $scope.beneficios = copies.beneficios.subtitle;
        $scope.condicionesTitle = copies.condiciones.title;
        $scope.condiciones = copies.condiciones.subtitle;
        switch (mercado) {
          case '1':
            //newException("Producto garantizado por el Instituto de Protección al Ahorro Bancario hasta por 400 mil UDIS, por cliente, por Institución. Para más información consultar <a href='http://www.ipab.org.mx' target='_blank'>www.ipab.org.mx</a>", "images/data/ipab.png");
            newException(copies.beneficios.ipab, "images/data/ipab.png");
            //newException("<span class='red'>*</span>Para tarjetas emitidas antes de agosto de 2016, actívala en tu sucursal o llama al 01 800 501 0000.", undefined);
            newException(copies.condiciones.notaPrivacidad, undefined);
            break;
          case '2':
            newException(copies.beneficios.ipab, "images/data/ipab.png");
            //newException("Producto garantizado por el Instituto de Protección al Ahorro Bancario hasta por 400 mil UDIS, por cliente, por Institución. Para más información consultar <a href='http://www.ipab.org.mx' target='_blank'>www.ipab.org.mx</a>", "images/data/ipab.png");
            //newException('<span class="red">*</span>Este beneficio se mantiene mientras la cuenta reciba depósitos mensuales por concepto de nómina.', undefined);
            newException(copies.beneficios.disclaimer, undefined);
            break;
          case '3':
            //newException("Producto garantizado por el Instituto de Protección al Ahorro Bancario hasta por 400 mil UDIS, por cliente, por Institución. Para más información consultar <a href='http://www.ipab.org.mx' target='_blank'>www.ipab.org.mx</a>", "images/data/ipab.png");
            newException(copies.beneficios.ipab, "images/data/ipab.png");
            //newException(copies.beneficios.disclaimer, undefined);
            break;
          default:
            break;
        }
      }



      const getBeneficios = () => {
        connection.getBeneficios().then(respGetBeneficios, errorConnection);
        $scope.loader = false;
      }

      const successLogging = response => {
        localStorageService.set('token', response.data);
        getCopies();
        getBeneficios();

      }

      function login() {
        localStorageService.set('tipoDeMercado', $stateParams.tipo);
        setBanner($stateParams.tipo);
        connection.login().then(successLogging, errorConnection);
        $scope.loader = false;
      }

      //Inicio de ventana

      login();

      $scope.triggerAviso = () => {
        let copies = localStorageService.get('copies');
        avisoOptions = {
          actionButtonText: 'Aceptar y continuar',
          showOk: true,
          headerText: copies.avisoPrivacidad.title,
          bodyText: copies.avisoPrivacidad.item1
        };
        utag.view({page:'/OBP_AvisoPrivacidad'});
        return avisoService.showModal({}, avisoOptions);
      }

      var showErrorMessage = function(id, value){
        var modalOptions = {};
        if(id == -3){
          value = $crypto.decrypt(value);
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            closeButtonText: 'Ok',
            showOk: false,
            showClose: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }else if(id == -2){
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            showOk: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: "Por el momento el servicio no está disponible, por favor intenta nuevamente más tarde."
          };
        }else if(id == 404 || id == 500){
          value = $crypto.decrypt(value);
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            showOk: true,
            showClose: false,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }else{
          value = $crypto.decrypt(value);
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            showOk: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }
        return modalService.showModal({}, modalOptions);
      };
    }]
  };
});
*/
