var app = angular.module('isbanWebApp');

app.directive('datosbeneficiario', function(){
  return{
    restrict: 'A',
    templateUrl: 'js/components/datosbeneficiario/datosbeneficiario.view.html',
    controllerAs: 'beneficiarioCtrl',
    controller: 'DatosBeneficiarioController'
  }
});
