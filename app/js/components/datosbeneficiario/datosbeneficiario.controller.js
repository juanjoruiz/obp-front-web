(function(){
  angular.module('isbanWebApp')
    .controller('DatosBeneficiarioController', DatosBeneficiarioController);

  function DatosBeneficiarioController($rootScope, $scope, localStorageService, connection, $filter, $state, config, baseConfig){
    let vm = this;
    let copies = localStorageService.get('copies');

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_DatosBeneficiario_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_DatosBeneficiario_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_DatosBeneficiario_MA_'+env});
          break;
        case '4':
          utag.view({page:'/OBP_DatosBeneficiario_MA_'+env});
          break;
      }
    }

    vm.beneficiarioNombreLabel = copies.direccionBeneficiario.nombre;
    vm.beneficiarioPaternoLabel = copies.direccionBeneficiario.paterno;
    vm.beneficiarioMaternoLabel = copies.direccionBeneficiario.materno;
    vm.beneficiarioNacimientoLabel = copies.direccionBeneficiario.nacimiento;
    vm.beneficiarioNacionalidadLabel = copies.direccionBeneficiario.nacionalidad;
    vm.beneficiarioTitle3 = copies.direccionBeneficiario.title;
    vm.beneficiarioTitle2 = copies.direccionBeneficiario.title2;
    vm.beneficiariominititle = copies.direccionBeneficiario.minititle;
    vm.beneficiarioTooltipDomicilio = copies.direccionBeneficiario.tooltipDomicilio;
    vm.beneficiarioCalleLabel = copies.direccionBeneficiario.calle;
    vm.beneficiarioNumeroExteriorLabel = copies.direccionBeneficiario.numeroExterior;
    vm.beneficiarioNumeroInteriorLabel = copies.direccionBeneficiario.numeroInterior;
    vm.beneficiarioCodigoPostalLabel = copies.direccionBeneficiario.codigoPostal;
    vm.beneficiarioColoniaLabel = copies.direccionBeneficiario.colonia;
    vm.beneficiarioRegistroCp = copies.direccionBeneficiario.registroCp;
    vm.beneficiarioCodigoAyuda = copies.direccionBeneficiario.codigoAyuda;
    vm.adultoAceptadoBeneficiario = true;

    vm.tooltipBeneficiario = "La persona que designa para ejercer los derechos de la cuenta en caso de fallecimiento es quién heredará los fondos depositados en ella. Puede ser designado en éste momento o posteriormente en la sucursal.";
    vm.tooltipColoniaPoblacion = "Si la colonia desplegada al ingresar tu código postal no es la indicada, puedes seleccionar otra tocando el campo Colonia/Población.";
    vm.tooltipDomicilioBeneficiario = "La dirección domiciliaria de tu beneficiario deberá estar ubicada en México.";
    vm.format = 'yyyy/MM/dd';
    vm.disableMonth = true;
    vm.disableYear = true;
    vm.showBeneficiario = false;

    connection.getPaises('Nacimiento_.json')
      .then(function(response){
        vm.estados = response.datos.estados;
        vm.paises = response.datos.paises;
      }, function(response){
        console.log(response.status);
      });

    $scope.$on('showBeneficiario', function(){
      vm.showBeneficiario = true;
    });

    $scope.$on('hideBeneficiario', function(){
      vm.showBeneficiario = false;
    });



    function getYears(){
      var thisYear = new Date().getFullYear();
      var numThisYear = parseInt(thisYear.toString());
      return numThisYear;
    }

    var max = getYears() - 18;
    var min = getYears() - 100;

    function calcAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    }

    $(function(){
      $('#DATE_fechanacimientobeneficiario').combodate({
        customClass:'form-control beneficiario',
        minYear: min,
        maxYear: max
      });
    });

    vm.saveNombreBeneficiario = function(){
      vm.nombreBeneficiario = vm.nombreBeneficiario.toUpperCase();
      localStorageService.set('nombrebeneficiario', vm.nombreBeneficiario);
    }

    vm.saveApPaternoBeneficiario = function(){
      vm.apellidoPaternoBeneficiario = vm.apellidoPaternoBeneficiario.toUpperCase();
      localStorageService.set('apellidopaternobeneficiario', vm.apellidoPaternoBeneficiario);
    }

    vm.saveApMaternoBeneficiario = function(){
      if(vm.apellidoMaternoBeneficiario !== undefined){
        vm.apellidoMaternoBeneficiario = vm.apellidoMaternoBeneficiario.toUpperCase();
        localStorageService.set('apellidomaternobeneficiario', vm.apellidoMaternoBeneficiario);
      }else{
        localStorageService.set('apellidomaternobeneficiario', '');
      }

    }

    vm.saveFechaNacimientoBeneficiario = function(){

      let bornTime = new Date(vm.fechaNacimientoBeneficiario);
      let age = calcAge(bornTime);
      $scope.beneficiarioForm.fechaNacimientoBeneficiario.$setValidity("adulto", age>=18);
      if(age >= 18){
        vm.adultoAceptadoBeneficiario = true;
        localStorageService.set('fechanacimientobeneficiario', $filter('date')(vm.fechaNacimientoBeneficiario, 'yyyy-MM-dd'));
      }else{
        vm.adultoAceptadoBeneficiario = false;
      }
        //$scope.beneficiarioForm.$valid = vm.adultoAceptadoBeneficiario

    }


    vm.saveNacionalidadBeneficiario = function(){

      localStorageService.set('paisDeNacimientoBeneficiario', vm.nacionalidadBeneficiario);
      console.log(vm.estados);
      if(vm.nacionalidadBeneficiario == 'M'){
        vm.chooseEntidadBeneficiario = vm.estados;
      }else{
        vm.chooseEntidadBeneficiario = vm.paises;
      }
    }
    vm.saveEntidadNacimientoBeneficiario = function(){
      localStorageService.set('entidadnacimientobeneficiario', vm.entidadNacimientoBeneficiario);

    }

    vm.addDomicilioBeneficiario = function(){
      if(vm.mismoDomicilioBeneficiario == 'n'){
        vm.domiciliodiferente = true;
        localStorageService.set('beneficiarioMismoDomicilio', false);
      }else{
        vm.domiciliodiferente = false;
        localStorageService.set('beneficiarioMismoDomicilio', true);
        //setDomicilioSameAsCliente();
      }
    }


    vm.saveCalleBeneficiario = function(){
      vm.callebeneficiario = vm.callebeneficiario.toUpperCase();
      localStorageService.set('callebeneficiario', vm.callebeneficiario );
    }
    vm.saveExteriorBeneficiario = function(){
      localStorageService.set('exteriorbeneficiario', vm.exteriorbeneficiario); }
    vm.saveInteriorBeneficiario = function(){
      localStorageService.set('interiorbeneficiario', vm.interiorbeneficiario); }
    vm.saveZipCodeBeneficiario = function(){
      if(vm.zipcodebeneficiario.length == 5){
        localStorageService.set('zipcodebeneficiario', vm.zipcodebeneficiario);
        connection.sendCp(localStorageService.get('zipcodebeneficiario'))
          .then(function(response){
            if(response.estatus !== 0){
              //showErrorMessage(response.estatus, response.descripcion);
              let error = {
                status: response.estatus,
                descripcion: response.descripcion,
                errorCode: response.codigo,
                tipo: response.tipo,
                titulo: response.titulo
              }
              config.generalErrors(error);
              vm.cpResponse2 = [];
            }else{
              vm.cpResponse2 = response.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;
              vm.selectedColoniaBeneficiario = vm.cpResponse2[vm.cpResponse2.length - 1];
              localStorageService.set('delegacionbeneficiario', response.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
              localStorageService.set('ciudadbeneficiario', response.datos.lstEstado[0].ciudades[0].ciudad);
              localStorageService.set('estadobeneficiario', response.datos.lstEstado[0].estado);
              localStorageService.set('coloniabeneficiario', vm.cpResponse2[vm.cpResponse2.length -1] );
            }
          }, function(response){
            var cuatrocientoscuatro = "Hubo un Error con el servidor.";
            var quinientos = "El servidor tardó en responder";
            var desconocido = "Ocurrió un problema en el servidor.";
            if(response.status == 404){
              showErrorMessage(response.status, cuatrocientoscuatro);
            }else if(response.status == 500){
              showErrorMessage(response.status, quinientos);
            }else{
              showErrorMessage(response.status, desconocido);
            }
            vm.cpResponse2 = [];
            vm.loader = false;
          });
      }
    }
    vm.saveColoniaBeneficiario = function(){
      localStorageService.set('coloniabeneficiario', vm.selectedColoniaBeneficiario);
    }

    vm.validarClienteConBeneficiario = () => {
      connection.sendDatosPersona()
        .then(function(response){
          if(response.estatus === 0){
            localStorageService.set('datosProspecto', response.datos);
            config.nextStep(1);
          }else{
            let error = {
              status: response.estatus,
              descripcion: response.descripcion,
              errorCode: response.codigo,
              tipo: response.tipo,
              titulo: response.titulo
            }
            config.generalErrors(error);
          }
        })
    }

  }

  DatosBeneficiarioController.$inject = ["$rootScope", "$scope","localStorageService", "connection", "$filter", "$state", "config", "baseConfig"];
})();
