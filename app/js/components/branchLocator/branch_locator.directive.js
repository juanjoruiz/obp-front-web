(function(){
  var app = angular.module('isbanWebApp');
  app.directive('branchLocator', branchLocator);

  function branchLocator(){
    return {
      restrict: 'A',
      templateUrl: 'js/components/branchLocator/branch_locator.view.html',
      controllerAs: 'branchCtrl',
      controller: 'BranchLocatorController'
    }
  }

})();
