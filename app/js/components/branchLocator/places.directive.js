(function(){
  angular.module('isbanWebApp')
    .directive('placeAutocomplete', placeAutocomplete);

  function placeAutocomplete(){
    return{
      templateUrl: 'js/components/branchLocator/places.view.html',
      restrict: 'EA',
      scope:{
        'ngModel':'='
      },
      controllerAs: 'placesCtrl',
      controller: 'PlacesController'
    }
  }
})();
