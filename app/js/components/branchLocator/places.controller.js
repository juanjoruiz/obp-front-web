(function(){
  var app = angular.module('isbanWebApp');
  app.controller('PlacesController', PlacesController);

  function PlacesController($scope, $q, $rootScope){
    $scope.placeSelected = false;
    if(!google || !google.maps){
      throw new Error('Google Maps no se ha podido cargar.');
    }else if(!google.maps.places){
      throw new Error('Google Maps no cuenta con los módulos de Places.');
    }

    let autocompleteService = new google.maps.places.AutocompleteService();
    let map = new google.maps.Map(document.createElement('div'));
    let placeService = new google.maps.places.PlacesService(map);

    $scope.ngModel = {};

    const getResults = (address) => {
      let deferred = $q.defer();
      autocompleteService.getPlacePredictions({
        input: address,
        componentRestrictions: {country: 'mx'},
      }, function(data) {
        deferred.resolve(data);
      });
      return deferred.promise;
    };

    const getDetails = (place) => {
      let deferred = $q.defer();
      placeService.getDetails({
        'placeId': place.place_id
      }, function(details){
        deferred.resolve(details);
      });
      return deferred.promise;
    };

    $scope.search = function(input){
      if(!input){
        $scope.places = [];
         return;
      }
      return getResults(input).then(function(places){
        $scope.places = places ? places : [];

        return places ? places : [];
      });
    }

    $scope.getLatLng = function(place){
      if(!place){
        $scope.ngModel = {};
        return;
      }
      getDetails(place).then(function(details){
        let args = {
           'name': place.description,
           'latitude': details.geometry.location.lat(),
           'longitude': details.geometry.location.lng()
        };
        $scope.searchText = args.name;
        $scope.placeSelected = true;
        $rootScope.$broadcast('startGeoSearch', args);
      });
    }

    $scope.clear = function(){
      $scope.searchText = "";
      $scope.placeSelected = false;
      $scope.places = null;
    }

  }

  PlacesController.$inject = ["$scope", "$q", "$rootScope"]
})();
