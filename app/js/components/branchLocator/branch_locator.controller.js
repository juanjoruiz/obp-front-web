(function(){
  var app = angular.module('isbanWebApp');
  app.controller('BranchLocatorController', BranchLocatorController);

  function BranchLocatorController($rootScope, $scope, $timeout, connection, localStorageService, config){
    let vm = this;
    let infoWindow;
    vm.markers = [];
    $scope.selectedCard = [];
    let nav = document.getElementById('BUTTON_FAV');
    let copies = localStorageService.get('copies');
    let firstCall = true;
    if(firstCall){
      let message = {
        id: 3,
        botonCerrado: copies.branchLocator.alertButton,
        titulo: copies.branchLocator.alertTitle,
        tipo: "info",
        descripcion: copies.branchLocator.alertMessage
      }
      config.generalMessages(message);
    }





    function showBranchCard(marker){
      console.log(marker);


      angular.forEach(vm.markers, function(m){
        if(marker.title == m.id){
          nav.style.transition = "all 0.3s";
          nav.style.bottom = "190px";
          $scope.selectedCard = [];
          $scope.selectedCard.push(m);
          $scope.$apply();
          let card = document.getElementById('CARD_branch');
        }
      });
    }

    vm.getDistance = (value) =>{
      return Number(value).toFixed(2)
    }

    vm.saveBranch = function(branch){
      let mensaje = "¿Deseas seleccionar la sucursal? \n\n" + branch.name + ", " + branch.address + ", " + branch.colony + ", " + branch.postalCode + ", " + branch.city;
      let message = {
        id: 4,
        title:"Selección de sucursal",
        descripcion: mensaje
      }
      config.generalMessages(message).then(function(){
        console.log("Guardar");
        localStorageService.set('branch', branch.id);
        localStorageService.set('sucursal', branch.name);
        if(localStorageService.get('tipoDeMercado') == 2 || localStorageService.get('tipoDeMercado') == 3){
          connection.createPanData()
            .then(function(response){
              if(response.estatus === 0){
                config.nextStep(1);
              }else{
                let error = {
                  status: response.estatus,
                  descripcion: response.descripcion,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo
                }
                config.generalErrors(error);
              }
            })
        }else{
          localStorageService.set('branch', branch.id);
          localStorageService.set('sucursal', branch.name);
          config.nextStep(1);
        }
      },function(){
        console.log("Se canceló la selección");
      });


    }

    function addBranch(m){
      var marker = new google.maps.Marker({
        clickable: true,
        icon: 'img/icons/puntero_santander.png',
        shadow: null,
        position: new google.maps.LatLng(m.latitude, m.longitude),
        title: m.id,
        more: m.dir2,
        map: vm.map
      });
      google.maps.event.addListener(marker, 'click', function(){
        showBranchCard(marker);
      });
    }

    vm.renderActualPosition = function(){
      navigator.geolocation.getCurrentPosition(function(position){
        var mapOptions = {
          center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
          zoom:15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          scrollwheel: false,
          disableDefaultUI:true
        };
        vm.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        vm.center = vm.map.getCenter();
        console.log(vm.center);
        $scope.selectedCard = [];
        nav.style.bottom = "10px";
        vm.addUserMarker(new google.maps.LatLng(position.coords.latitude, position.coords.longitude), vm.map, vm.center);
          connection.getBranches(position.coords.latitude, position.coords.longitude).then(function(response){
            console.log(response);
            if(response.data.datos.length > 0){
              vm.markers = response.data.datos;
              angular.forEach(response.data.datos, function(marker){
                addBranch(marker);
              });
            }else{
              let error = {
                status: -2,
                descripcion: "No se encontraron sucursales en esta ubicación, favor de buscar por dirección de la sucursal que desees.",
                errorCode: response.codigo,
                tipo: "info",
                titulo: "Sin Sucursales"
              }
              config.generalErrors(error);
            }

          });
      });
    }



    vm.renderSearchPosition = (position) => {
      let mapOptions = {
        center: new google.maps.LatLng(position.latitude, position.longitude),
        zoom:15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        disableDefaultUI:true
      };
      vm.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
      vm.addSearchMarker(new google.maps.LatLng(position.latitude, position.longitude), vm.map, vm.map.getCenter());
      $scope.selectedCard = [];
      nav.style.bottom = "10px";
      connection.getBranches(position.latitude, position.longitude).then(function(response){
        if(response.data.datos.length > 0){
          vm.markers = response.data.datos;
          angular.forEach(response.data.datos, function(marker){
            addBranch(marker);
          });
        }else{
          let error = {
            status: -2,
            descripcion: "No se encontraron las sucursales en esta ubicación, favor de buscar por dirección de la sucursal que desees.",
            errorCode: response.codigo,
            tipo: "info",
            titulo: "Sin Sucursales"
          }
          config.generalErrors(error);
        }
      });
    }

    $scope.$on('startGeoSearch', function(event, args){
      $scope.addressToSearch = args.name
      vm.renderSearchPosition(args);
    });

    vm.addSearchMarker = function(position, map, center){
      var marker = new google.maps.Marker({
        clickable: false,
        icon: 'img/icons/bluedot.png',
        shadow: null,
        position: position,
        map: map
      });
      var circle = new google.maps.Circle({
        strokeColor: '#ababab',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#ababab',
        fillOpacity: 0.35,
        map: map,
        center: center,
        radius: 250
      });
    }

    vm.addUserMarker = function(position, map, center){
      var marker = new google.maps.Marker({
        clickable: false,
        icon: 'img/icons/bluedot.png',
        shadow: null,
        position: position,
        map: map
      });
      var circle = new google.maps.Circle({
        strokeColor: '#ababab',
        strokeOpacity: 0,
        strokeWeight: 2,
        fillColor: '#ababab',
        fillOpacity: 0.35,
        map: map,
        center: center,
        radius: 250
      });
    }

    vm.renderActualPosition();

    vm.centerToPosition = function(){
      $scope.selectedCard = [];
      nav.style.bottom = "10px";
      vm.renderActualPosition();
      vm.map.setCenter(vm.center);
    }


    //var selected = scope.tipoBusqueda;
    //var branches = scope.getBranches();

    // if(navigator.geolocation && selected == 'ubicacion'){
    //   navigator.geolocation.getCurrentPosition(function(position){
    //     scope.$apply(function(){
    //
    //       var mapOptions = {
    //         center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
    //         zoom:15,
    //         mapTypeId: google.maps.MapTypeId.ROADMAP,
    //         scrollwheel: false
    //       };
    //
    //
    //       function initMap(){
    //         scope.map = new google.maps.Map(element[0], mapOptions);
    //         scope.loader = false;
    //       }
    //
    //       scope.loader = true;
    //       initMap();
    //       //console.log(position);
    //
    //       scope.setMarker(scope.map, new google.maps.LatLng(position.coords.latitude, position.coords.longitude), '', {}, "normal");
    //       var branches = scope.getBranches(position.coords.latitude, position.coords.longitude);
    //       // console.log(branches);
    //       //scope.$watch('branches', function(newValue))
    //       angular.forEach(branches, function(branch){
    //       //   console.log(branch);
    //         scope.setMarker(scope.map, new google.maps.LatLng(branch.latitude, branch.longitude), 'Santander',  branch, "branch");
    //       });
    //
    //     });
    //   });
    // }else if(navigator.geolocation && selected == 'direccion'){
    //   navigator.geolocation.getCurrentPosition(function(position){
    //     var mapOptions = {
    //       center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
    //       zoom:15,
    //       mapTypeId: google.maps.MapTypeId.ROADMAP,
    //       scrollwheel: false
    //     };
    //
    //     var initMap = function(){
    //       //if(map == void 0){
    //         scope.map = new google.maps.Map(element[0], mapOptions);
    //         initialize();
    //       //}
    //     }
    //
    //     var initialize = function() {
    //       var geocoder = new google.maps.Geocoder();
    //       if(scope.searchAddress === undefined ) return;
    //       geocoder.geocode({ 'address': scope.searchAddress }, function(results, status){
    //         if(status === google.maps.GeocoderStatus.OK){
    //           scope.map.setCenter(results[0].geometry.location);
    //           scope.map.setZoom(15);
    //           scope.setMarker(scope.map, results[0].geometry.location, '', {}, "normal");
    //
    //           var branches = scope.getBranches(results[0].geometry.location.lat(), results[0].geometry.location.lng());
    //           angular.forEach(branches, function(branch){
    //             scope.setMarker(scope.map, new google.maps.LatLng(branch.latitude, branch.longitude), 'Santander',  branch, "branch");
    //           });
    //         }else{
    //           alert('Geocode was not successful for the following reason: ' + status);
    //         }
    //       });
    //     }
    //     initMap();
    //
    //
    //   });
    //
    // }
    // scope.loader = false;





    // $timeout(function(){
    //   let latlng = new google.maps.LatLng(35.7042995, 139.7597564);
    //
    //
    //   branch.map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    //   branch.overlay = new google.maps.OverlayView();
    //   branch.overlay.draw = function(){};
    //   branch.overlay.setMap(branch.map);
    //   branch.element = document.getElementById("map_canvas");
    //   //branch.hammertime = Hammer(branch.element).on("hold", function(event){
    //     //branch.addOnClick(event);
    //   //});
    // }, 100);
    //
    // branch.deleteAllMarkers = function(){
    //   if(branch.markers == 0){
    //     alert("No hay marcadores que borrar");
    //     return;
    //   }
    //   for(var i = 0; i < branch.markers.length; i ++){
    //     //remover el marcador del mapa
    //     branch.markers[i].setMap(null);
    //   }
    //   //remover el marcador del arreglo
    //   branch.markers.length = 0;
    //   branch.markerId = 0;
    //   alert("Se borraron todos los marcadores");
    // };
    //
    // branch.rad = function(x){
    //   return x * Math.PI / 180;
    // }
    //
    // branch.addOnClick = function(){
    //
    // }
  }

  BranchLocatorController.$inject = ["$rootScope", "$scope", "$timeout", "connection", "localStorageService", "config"];
})();
