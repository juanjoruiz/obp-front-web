var app = angular.module('isbanWebApp');

app.directive('personales', function(){
  return {
    restrict: 'E',
    templateUrl: 'js/components/personales/personales.view.html'
  };
});
