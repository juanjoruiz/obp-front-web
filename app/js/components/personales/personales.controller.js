var app = angular.module('isbanWebApp');

app.controller('personalDataCtrl',['$scope', 'localStorageService', '$location', '$anchorScroll', 'connection', 'modalService', '$window', '$crypto', '$stateParams', '$state', 'alertaService', '$rootScope',
function($scope, localStorageService, $location, $anchorScroll, connection, modalService, $window, $crypto, $stateParams, $state, alertaService, $rootScope){

  /* Configuración de visibilidad de secciones */
  /* Contacto */
  $scope.contactoIsVisible = true; //inicialmente debe ser true
  $scope.contactoTelefonoIsVisible = true; //inicialmente debe ser true
  $scope.contactoOtroTelefonoIsVisible = false;
  $scope.validationCodeIsVisible = false;
  $scope.cardInformationIsVisible = false;
  /* Branch Locator */
  $scope.branchLocatorIsVisible = false
  /* Identificación */
  $scope.identificacionIsVisible = false;
  $scope.idSelectionIsVisible = false;
  $scope.idDatosIsVisible = false;
  $scope.beneficiarioIsVisible = false;
  /* contrato */
  $scope.contratoIsVisible = false;
  $scope.nipIsVisible = false;
  $scope.cuentaactivadaIsVisible = false;
  $scope.contraseniaIsVisible = false;
  $scope.buroIsVisible = false;
  $scope.encuestaIsVisible = false;

  $scope.content = {};
  $scope.content.ingresarTelefono = {};
  $scope.content.ingresarCodigo = {};
  $scope.content.registroTarjeta = {};
  const generalErrorNotFound = () => showErrorMessage(404, "Hubo un Error con el servidor.");
  const generalErrorTimeOut = () => showErrorMessage(500, "El servidor tardó en responder.");
  const generalErrorUnkonwn = status => showErrorMessage(status, "Ocurrió un problema en el servidor.");
  const errorConnection = response => generalErrors();
  const generalErrors = status => {
      switch (status) {
        case '404':
          generalErrorNotFound();
          break;
        case '500':
          generalErrorTimeOut();
          break;
        default:
          generalErrorUnkonwn(status);
          break;
      }
    }
  const getCopies = mercado => {
    console.log(mercado);
    let copies = localStorageService.get('copies');
    switch (mercado) {
      case '1':
      console.log('universidades');
        $scope.formCardSubTitle = copies.registroTarjeta.subtitle;
        $scope.mensajeTooltipNumero = copies.registroTarjeta.toolTipNumTarjeta;
        $scope.datosPersonaRol = copies.dataPersona.rol;
        $scope.esUniversidades = true;
        $scope.esNomina = false;
        $scope.esMercadoAbierto = false;
        break;
      case '2':
      console.log('nomina');
        $scope.formCardSubTitle = copies.registroTarjeta.subtitle2;
        $scope.mensajeTooltipNumero = copies.registroTarjeta.toolTipNumTarjetaNomina;
        $scope.datosPersonaRol = copies.dataPersona.rol2;
        $scope.esUniversidades = false;
        $scope.esNomina = true;
        $scope.esMercadoAbierto = false;
        break;
      case '3':
        $scope.formCardSubTitle = copies.registroTarjeta.subtitle2;
        $scope.mensajeTooltipNumero = copies.registroTarjeta.toolTipNumTarjetaNomina;
        $scope.datosPersonaRol = copies.dataPersona.rol2;
        $scope.esUniversidades = false;
        $scope.esNomina = false;
        $scope.esMercadoAbierto = true;
        break;
      default:
        break;
    }
    //Pantalla de Teléfono
    $scope.formTitle = copies.ingresarTelefono.head;
    $scope.formTitle2 = copies.ingresarTelefono.title;
    $scope.formSubTitle = copies.ingresarTelefono.subtitle;
    $scope.formTelefonoLabel = copies.ingresarTelefono.labelTelefono;
    $scope.formEmailLabel = copies.ingresarTelefono.labelEmail;
    $scope.importante = copies.ingresarTelefono.confirmarEmail;
    $scope.formCompanyLabel = copies.ingresarTelefono.labelCompania;
    $scope.formConfirmacionLabel = copies.ingresarTelefono.labelEmailConf;
    //Pantalla de SMS
    $scope.formVerificationCodeSubtitle = copies.ingresarCodigo.subtitle;
    $scope.formVerificationCodeLabel = copies.ingresarCodigo.labelConf;
    $scope.smsHint = "En caso de que después de un minuto no hayas recibido el SMS, selecciona una opción:";
    $scope.formResendLink = copies.ingresarCodigo.reEnviar;
    $scope.formSendOtherToken = "Enviar a otro teléfono";
    //Pantalla de tarjeta
    $scope.formCardTitle = copies.registroTarjeta.title;
    $scope.formCardNumero = copies.registroTarjeta.numero;
    $scope.formCardNumeroPlaceholder = copies.registroTarjeta.numero;
    $scope.formCardPassDate = copies.registroTarjeta.fecha;
    $scope.formCardCcv = copies.registroTarjeta.cvv;
    $scope.tooltipCardPassDate = copies.registroTarjeta.toolTipFecVen;
    $scope.tooltipCardCcv = copies.registroTarjeta.toolTipCVV;
    $scope.preguntanewcard = copies.registroTarjeta.conTarjeta;
    $scope.preguntaCreateCard = copies.registroTarjeta.asingarTarjeta;
    $scope.span = copies.registroTarjeta.costoTarjeta;

    $scope.copyTitleMercadoAbierto = "Tarjeta de débito";
    $scope.copyMercadoAbierto = "El sistema te asignará automáticamente un número de tarjeta de débito, la cuál te llegará al domicilio proporcionado.";

    //Pantalla de INE-IFE
    $scope.identificacionTitle = copies.seleccionarCredencial.title;
    $scope.identificacionSubtitle = copies.seleccionarCredencial.mensaje;
    $scope.identificacionIneLabel = copies.seleccionarCredencial.btnINE;
    $scope.identificacionPasaporteLabel = copies.seleccionarCredencial.btnPasaporte;
    $scope.identificacionIneTextoFrente = copies.subirINE.frontInput;
    $scope.identificacionIneTextoReverso = copies.subirINE.backInput;
    $scope.identificacionPasaporteTextoFrente = copies.subirPasaporte.Input;

    //Pantalla de datos de persona
    $scope.datosPersonaTitulo = copies.dataPersona.title;
    $scope.datosPersonaMensaje = copies.dataPersona.subtitle;
    $scope.datosPersonaNombre = copies.dataPersona.nombre;
    $scope.datosPersonaPaterno = copies.dataPersona.paterno;
    $scope.datosPersonaMaterno = copies.dataPersona.materno;
    $scope.datosPersonaSexo = copies.dataPersona.sexo;
    $scope.datosPersonaNacimiento = copies.dataPersona.nacimiento;
    $scope.datosPersonaEntidad = copies.dataPersona.entidad;
    $scope.agregarAhora = copies.direccionPersona.agregarAhora;
    $scope.agregarDespues = copies.direccionPersona.agregarDespues;
    $scope.datosPersonaTitulo2 = copies.direccionPersona.title;
    $scope.datosPersonaCalleLabel = copies.direccionPersona.calle;
    $scope.datosPersonaNumeroExteriorLabel = copies.direccionPersona.numeroExterior;
    $scope.datosPersonaNumeroInteriorLabel = copies.direccionPersona.numeroInterior;
    $scope.datosPersonaCodigoPostalLabel = copies.direccionPersona.codigoPostal;
    $scope.datosPersonaRegistroCp = copies.direccionPersona.registroCp;
    $scope.datosPersonaColoniaLabel = copies.direccionPersona.colonia;
    $scope.datosPersonaCodigoAyuda = copies.direccionPersona.codigoAyuda;
    $scope.datosPersonaMunicipioLabel = copies.direccionPersona.municipio;
    $scope.datosPersonaCiudadLabel = copies.direccionPersona.ciudad;
    $scope.datosPersonaEstadoLabel = copies.direccionPersona.estado;
    if(!$scope.beneficiarioIsVisible){
      $scope.beneficiarioTitle = copies.direccionPersona.titleBeneficiario;
      $scope.beneficiarioSubtitle = copies.direccionPersona.subtitle;
      $scope.tooltipBeneficiario = copies.direccionPersona.tooltipBeneficiario;
      $scope.textoBeneficiario = copies.direccionPersona.textoBeneficiario;
    }else{
      $scope.beneficiarioTitle = copies.direccionBeneficiario.title;
      $scope.beneficiarioSubtitle = copies.direccionBeneficiario.subtitle;
      $scope.tooltipBeneficiario = copies.direccionPersona.tooltipBeneficiario;
      $scope.textoBeneficiario = "";
    }

    //Pantalla de beneficiario
    $scope.beneficiarioNombreLabel = copies.direccionBeneficiario.nombre;
    $scope.beneficiarioPaternoLabel = copies.direccionBeneficiario.paterno;
    $scope.beneficiarioMaternoLabel = copies.direccionBeneficiario.materno;
    $scope.beneficiarioNacimientoLabel = copies.direccionBeneficiario.nacimiento;
    $scope.beneficiarioNacionalidadLabel = copies.direccionBeneficiario.nacionalidad;
    $scope.beneficiarioTitle3 = copies.direccionBeneficiario.title;
    $scope.beneficiarioTitle2 = copies.direccionBeneficiario.title2;
    $scope.beneficiariominititle = copies.direccionBeneficiario.minititle;
    $scope.beneficiarioTooltipDomicilio = copies.direccionBeneficiario.tooltipDomicilio;
    $scope.beneficiarioCalleLabel = copies.direccionBeneficiario.calle;
    $scope.beneficiarioNumeroExteriorLabel = copies.direccionBeneficiario.numeroExterior;
    $scope.beneficiarioNumeroInteriorLabel = copies.direccionBeneficiario.numeroInterior;
    $scope.beneficiarioCodigoPostalLabel = copies.direccionBeneficiario.codigoPostal;
    $scope.beneficiarioColoniaLabel = copies.direccionBeneficiario.colonia;
    $scope.beneficiarioRegistroCp = copies.direccionBeneficiario.registroCp;
    $scope.beneficiarioCodigoAyuda = copies.direccionBeneficiario.codigoAyuda;

    //Pantalla de contrato

    $scope.contratoTitle = copies.contrato.title;
    $scope.contratoSubtitle = copies.contrato.subtitle;
    $scope.terminos = copies.contrato.terminos;

    //Pantalla de buró
    $scope.buroTitle = copies.consultaBuro.title;
    $scope.buroDisclaimer = copies.consultaBuro.disclaimer;
    $scope.buroMensaje = copies.consultaBuro.mensaje;
    $scope.buroPreguntaTarjeta = copies.consultaBuro.pregunta1;
    $scope.buroPreguntaCredito = copies.consultaBuro.pregunta2;
    $scope.buroPreguntaAutomotriz = copies.consultaBuro.pregunta3;
    $scope.buroTerminos = copies.consultaBuro.terminos;

    //Pantalla de resúmen

    $scope.resumenTitle = copies.confirmation.title;
    $scope.resumenSubtitle = copies.confirmation.subtitle;
    $scope.resumenNombre = "NOMBRE DE CLIENTE";
    $scope.resumenCodigo = copies.confirmation.codigo;
    $scope.resumenTarjeta = copies.confirmation.tarjeta;
    $scope.resumenCuenta = copies.confirmation.cuenta;
    $scope.resumenClabe = copies.confirmation.clabe;
    $scope.resumenSucursal = copies.confirmation.sucursal;
  }

  getCopies(localStorageService.get('tipoDeMercado'));

  var pdc = this;
  $scope.ENV_VAR = "pre";
  $scope.step = 1;
  $scope.toGo = 3;
  /* se obtendrá del JSON */

  $scope.content.ingresarTelefono.dataPolicy = "";

  $scope.companys = [
      { id: "IU", company: "IUSACELL" },
      { id: "NE", company: "NEXTEL" },
      { id: "TE", company: "TELCEL" },
      { id: "TF", company: "TELEFONICA" },
      { id: "UN", company: "UNEFON" }
    ];
  /*****************************************/

  /*Datos formulario de contacto*/
  $scope.formCompanies = $scope.companys;
  $scope.dataPolicy = $scope.content.ingresarTelefono.dataPolicy;

  /*Datos formulario tarjeta de crédito*/



  /* REGEX necesarios */
  $scope.emailRegex = /^[a-zA-Z0-9_\-\.~]{4,}@[a-zA-Z0-9_\-\.~]{2,}\.[a-zA-Z]{2,4}$/;
  $scope.justNumbers = /^[0-9]+$/;




  utag.view({page:'/OBP_DatosContacto'});

  const mensajesError = (response) => {
    let cuatrocientoscuatro = "Hubo un Error con el servidor.";
    let quinientos = "El servidor tardó en responder";
    let desconocido = "Ocurrió un problema en el servidor.";
    if(response.status == 404){
      showErrorMessage(response.status, cuatrocientoscuatro);
    }else if(response.status == 500){
      showErrorMessage(response.status, quinientos);
    }else{
      showErrorMessage(response.status, desconocido);
    }
  }


// // // /* revision */
  //  $scope.contactoIsVisible = false; //inicialmente debe ser true
  //  $scope.contactoTelefonoIsVisible = false; //inicialmente debe ser true
  //  $scope.contactoOtroTelefonoIsVisible = false;
  //  $scope.validationCodeIsVisible = false;
  //  $scope.cardInformationIsVisible = false;
  // /* Branch Locator */
  //  $scope.branchLocatorIsVisible = false;
  // // /* Identificación */
  //  $scope.identificacionIsVisible = true;
  //  $scope.idSelectionIsVisible = false;
  //  $scope.idDatosIsVisible = true;
  //  $scope.beneficiarioIsVisible = false;
  // // /* contrato */
  //  $scope.contratoIsVisible = false;
  //  $scope.cuentaactivadaIsVisible = false;
  //  $scope.contraseniaIsVisible = false;
  //  $scope.nipIsVisible = false;
  //  $scope.buroIsVisible = false;
  //  $scope.encuestaIsVisible = false;

  /*Funciones de trabajo*/

  function setData(datos){
    localStorageService.set('idSolicitud', datos.idSolicitud );
    localStorageService.set('idNoCliente', datos.idNoCliente );
  }

  $scope.telefonoDiferente = function(){
    $scope.contactoTelefonoIsVisible = false; //inicialmente debe ser true
    $scope.contactoOtroTelefonoIsVisible = true;
    $scope.validationCodeIsVisible = false;
  }

  const sendAviso = (titulo, descripcion) => {
    console.log(descripcion);
    let alertaOptions = {
      headerText: titulo,
      modalIcon: 'images/icons/icono-error-validacion.png'
    }
    if(descripcion === undefined){
      alertaOptions.actionButtonText = "Ok";
      alertaOptions.showOk = true;
      alertaOptions.showBoth = false;
      alertaOptions.showClose = false;
      alertaOptions.bodyText = "Tiene una solicitud en proceso.";
    }
    else{
      alertaOptions.actionButtonText = "Continuar";
      alertaOptions.closeButtonText = "Nuevo";
      alertaOptions.showOk = false;
      alertaOptions.showBoth = true;
      alertaOptions.showClose = false;
      alertaOptions.bodyText = descripcion;
    }
    console.log(alertaOptions);
    return alertaService.showModal({}, alertaOptions);
  }

  const updateView = (storedStep, descripcion) => {
    // if(descripcion === undefined){
    //   storedStep = 5;
    // }
    console.log(storedStep);
    switch(storedStep){
      case 1:
        console.log("TARJETA");
        localStorageService.set('nuevaContratacion', true);
        var item = {
          $name : 'telefonoForm'
        };
        $scope.saveData(item);
        break;
      case 2:
        console.log("TARJETA");
        $scope.step = 2;
        if(localStorageService.get('tipoDeMercado') == '1'){
          $scope.cardInformationIsVisible = true;
          $scope.contactoTelefonoIsVisible = false;
          $scope.contactoOtroTelefonoIsVisible = false;
          $scope.validationCodeIsVisible = false;
        }else {
          $scope.branchLocatorIsVisible = true;
          $scope.cardInformationIsVisible = false;
          $scope.contactoTelefonoIsVisible = false;
          $scope.contactoOtroTelefonoIsVisible = false;
          $scope.validationCodeIsVisible = false;
        }
        break;
      case 3:
        if(localStorageService.get('tipoDeMercado') == '1'){
          $scope.cardInformationIsVisible = true;
          $scope.contactoTelefonoIsVisible = false;
          $scope.contactoOtroTelefonoIsVisible = false;
          $scope.validationCodeIsVisible = false;
        }else {
          $scope.branchLocatorIsVisible = true;
          $scope.cardInformationIsVisible = false;
          $scope.contactoTelefonoIsVisible = false;
          $scope.contactoOtroTelefonoIsVisible = false;
          $scope.validationCodeIsVisible = false;
        }
        break;
      case 4:
        console.log("PROSPECTO");
        $scope.step = 2;
        $scope.formTitle = "Cuéntanos acerca de ti";
        $scope.contactoIsVisible = false;
        $scope.identificacionIsVisible = true;
        $scope.idSelectionIsVisible = false;
        $scope.idDatosIsVisible = true;
        break;
      case 5:
        console.log("CONTRATO");

        $scope.step = 3;
        $scope.formTitle = "Creación de cuenta";
        $scope.contactoTelefonoIsVisible = false;
        $scope.contactoOtroTelefonoIsVisible = false;
        $scope.validationCodeIsVisible = false;
        $scope.identificacionIsVisible = false;
        $scope.contratoIsVisible = true;
        break;
    }

  }



  // const getActividad = rol => {
  //   let actividad = {};
  //   connection.getActividades()
  //     .then((response) => {
  //       angular.forEach(response.data.datos, (tempActivity) => {
  //         if(tempActivity.actividadEspecifica == rol) actividad = tempActivity;
  //       });
  //     }, errorConnection);
  //
  //   return actividad;
  // }

  const getDomInfo = zipcode => {
    connection.sendCp(zipcode)
      .then((response) => {
        if(response.data.estatus !== 0){
          showErrorMessage(response.data.estatus, response.data.descripcion);
          identificacion.cpResponse = [];
        }else{
          let cpResponse = response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;

          let selectedColonia = cpResponse[cpResponse.length - 1];
          localStorageService.set('delegacion', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
          localStorageService.set('ciudad', response.data.datos.lstEstado[0].ciudades[0].ciudad);
          localStorageService.set('estado', response.data.datos.lstEstado[0].estado);
          localStorageService.set('colonia', cpResponse[cpResponse.length -1] );
        }
        //identificacion.loader = false;
      }, errorConnection);
  }

  const getEntidad = (nacId, entidadId) => {
    let entidad = {};
    connection.getPaises('Nacimiento_.json')
      .then(function(response){
        if(nacId == '052'){
           angular.forEach(response.data.datos.estados, (estado) => {
             if(entidadId == estado.id) {
               entidad = estado
             }
           });
         } else {
           angular.forEach(response.data.datos.paises, (pais) => {
             if(entidadId == pais.id) {
                entidad = pais
              }
            });
         }
      }, errorConnection);
      return entidad;
  }

  const setDatosCliente = (datos, domicilio) => {
    console.log("---info basica -----> " + datos);
    connection.getPaises('Nacimiento_.json')
      .then(function(response){
        let estados = response.data.datos.estados;
        let paises = response.data.datos.paises;
        //console.log(datos.entFedNac);
        if(datos !== undefined){
          if(datos.nombrePersona !== undefined)
            localStorageService.set('nombre', datos.nombrePersona.toUpperCase());
          if(datos.apellidoPaterno !== undefined)
            localStorageService.set('apellidopaterno', datos.apellidoPaterno.toUpperCase());
          if(datos.apellidoMaterno !== undefined){
            localStorageService.set('apellidomaterno', datos.apellidoMaterno.toUpperCase());
          } else {
            localStorageService.set('apellidomaterno', "");
          }
          if(datos.fechaNacimiento !== undefined)
            localStorageService.set('fechanacimiento', datos.fechaNacimiento);
          if(datos.sexo !== undefined)
            localStorageService.set('gender', datos.sexo);
          if(datos.paisNacimiento !== undefined)
            localStorageService.set('paisDeNacimiento', datos.paisNacimiento);
          if(datos.actEspecifica !== undefined)
            localStorageService.set('roluniversitario', datos.actEspecifica);

          console.log(datos.actEspecifica);

          localStorageService.set('emailContacto', datos.mail);
          if(datos.paisNacimiento == "052") {
            console.log(datos.entFedNac);
            angular.forEach(estados, function(estado){
              if(estado.id == datos.entFedNac) {
                console.log("entre");

                localStorageService.set('entidadnacimiento', estado);
              }
            });
          } else {
            angular.forEach(paises, function(pais){
              if(pais.id == datos.entFedNac) localStorageService.set('entidadnacimiento', pais);
            });
          }
          console.log(localStorageService.get('entidadnacimiento'));

        }
        //domicilio
        if(domicilio !== undefined){
          if(domicilio.codPostal !== undefined){
            localStorageService.set('zipcode', domicilio.codPostal);
            getDomInfo(domicilio.codPostal);
          }
          if(domicilio.nombreVia !== undefined)
            localStorageService.set('calle', domicilio.nombreVia);
          if(domicilio.numeroExterior !== undefined)
            localStorageService.set('numeroexterior', domicilio.numeroExterior);
          if(domicilio.numeroInterior !== undefined)
            localStorageService.set('numerointerior', domicilio.numeroInterior);
          if(domicilio.asentamiento !== undefined)
            localStorageService.set('retrieveasentamiento', domicilio.asentamiento);
        }

      }, function(response){
        console.log(response.status);
      })
      .finally(function(){
        console.log(localStorageService.get('entidadnacimiento'));
        $rootScope.$broadcast("datos_editados");
      });
  }



  function getSaveRetrieve(datos){
    console.log(datos);
      sendAviso('Solicitud pendiente', datos.questionToShow)
        .then((response) => {

          localStorageService.set('nuevaContratacion', false);
          localStorageService.set('idSolicitud', datos.idSolicitud);
          localStorageService.set('idNoCliente', datos.idNocliente);
          localStorageService.set('clienteEditable', datos.datosBasicosEditable);
          localStorageService.set('beneficiarioEditable', datos.beneficiarioEditable);
          localStorageService.set('branch', datos.idSucursal);
          if(!localStorageService.get('sucursal')) localStorageService.set('sucursal', datos.nombreSucursal);
          localStorageService.set('solicitudRetomada', true);
          if(datos.codigoPersona !== undefined){
            let datosProspecto = {
              codigoPersona: datos.codigoPersona
            }
            localStorageService.set('datosProspecto', datosProspecto);
          }
          var item = {
            $name : 'telefonoForm'
          };
          $scope.saveData(item);


        }, (response) => {
          localStorageService.set('nuevaContratacion', true);
          console.log('Nuevo');
          var item = {
            $name : 'telefonoForm'
          };
          $scope.saveData(item);
        });
  }

  $scope.saveData = function(item){
    switch(item.$name){
      case 'telefonoForm':
        $scope.loader = true;
        connection.sendDatosContacto()
          .then(function(response){
            console.log("----estatus----> " + response.data.estatus);
            if(response.data.estatus === -4 && localStorageService.get('nuevaContratacion') !== true){
              //modificación para iniciar en SMS
              localStorageService.set('initsr', true);
              localStorageService.set('srdata', response.data.datos);

              getSaveRetrieve(response.data.datos);

            }else if (response.data.estatus === -5) {
              //localStorageService.set('ongoingprocess', true);
              showErrorMessage(response.data.estatus, "La solicitud está siendo procesada, vuelve a intentarlo más tarde.");
            }else if (response.data.estatus !== 0) {
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              //setData(response.data.datos);
              if(!localStorageService.get('initsr') || localStorageService.get('nuevaContratacion')){
                localStorageService.set('idSolicitud', response.data.datos.idSolicitud);
                localStorageService.set('idNoCliente', response.data.datos.idNoCliente);
              }
              console.log(response.data.datos.token);
              $scope.token = response.data.datos.token;
              $scope.validationCodeIsVisible = true;
              utag.view({page:'/OBP_CodigoConfirmacion'});
              $location.hash('SEC_ValidacionCodigo');
              $anchorScroll();
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'nuevoTelefonoForm':
        $scope.loader = true;
        connection.sendDatosContacto('otroTelefono')
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              $scope.token = response.data.datos.token;
              $scope.validationCodeIsVisible = true;
              $location.hash('SEC_ValidacionCodigo');
              $anchorScroll();
              utag.view({page:'/OBP_NuevoNumero'});
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          })
        break;
      case 'otroTelefonoForm':
        $scope.loader = true;
        connection.sendDatosContacto('otroTelefono')
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              console.log(response.data.datos);
              $scope.validationCodeIsVisible = true;
              $location.hash('SEC_ValidacionCodigo');
              $anchorScroll();
              utag.view({page:'/OBP_CodigoConfirmacion'});
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'smsForm':
        // if(localStorageService.get('smsToken')!== $scope.token){
        //   showErrorMessage('Token erróneo', 'Vuelve a enviar el token o revisa que tu token esté correcto');
        // }else{
          $scope.loader=true;
          connection.sendSmsToken()
            .then(function(response){
              if(localStorageService.get('initsr') && response.data.estatus === 0 && !localStorageService.get('nuevaContratacion')){
                let datos = localStorageService.get('srdata');
                if(datos.hasOwnProperty('datosBasicos') && datos.datosBasicos.hasOwnProperty('nombrePersona')) {
                  setDatosCliente(datos.datosBasicos, datos.domicilio);
                  localStorageService.set('saveandretrieveDatoseditables', true);
                }else if(datos.hasOwnProperty('datosBasicos') && !datos.datosBasicos.hasOwnProperty('nombrePersona') || !datos.hasOwnProperty('datosBasicos')){
                  localStorageService.set('saveandretrieveDatoseditables', false);
                }
                //if(datos.beneficiarioEditable === true) setDatosBeneficiario();
                console.log('Continuar');
                console.log(datos.paso);
                updateView(datos.paso, datos.questionToShow);
              }else if(response.data.estatus !== 0){
                showErrorMessage(response.data.estatus, response.data.descripcion);
              }else{
                if(localStorageService.get('tipoDeMercado') == '1'){
                  $scope.cardInformationIsVisible = true;
                  $scope.contactoTelefonoIsVisible = false;
                  $scope.contactoOtroTelefonoIsVisible = false;
                  $scope.validationCodeIsVisible = false;
                  utag.view({page:'/OBP_Tarjeta'});
                }else{
                  $scope.branchLocatorIsVisible = true;
                  $scope.contactoTelefonoIsVisible = false;
                  $scope.contactoOtroTelefonoIsVisible = false;
                  $scope.validationCodeIsVisible = false;
                  utag.view({page:'/OBP_BranchLocator'});
                }

              }
              $scope.loader = false;
            }, function(response){
              mensajesError(response);
              $scope.loader = false;
            });
        // }
        break;
      case 'selectedBranch':
        $scope.loader = true;
        if($scope.esMercadoAbierto){
          connection.createPanData()
            .then(function(response){
              if(response.data.estatus !== 0){
                showErrorMessage(response.data.estatus, response.data.descripcion);
              }else{
                $scope.step = 2;
                $scope.formTitle = "Cuéntanos acerca de ti";
                $scope.branchLocatorIsVisible = false;
                $scope.contactoIsVisible = false;
                $scope.identificacionIsVisible = true;
                $scope.idSelectionIsVisible = true;
                utag.view({page:'/OBP_EnviarDocs'});
                //$scope.branchLocatorIsVisible = false;
                //$scope.contactoTelefonoIsVisible = true;
                //$scope.cardInformationIsVisible = true;
              }
            }, function(response){
              mensajesError(response);
              $scope.loader = false;
            })
        }else{
          $scope.branchLocatorIsVisible = false;
          //$scope.contactoTelefonoIsVisible = true;
          $scope.cardInformationIsVisible = true;
        }

        $scope.loader = false;
        utag.view({page:'/OBP_Tarjeta'});
        break;
      case 'cardForm':
        $scope.loader = true;
        if($scope.esUniversidades || $scope.esNomina){
          connection.sendPanData()
            .then(function(response){
              if(response.data.estatus !== 0){
                showErrorMessage(response.data.estatus, response.data.descripcion);
              }else{
                $scope.step = 2;
                $scope.formTitle = "Cuéntanos acerca de ti";
                $scope.contactoIsVisible = false;
                $scope.identificacionIsVisible = true;
                $scope.idSelectionIsVisible = true;
                utag.view({page:'/OBP_EnviarDocs'});
              }
              $scope.loader = false;
            }, function(response){
              mensajesError(response);
              $scope.loader = false;
            });
        }else{
          $scope.step = 3;
          $scope.formTitle = "Creación de cuenta";
          $scope.contactoIsVisible = false;
          $scope.identificacionIsVisible = false;
          $scope.contratoIsVisible = true;
          utag.view({page:'/OBP_Contrato'});
        }

        break;

      case 'createCard':
          $scope.loader = true;
          if($scope.esUniversidades || $scope.esNomina){
            connection.createPanData()
              .then(function(response){
                if(response.data.estatus !== 0){
                  showErrorMessage(response.data.estatus, response.data.descripcion);

                }else{
                  $scope.step = 2;
                  $scope.formTitle = "Cuéntanos acerca de ti";
                  $scope.contactoIsVisible = false;
                  $scope.identificacionIsVisible = true;
                  $scope.idSelectionIsVisible = true;
                  utag.view({page:'/OBP_EnviarDocs'});
                }

              }, function(response){
                mensajesError(response);
              });
          }else{
            $scope.step = 3;
            $scope.formTitle = "Creación de cuenta";
            $scope.contactoIsVisible = false;
            $scope.identificacionIsVisible = false;
            $scope.contratoIsVisible = true;
            utag.view({page:'/OBP_Contrato'});
          }
          $scope.loader = false;

          break;
      case 'branchSelected':
      $scope.loader = true;
      connection.createPanData()
        .then(function(response){
          if(response.data.estatus !== 0){
            showErrorMessage(response.data.estatus, response.data.descripcion);
          }else{
            $scope.step = 2;
            $scope.formTitle = "Cuéntanos acerca de ti";
            $scope.contactoIsVisible = false;
            $scope.identificacionIsVisible = true;
            $scope.idSelectionIsVisible = true;
            utag.view({page:'/OBP_EnviarDocs'});
          }
          $scope.loader = false;
        },function(response){
          mensajesError(response);
          $scope.loader = false;
        });
        break;
      case 'ineForm':
        $scope.loader = true;
        connection.sendIdentification()
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              $scope.idSelectionIsVisible = false;
              $scope.idDatosIsVisible = true;
              utag.view({page:'/OBP_DatosCliente'});
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'clientForm':
        $scope.loader = true;
        connection.sendDatosPersona()
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              localStorageService.set('datosProspecto', response.data.datos);
              if($scope.esMercadoAbierto){
                console.log($scope);

                $scope.contactoIsVisible = true;
                $scope.contactoTelefonoIsVisible = false;
                $scope.validationCodeIsVisible = false;
                $scope.cardInformationIsVisible = true;
                $scope.identificacionIsVisible = false;
              }else{

                $scope.step = 3;
                $scope.formTitle = "Creación de cuenta";
                $scope.identificacionIsVisible = false;
                $scope.contratoIsVisible = true;
                utag.view({page:'/OBP_Contrato'});
              }

            }
            $scope.loader = false;
          },function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      default:
        showErrorMessage(-3, 'Hubo un error con la aplicacíon');
        break;
    }
  }
  $scope.next = function(value){
    switch(value){
      case 'aceptado':
        $scope.loader = true;
        connection.sendAcepto()
          .then(function(response){
            let datosGuardados = localStorageService.get('srdata');
            console.log(response.data);
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }
            else{
              localStorageService.set('contratoAceptado', response.data.datos);
              $scope.datosProspecto = localStorageService.get('datosProspecto');
              if(!$scope.datosProspecto.hasOwnProperty('nombrePersona')){
                $scope.datosProspecto.nombrePersona = localStorageService.get('nombre') + " " + localStorageService.get('apellidopaterno') + " " + localStorageService.get('apellidomaterno');
              }
              $scope.datos = localStorageService.get('contratoAceptado');
              $scope.clabe = $scope.datos.clabe.substring(6,17);
              $scope.card = $scope.datos.pan;
              $scope.sucursal = localStorageService.get('sucursal') ? localStorageService.get('sucursal') : datosGuardados.nombreSucursal  ;
              $scope.contratoIsVisible = false;
              //$scope.nipIsVisible = true;
              $scope.cuentaactivadaIsVisible = true;
              utag.view({page:'/OBP_CuentaCreada'});
              console.log($scope.datosProspecto.codigoPersona);
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
          $scope.loader = false;
        break;
      case 'enviarCodigo':
        connection.saveNip()
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus.response.data.descripcion);
            } else {
              $scope.nipIsVisible = false;
              $scope.cuentaactivadaIsVisible = true;
              utag.view({page:'/OBP_CuentaCreada'});
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'datosrevisados':
        $scope.cuentaactivadaIsVisible = false;
        //if(localStorageService.get('tipoDeMercado') === 1 ){
        //  $scope.buroIsVisible = true;
        //  utag.view({page:'/OBP_Buro'});
        //}else{
          $scope.contraseniaIsVisible = true;
          utag.view({page:'/OBP_Contrasena'});
        //}
        break;

      case 'sendPassword':
        $scope.loader= true;
        connection.sendPass()
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              $scope.contraseniaIsVisible = false;
              //$scope.nipIsVisible = true;
              $scope.encuestaIsVisible = true;
              //utag.view({page:'/OBP_nip'});
              utag.view({page:'OBP_Encuesta'});
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'enviarPin':
        $scope.loader = true;
        connection.sendpin()
          .then(function(response){
            if(response.data.estatus !== 0) showErrorMessage(response.data.estatus, response.data.descripcion);
            else{
              $scope.nipIsVisible = false;
              $scope.encuestaIsVisible = true;
              utag.view({page:'OBP_Encuesta'});
            }
            $scope.loader = false;
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'enviarBuro':
        $scope.loader = true;
        connection.sendConBuro()
          .then(function(response){
            if(response.data.estatus !== 0){
              showErrorMessage(response.data.estatus, response.data.descripcion);
            }else{
              $scope.buroIsVisible = false;
              $scope.encuestaIsVisible = true;
              utag.view({page:'OBP_Encuesta'});
            }
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      case 'enviarEncuesta':
        $scope.loader = true;
        connection.sendEncuesta()
          .then(function(response){
            $scope.loader = false;
            $window.location.href = 'http://www.santander.com.mx/';
          }, function(response){
            mensajesError(response);
            $scope.loader = false;
          });
        break;
      default:
        break;
    }
  }

  var showErrorMessage = function(id, value){
    //console.log($crypto.decrypt(value, 'obparticulares'));
    console.log(value);
    var modalOptions = {};
    if(id == -3){
      value = $crypto.decrypt(value);
      modalOptions = {
        actionButtonText: 'Volver a intentar',
        closeButtonText: 'Ok',
        showOk: false,
        showClose: true,
        headerText: 'Error ',
        modalIcon: 'images/icons/icono-error-validacion.png',
        bodyText: value
      };
    }else if (id == -5) {
      modalOptions = {
        actionButtonText: 'Volver a intentar',
        showOk: true,
        headerText: 'Error ',
        modalIcon: 'images/icons/icono-error-validacion.png',
        bodyText: value
      };
    }else if(id == -2){
      $crypto.decrypt(value);
      modalOptions = {
        actionButtonText: 'Volver a intentar',
        showOk: true,
        headerText: 'Error ',
        modalIcon: 'images/icons/icono-error-validacion.png',
        bodyText: "Por el momento el servicio no está disponible, por favor intenta nuevamente más tarde."
      };
    }else if(id == 404 || id == 500){
      value = $crypto.decrypt(value);
      modalOptions = {
        actionButtonText: 'Volver a intentar',
        showOk: true,
        showClose: false,
        headerText: 'Error ',
        modalIcon: 'images/icons/icono-error-validacion.png',
        bodyText: 'Por el momento el servicio no está disponible, por favor intenta nuevamente más tarde.'
      };
    }else{
      value = $crypto.decrypt(value);
      modalOptions = {
        actionButtonText: 'Volver a intentar',
        showOk: true,
        headerText: 'Error ',
        modalIcon: 'images/icons/icono-error-validacion.png',
        bodyText: value
      };
    }
    return modalService.showModal({}, modalOptions);
  };

}]);
