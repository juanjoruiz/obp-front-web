(function(){
  angular.module('isbanWebApp')
    .controller('DatosClienteController', DatosClienteController);

  function DatosClienteController(localStorageService, connection, $state, config, $rootScope, $document, $location, baseConfig, $scope) {
    let vm = this;
    let copies = localStorageService.get('copies');

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_DatosCliente_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_DatosCliente_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_DatosCliente_MA_'+env});
          break;
        case '4':
          utag.view({page:'/OBP_DatosCliente_MA_'+env});
          break;
      }
    }

    //vm.notEditable = true;

    function getEntidad(){
      return  connection.getPaises('Nacimiento_.json')
          .then(function(response){
            console.log(response);
            vm.estados = response.datos.estados;
            vm.paises = response.datos.paises;
            return response.datos;

          }, function(response){
            console.log(response.status);
          });
    }
    getEntidad();



    vm.datosPersonaTitulo = copies.dataPersona.title;
    vm.datosPersonaMensaje = copies.dataPersona.subtitle;
    vm.datosPersonaNombre = copies.dataPersona.nombre;
    vm.datosPersonaPaterno = copies.dataPersona.paterno;
    vm.datosPersonaMaterno = copies.dataPersona.materno;
    vm.datosPersonaSexo = copies.dataPersona.sexo;
    vm.datosPersonaNacimiento = copies.dataPersona.nacimiento;
    vm.datosPersonaPaisNacimiento = "País de nacimiento";
    vm.datosPersonaEntidad = copies.dataPersona.entidad;
    vm.agregarAhora = copies.direccionPersona.agregarAhora;
    vm.agregarDespues = copies.direccionPersona.agregarDespues;
    vm.datosPersonaTitulo2 = copies.direccionPersona.title;
    vm.datosPersonaCalleLabel = copies.direccionPersona.calle;
    vm.datosPersonaNumeroExteriorLabel = copies.direccionPersona.numeroExterior;
    vm.datosPersonaNumeroInteriorLabel = copies.direccionPersona.numeroInterior;
    vm.datosPersonaCodigoPostalLabel = copies.direccionPersona.codigoPostal;
    vm.datosPersonaRegistroCp = copies.direccionPersona.registroCp;
    vm.datosPersonaColoniaLabel = copies.direccionPersona.colonia;
    vm.datosPersonaCodigoAyuda = copies.direccionPersona.codigoAyuda;
    vm.datosPersonaMunicipioLabel = copies.direccionPersona.colonia;
    vm.datosPersonaCiudadLabel = copies.direccionPersona.ciudad;
    vm.datosPersonaEstadoLabel = copies.direccionPersona.estado;
    vm.beneficiarioTitle = copies.direccionPersona.titleBeneficiario;
    vm.beneficiarioSubtitle = copies.direccionPersona.subtitle;
    vm.tooltipBeneficiario = copies.direccionPersona.tooltipBeneficiario;
    vm.textoBeneficiario = copies.direccionPersona.textoBeneficiario;
    vm.beneficiarioTitle = copies.direccionBeneficiario.title;
    vm.beneficiarioSubtitle = copies.direccionBeneficiario.subtitle;
    vm.tooltipBeneficiario = copies.direccionPersona.tooltipBeneficiario;
    vm.textoBeneficiario = "";
    vm.adultoAceptado = true;

    //vm.notEditable = false;
    vm.soloCliente = true;

    function getYears(){
      var thisYear = new Date().getFullYear();
      var numThisYear = parseInt(thisYear.toString());
      return numThisYear;
    }


    function calcAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    }

    var max = getYears() - 18;
    var min = getYears() - 100;

    vm.popup1 = {
      opened: false
    };

    vm.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(2050, 5, 22),
      minDate: new Date(1930, 1, 1),
      startingDay: 1
    };

    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.open1 = function() {
      vm.popup1.opened = true;
    };

    $(function(){
      let clase = " ";
        clase = "form-control"
        $('#DATE_fechanacimiento').combodate({
          customClass: clase,
          minYear: min,
          maxYear: max
        });
    });

    vm.saveNombre = () => {
      if(vm.nombrePersona !== undefined){
        vm.nombrePersona = vm.nombrePersona.toUpperCase();
        localStorageService.set('nombre', vm.nombrePersona.toUpperCase());
      }
    }

    vm.saveApPaterno = () => {
      if(vm.apellidoPaterno !== undefined){
        vm.apellidoPaterno = vm.apellidoPaterno.toUpperCase();
        localStorageService.set('apellidopaterno', vm.apellidoPaterno.toUpperCase());
      }
    }

    vm.saveApMaterno = () => {
      if(vm.apellidoMaterno !== undefined){
        vm.apellidoMaterno = vm.apellidoMaterno.toUpperCase();
        localStorageService.set('apellidomaterno', vm.apellidoMaterno.toUpperCase());
      }else{
        localStorageService.set('apellidomaterno', '');
      }
    }
    vm.saveGender = () => localStorageService.set('gender', vm.sexo.toUpperCase());

    vm.saveFechaNacimiento = () => {
      let bornTime = new Date(vm.fechaNacimiento);
      let age = calcAge(bornTime);
      $scope.clientForm.fechaNacimiento.$setValidity("adulto", age>=18);
      if(age >= 18){
        vm.adultoAceptado = true;
        localStorageService.set('fechanacimiento', vm.fechaNacimiento);
      }else{
        let date = document.getElementById("DATE_fechanacimiento");
        date.className += "has-error";
        vm.adultoAceptado = false;
      }
    }

    vm.saveNacionalidad = (value) => {
      if(value != "saveandretrieve") localStorageService.set('paisDeNacimiento', vm.nacionalidad);
      console.log(vm.nacionalidad);
      if(vm.nacionalidad == 'M'){
        vm.chooseEntidad = vm.estados;
        console.log(vm.estados);
        let entidad = document.querySelector("#SELECT_entidadNacimiento");
        entidad.options = vm.estados;
      }else{
        vm.chooseEntidad = vm.paises;
        let entidad = document.querySelector("#SELECT_entidadNacimiento");
        entidad.options = vm.paises;
      }
    }

    vm.disableEntidad = true;
    vm.saveNacionalidadNew = function(value){
      vm.nacionalidad = value.descripcion;
      if(value.id == '052'){
        vm.disableEntidad = false;
        vm.chooseEntidad = vm.estados;
      }else{
        vm.disableEntidad = true;
      }
    }

    vm.saveRolUni = () => localStorageService.set('roluniversitario', vm.rolUniversitario);

    vm.showOcupacion = true;
    vm.checkilicita = function(){
      if(vm.ilicita.descripcion != "Ninguna de las anteriores"){
        let message = {
          id: 999,
          titulo: "Actividad genérica",
          descripcion: "Has seleccionado " + vm.ilicita.descripcion
        }
        config.generalMessages(message).then(function(){
          let message = {
            id: 1000,
            descripcion: 'No podemos continuar con el proceso de contratación, seguimos a sus órdenes'
          }
          config.generalMessages(message);
        }, function(){
          vm.ilicita = null;
        });

      }else{
        vm.showOcupacion = false;
      }
    }

    vm.saveCalle = function(){
      if(vm.calle !== undefined){
        vm.calle = vm.calle.toUpperCase();
        localStorageService.set('calle', vm.calle);
      }
    }

    vm.saveExterior = () => localStorageService.set('numeroexterior', vm.exterior);
    vm.saveInterior = () => localStorageService.set('numerointerior', vm.interior);

    vm.saveZipCode = (value) => {
      if(vm.zipcode !== undefined && vm.zipcode !== null && vm.zipcode !== ""){
        if(vm.zipcode.length === 5){
          localStorageService.set('zipcode', vm.zipcode);
          connection.sendCp(localStorageService.get('zipcode'))
            .then(function(response){
              if(response.estatus !== 0){
                console.log(response.estatus);
                let error = {
                  status: response.estatus,
                  descripcion: response.descripcion,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo
                }
                config.generalErrors(error);
                vm.cpResponse = [];
              }else{
                vm.cpResponse = response.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;
                if(value != "saveandretrieve"){
                  vm.selectedColonia = vm.cpResponse[vm.cpResponse.length - 1];
                  localStorageService.set('delegacion', response.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
                  localStorageService.set('ciudad', response.datos.lstEstado[0].ciudades[0].ciudad);
                  localStorageService.set('estado', response.datos.lstEstado[0].estado);
                  localStorageService.set('colonia', vm.cpResponse[vm.cpResponse.length -1] );
                }else{
                  let asentamientoRetrieved = localStorageService.get('retrieveasentamiento');
                  angular.forEach(vm.cpResponse, function(asentamiento){
                    if(asentamiento.nombre == asentamientoRetrieved){
                      vm.selectedColonia = asentamiento;
                      localStorageService.set('delegacion', response.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
                      localStorageService.set('ciudad', response.datos.lstEstado[0].ciudades[0].ciudad);
                      localStorageService.set('estado', response.datos.lstEstado[0].estado);
                      localStorageService.set('colonia', vm.cpResponse[vm.cpResponse.length -1] );
                    }
                  });
                }
              }
            });
        }
      }
    }

    vm.savePoblacion = () => {
      localStorageService.set('colonia', vm.selectedColonia);
    }

    vm.saveEntidadNacimiento = () => {
      localStorageService.set('entidadnacimiento', vm.entidadNacimiento);
    }

    vm.validarCliente = () => {
      connection.sendDatosPersona()
        .then(function(response){
          if(response.estatus === 0){
            localStorageService.set('datosProspecto', response.datos);
            config.nextStep(1);
          }else{
            let error = {
              status: response.estatus,
              descripcion: response.descripcion,
              errorCode: response.codigo,
              tipo: response.tipo,
              titulo: response.titulo
            }
            config.generalErrors(error);
          }
        })
    }

    vm.showBeneficiario = () => {
      localStorageService.set('addBeneficiario', vm.addBeneficiario);
      if(vm.addBeneficiario === 's'){
        $rootScope.$broadcast('showBeneficiario');
        return true;
      }else{
        $rootScope.$broadcast('hideBeneficiario');
        return false;
      }
    }



    connection.getActividades('actividades.json')
      .then(function(response){
        switch(localStorageService.get('tipoDeMercado')){
          case '1':
            vm.titleActividad = copies.dataPersona.rol;
            vm.showIlicitas = false;
            vm.showOcupacion = false;
            vm.uniRoles = response.data.datos.universidades;
            break;
          case '2':
            vm.actividadIlicita = copies.dataPersona.rol2;
            vm.titleActividad = copies.dataPersona.ocupacion;
            vm.showIlicitas = true;
            vm.uniRoles = response.data.datos.ocupacionesNomina;
            vm.ilicitas = response.data.datos.bloqueante;
            break;
          case '3':
            vm.actividadIlicita = copies.dataPersona.rol2;
            vm.titleActividad = copies.dataPersona.ocupacion;
            vm.showIlicitas = true;
            vm.uniRoles = response.data.datos.ocupaciones;
            vm.ilicitas = response.data.datos.bloqueante;
            break;
          case '4':
              vm.actividadIlicita = copies.dataPersona.rol2;
              vm.titleActividad = copies.dataPersona.ocupacion;
              vm.showIlicitas = true;
              vm.uniRoles = response.data.datos.ocupaciones;
              vm.ilicitas = response.data.datos.bloqueante;
              break;
        }
      }, function(response){
        console.log(response);
      });

    let fillData = () => {

      setTimeout(function(){


        if(localStorageService.get('tipoDeMercado') == '2' || localStorageService.get('tipoDeMercado') == '3' || localStorageService.get('tipoDeMercado') == '4'){
          let ilicita = document.querySelector('#SELECT_actividadilicita');
          if(localStorageService.get('srstepretrive') == '4'){
            if(!localStorageService.get('clienteEditable')){
              ilicita.selectedIndex = ilicita.options.length - 1;
            }
          }
        }

        let entidad = document.querySelector("#SELECT_entidadNacimiento");
        vm.nombrePersona = localStorageService.get('nombre');
        vm.apellidoPaterno = localStorageService.get('apellidopaterno');
        vm.apellidoMaterno = localStorageService.get('apellidomaterno');
        vm.sexo = localStorageService.get('gender');
        vm.fechaNacimiento = localStorageService.get('fechanacimiento');
        $('#DATE_fechanacimiento').combodate('setValue', vm.fechaNacimiento);
        vm.nacionalidad = localStorageService.get('paisDeNacimiento');
        vm.rolUniversitario = localStorageService.get('roluniversitario');
        vm.calle = localStorageService.get('calle');
        vm.exterior = localStorageService.get('numeroexterior');
        vm.interior = localStorageService.get('numerointerior');
        vm.zipcode = localStorageService.get('zipcode');
        let paisNacimientoGuardado = localStorageService.get('paisDeNacimiento');
        let rolSelector = document.getElementById('SELECT_rolUniversitario');
        let actividadEspecificaGuardada = localStorageService.get('roluniversitario');
        angular.forEach(vm.uniRoles, function(rol){
          if(rol.actEspecifica == actividadEspecificaGuardada) {
            vm.rolUniversitario = rol;
            vm.saveRolUni();
          }
        });

        if(paisNacimientoGuardado == '052') {
          console.log('entre');
          vm.nacionalidad = 'M';
          let value = "saveandretrieve";
          vm.saveNacionalidad(value);

          angular.forEach(vm.estados, function(estado){
            if(estado.id == localStorageService.get('entidadnacimiento').id){
              vm.entidadNacimiento = estado;
              vm.saveEntidadNacimiento();
            }
          });
        } else {
          vm.nacionalidad = 'E';
          let value = "saveandretrieve";
          vm.saveNacionalidad(value);
          angular.forEach(vm.paises, function(pais){
            if(pais.id == localStorageService.get('paisDeNacimiento')){
              vm.entidadNacimiento = pais;
              vm.saveEntidadNacimiento();
            }
          });
        }
        vm.saveZipCode("saveandretrieve");
        $scope.$apply();
      }, 1000);
    }

    $document.ready(function(){
      if(localStorageService.get('initsr') || localStorageService.get('initsrcp') || localStorageService.get('initsrocr')){
        let srstep = localStorageService.get('srdata');
        //if(srstep == null) srstep = 4;
        if(srstep === null && localStorageService.get('initsrocr')){
          vm.notEditable = false;
          $('.combodate').children().attr('disabled', false);
          fillData();
        }
        else if(srstep.paso >= 4 || localStorageService.get('initsrcp')){
          if(localStorageService.get('nuevaContratacion')){
            vm.notEditable = false;
            $('.combodate').children().attr('disabled', false);
          }else{
            vm.notEditable = !localStorageService.get('clienteEditable');
            $('.combodate').children().attr('disabled', !localStorageService.get('clienteEditable'));
          }


          fillData();

        }
      }
    });


  }

  DatosClienteController.$inject = ['localStorageService', 'connection', '$state', 'config', '$rootScope', '$document', '$location', 'baseConfig', '$scope'];
})();
