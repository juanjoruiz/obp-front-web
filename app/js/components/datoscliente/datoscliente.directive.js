var app = angular.module('isbanWebApp');

app.directive('datoscliente', function(){
  return{
    restrict: 'A',
    templateUrl: 'js/components/datoscliente/datoscliente.view.html',
    controllerAs: 'datosclienteCtrl',
    controller: 'DatosClienteController'
  }
});
