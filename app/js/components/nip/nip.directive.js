var app = angular.module('isbanWebApp');

app.directive('nip', function(){
  return{
    restrict: 'E',
    templateUrl: 'js/components/nip/nip.view.html',
    controllerAs: 'nip',
    controller: ['$scope','connection', '$base64', 'localStorageService', function($scope, connection, $base64, localStorageService){

      var nip = this;

      let env = baseConfig.ENV.toUpperCase();
      if(localStorageService.get('configuracion').ENV_VAR == 'pro'){
        switch(localStorageService.get('tipoDeMercado')){
          case '1':
            utag.view({page:'/OBP_Nip_UNI_'+env});
            break;
          case '2':
            utag.view({page:'/OBP_Nip_NOM_'+env});
            break;
          case '3':
            utag.view({page:'/OBP_Nip_MA_'+env});
            break;
        }
      }

      nip.disableSelect = false;
      nip.disableConfirmation = true;
      nip.showtick = false;
      nip.disableBorrar = true;
      nip.instrucciones = "Genera un NIP de 4 dígitos con el teclado que se presenta para tener acceso a los cajeros automáticos.";
      nip.labelNip = "NIP cajero automático";
      nip.labelConfNip = "Confirmar NIP";
      nip.button = {
        imagen: '',
        valor: '',
        clase: '',
        id: ''
      };
      nip.buttons = [];
      nip.selectedValues = [];
      nip.confirmationValues = [];
      nip.borrar = {
        imagenActiva: "images/data/btnBorrarActive.png",
        imagenInactiva:"images/data/btnBorrarInactive.png",
        valor: 'borrar',
        clase: '',
        id: 'nip_btn_borrar'
      }
      nip.saveAndSend = () => {
        localStorageService.set('selectedNumbers', nip.selectedValues.join(""));
        $scope.next('enviarCodigo');
      }

      var validateNip = function(){
        if(nip.selectedValues.length === 0 || nip.confirmationValues.length === 0) {
          nip.showtick = false;
          return;
        }
        cont = 0;
        for(var i = 0; i < 4; i++){
          if(nip.selectedValues[i] == nip.confirmationValues[i]) {
            cont++;
            if(cont == 4) nip.showtick = true;
            else nip.showtick = false;
          }
        }
      }



      nip.buttonPressed = function(button){
        if(button.valor == 'borrar'){
          nip.selectedValues = [];
          nip.confirmationValues = [];
          nip.disableSelect = false;
          nip.disableConfirmation = true;
          nip.disableBorrar = true;
          validateNip();
        }else if(nip.selectedValues.length < 4){
          nip.disableBorrar = false;
          nip.selectedValues.push(button.valor);
          if(nip.selectedValues.length == 4) {
            nip.disableSelect = true;
            nip.disableConfirmation = false;
          }
        }else if(nip.selectedValues.length == 4 && nip.confirmationValues.length < 4) {
          nip.confirmationValues.push(button.valor);
          if(nip.confirmationValues.length == 4) validateNip();
        }
      }

      var clearButton = function(){
        nip.button = {
          imagen: '',
          valor: '',
          clase: '',
          id: ''
        };
      }

      var fillButton = function(key, button){
        nip.button = {
          imagen: button.imagen,
          valor: button.valor,
          clase: '',
          id: 'nip_btn_n'+ key
        };
      }

      if($scope.nipIsVisible){
        connection.getImagesData()
          .then(function(response){
            console.log(response);
            angular.forEach(response.data.datos, function(button, key){
              clearButton();
              if(key === 9) nip.buttons.push(nip.button);
              fillButton(key, button);
              nip.buttons.push(nip.button);
            });
          }, function(response){
            console.log('Error: ', response);
          })
      }

    }]
  }
});
