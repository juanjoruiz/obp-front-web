var app = angular.module('isbanWebApp');

app.directive('identificacion', ['localStorageService', function( localStorageService){
  return {
    restrict: 'E',
    templateUrl: 'js/components/identificacion/identificacion.view.html',
    controllerAs: 'identificacion',
    controller: function($scope, $attrs, $location, $anchorScroll, connection, $filter, modalService, $q, imageProcessor, alertaService, $crypto, localStorageService){
      var identificacion = this;
      identificacion.loader = false;
      var parent = $scope.$eval($attrs.personalData);
      let copies = localStorageService.get('copies');
      /**/
      identificacion.isDisabled = true;
      identificacion.loadingImageFront=false;
      identificacion.loadingImageBack=false;
      identificacion.loadingImagePass=false;
      identificacion.ineSelected = false;
      identificacion.adultoAceptado = false;
      identificacion.pasaporteSelected = false;
      //identificacion.notEditable = localStorageService.get('clienteEditable');

      //$scope.$watch('identificacion.rolUniversitario')

      //console.info($scope.clientForm);

      connection.getActividades('actividades.json')
        .then(function(response){
          if(localStorageService.get('tipoDeMercado') == '1'){
            identificacion.titleActividad = copies.dataPersona.rol;
            identificacion.showIlicitas = false;
            identificacion.showOcupacion = false;
            identificacion.uniRoles = response.data.datos.universidades;
          }else if(localStorageService.get('tipoDeMercado') == '2' ){
            identificacion.actividadIlicita = copies.dataPersona.rol2;
            identificacion.titleActividad = copies.dataPersona.ocupacion;
            identificacion.showIlicitas = true;
            identificacion.uniRoles = response.data.datos.ocupacionesNomina;
            console.log("se llenaron roles");
            identificacion.ilicitas = response.data.datos.bloqueante;
          }else if (localStorageService.get('tipoDeMercado') == '3') {
            identificacion.actividadIlicita = copies.dataPersona.rol2;
            identificacion.titleActividad = copies.dataPersona.ocupacion;
            identificacion.showIlicitas = true;
            identificacion.uniRoles = response.data.datos.ocupaciones;
            console.log("se llenaron roles");
            identificacion.ilicitas = response.data.datos.bloqueante;
          }
        }, function(response){
          console.log(response);
        });
      $scope.$on("datos_editados", function(){
        if(localStorageService.get('saveandretrieveDatoseditables')){
          identificacion.notEditable = true;
          $('.combodate').children().attr('disabled', true)
        }else {
          identificacion.notEditable = false;
          $('.combodate').children().attr('disabled', false);
        }
        let entidad = document.querySelector("#SELECT_entidadNacimiento");
        let ilicita = document.querySelector("#SELECT_actividadilicita");

        ilicita.selectedIndex = ilicita.options.length - 1;
        identificacion.nombrePersona = localStorageService.get('nombre');
        identificacion.apellidoPaterno = localStorageService.get('apellidopaterno');
        identificacion.apellidoMaterno = localStorageService.get('apellidomaterno');
        identificacion.sexo = localStorageService.get('gender');
        identificacion.fechaNacimiento = localStorageService.get('fechanacimiento');
        $('#DATE_fechanacimiento').combodate('setValue', identificacion.fechaNacimiento);
        identificacion.nacionalidad = localStorageService.get('paisDeNacimiento');
        //identificacion.entidadNacimiento = localStorageService.get('entidadnacimiento');
        identificacion.rolUniversitario = localStorageService.get('roluniversitario');
        identificacion.calle = localStorageService.get('calle');
        identificacion.exterior = localStorageService.get('numeroexterior');
        identificacion.interior = localStorageService.get('numerointerior');
        identificacion.zipcode = localStorageService.get('zipcode');
        let paisNacimientoGuardado = localStorageService.get('paisDeNacimiento');
        let rolSelector = document.getElementById('SELECT_rolUniversitario');
        let actividadEspecificaGuardada = localStorageService.get('roluniversitario');
        angular.forEach(identificacion.uniRoles, function(rol){
          if(rol.actEspecifica == actividadEspecificaGuardada) {
            identificacion.rolUniversitario = rol;
            identificacion.saveRolUni();
          }
        });
        if(paisNacimientoGuardado == '052') {
          identificacion.nacionalidad = 'M';
          let value = "saveandretrieve";
          identificacion.saveNacionalidad(value);

          angular.forEach(identificacion.estados, function(estado){
            if(estado.id == localStorageService.get('entidadnacimiento').id){
              //entidad.selectedIndex = count
              identificacion.entidadNacimiento = estado;
              identificacion.saveEntidadNacimiento();
            }
          });


        } else {
          identificacion.nacionalidad = 'E';
          let value = "saveandretrieve";
          identificacion.saveNacionalidad(value);

          angular.forEach(identificacion.paises, function(pais){
            if(pais.id == localStorageService.get('paisDeNacimiento')){
              //entidad.selectedIndex = count
              identificacion.entidadNacimiento = pais;
              identificacion.saveEntidadNacimiento();
            }
          });
          // $('#SELECT_entidadNacimiento').find("option").each(function(i,e){
          // 	if($(e).val() == localStorageService.get('paisDeNacimiento')){
          // 		$('#SELECT_entidadNacimiento').prop('selectedIndex', i);
          // 	}
          // });
        }
        identificacion.saveZipCode("saveandretrieve");
      });

      $scope.$watch('identificacion.rolUniversitario', function(newValue, oldVale){
        console.log("Cambio ", newValue);
      });

      identificacion.tooltipBeneficiario = "La persona que designa para ejercer los derechos de la cuenta en caso de fallecimiento es quién heredará los fondos depositados en ella. Puede ser designado en éste momento o posteriormente en la sucursal.";
      identificacion.tooltipColoniaPoblacion = "Si la colonia desplegada al ingresar tu código postal no es la indicada, puedes seleccionar otra tocando el campo Colonia/Población.";
      identificacion.tooltipDomicilioBeneficiario = "La dirección domiciliaria de tu beneficiario deberá estar ubicada en México.";
      identificacion.format = 'yyyy/MM/dd';
      identificacion.soloCliente = true;
      identificacion.conBeneficiario = false;
      identificacion.disableMonth = true;
      identificacion.disableYear = true;



      function getYears(){
        var thisYear = new Date().getFullYear();
        var numThisYear = parseInt(thisYear.toString());
        return numThisYear;
      }

      var max = getYears() - 18;
      var min = getYears() - 100;

      $(function(){
          $('#DATE_fechanacimiento').combodate({
            customClass:'form-control',
            minYear: min,
            maxYear: max
          });
      });

      $(function(){
        $('#DATE_fechanacimientobeneficiario').combodate({
          customClass:'form-control beneficiario',
          minYear: min,
          maxYear: max
        });
      });


      var showErrorMessage = function(id, value){
        var modalOptions = {};
        if(id == -3){
          value = $crypto.decrypt(value);
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            closeButtonText: 'Ok',
            showOk: false,
            showClose: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }else if(id == -2){
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            showOk: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: "Por el momento el servicio no está disponible, por favor intenta nuevamente más tarde."
          };
        }else if(id == 404 || id == 500){
          value = $crypto.decrypt(value);
          modalOptions = {
            closeButtonText: 'Ok',
            showOk: false,
            showClose: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }else if(id == 1000){
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            closeButtonText: 'Cerrar',
            showOk: false,
            showClose: true,
            headerText: 'Error',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }else{
          value = $crypto.decrypt(value);
          modalOptions = {
            actionButtonText: 'Volver a intentar',
            showOk: true,
            headerText: 'Error ',
            modalIcon: 'images/icons/icono-error-validacion.png',
            bodyText: value
          };
        }
        return alertaService.showModal({}, modalOptions);
      };

      identificacion.resizeImage = function ( file ) {

          var resized = {};
          var deferred = $q.defer();
          var img = document.createElement("img");
          var reader = new FileReader();
          reader.onload = function(e) {
               // resize the picture
               img.src = e.target.result;
               img.onload = function(){
                 console.log(this.width);
                 var canvas = document.createElement("canvas");

                 var MAX_WIDTH = 512;
                 var MAX_HEIGHT = 658;
                 var width = this.width;
                 var height = this.height;
                 if (width > height) {
                   console.log('width ' +  width);
                   if (width > MAX_WIDTH) {
                     height *= MAX_WIDTH / width;
                     width = MAX_WIDTH;
                   }
                 } else {
                   if (height > MAX_HEIGHT) {
                     console.log('height ' +  height);
                     width *= MAX_HEIGHT / height;
                     height = MAX_HEIGHT;
                   }else{
                     console.log('width ' +  width + ' height '+ height);
                     width = width;
                     height = height;
                   }
                 }
                 canvas.width = width;
                 canvas.height = height;
                 var ctx = canvas.getContext("2d");
                 ctx.drawImage(img, 0, 0, width, height);
                 //console.log(ctx);

                 resized.base64 = canvas.toDataURL("image/png");
                 deferred.resolve(resized);
               }
          };
          reader.readAsDataURL(file, "UTF-8");

          return deferred.promise;

        }


      identificacion.popup1 = {
        opened: false
      };

      identificacion.popup2 = {
        opened: false
      };

      identificacion.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2050, 5, 22),
        minDate: new Date(1930, 1, 1),
        startingDay: 1
      };

      identificacion.altInputFormats = ['M!/d!/yyyy'];

      identificacion.open1 = function() {
        identificacion.popup1.opened = true;
      };

      identificacion.open2 = function() {
        identificacion.popup2.opened = true;
      };

      identificacion.showIne = function(item){
        localStorageService.set('typeId', 'ine');
        identificacion.documentoSeleccionado = 'ine';
        identificacion.ineSelected = true;
        identificacion.pasaporteSelected = false;
      }

      identificacion.ine = {};

      identificacion.ine.anversoImage = "../images/icons/cam.svg";

      identificacion.ine.reversoImage = "../images/icons/cam.svg";

      identificacion.showPass = function(){
        localStorageService.set('typeId', 'pasaporte');
        identificacion.documentoSeleccionado = 'pass';
        identificacion.ineSelected = false;
        identificacion.pasaporteSelected = true;
      }

      identificacion.pasaporte = {};

      identificacion.pasaporte.anversoImage = "../images/icons/cam.svg";

      identificacion.disableIdButton = function(){
        var disable = true;
        switch(identificacion.documentoSeleccionado){
          case 'ine':
            if(identificacion.ine.anverso && identificacion.ine.reverso) disable = false;
            break;
          case 'pass':
            if(identificacion.pasaporte.datos) disable = false;
            break;
          default:
            disable = true;
            break;
        }
        return disable;
      }

      identificacion.cambioTexto = function(item, image){
        item = "Volver a seleccionar";
        switch(image){
          case 'anverso':
            identificacion.saveIneFrente();
            identificacion.ine.textoFrente = item;
            break;
          case 'reverso':
            identificacion.saveIneReverso();
            identificacion.ine.textoReverso = item;
            break;
          case 'pasaporte':
            identificacion.savePasaporte();
            identificacion.pasaporte.textoFrente = item;
            break;
          default:
            break;
        }
      }

      identificacion.onLoadStartAnverso = function (e, reader, file, fileList, fileOjects, fileObj) {
        console.log('llamado');
        identificacion.loadingImageFront=true;
      };

      identificacion.saveIneFrente = function(){
        //onsole.log(identificacion.ine.anverso);
        identificacion.ine.anverso.base64 = identificacion.ine.anverso.base64.substring(22);
        localStorageService.set('ineAnverso', identificacion.ine.anverso);
        console.log(localStorageService.get('ineAnverso'));
      }

      identificacion.saveIneReverso = function(){
        identificacion.ine.reverso.base64 = identificacion.ine.reverso.base64.substring(22);
        localStorageService.set('ineReverso', identificacion.ine.reverso);
        console.log(localStorageService.get('ineReverso'));
      }

      identificacion.savePasaporte = function(){
        identificacion.pasaporte.datos.base64 = identificacion.pasaporte.datos.base64.substring(22);
        localStorageService.set('pasaporte', identificacion.pasaporte.datos);
      }

      /*Datos personales*/

      identificacion.saveNombre = function(){
        identificacion.nombrePersona = identificacion.nombrePersona.toUpperCase();
        localStorageService.set('nombre', identificacion.nombrePersona.toUpperCase());
      }

      identificacion.saveApPaterno = function(){
        identificacion.apellidoPaterno = identificacion.apellidoPaterno.toUpperCase();
        localStorageService.set('apellidopaterno', identificacion.apellidoPaterno.toUpperCase());
      }


      identificacion.saveApMaterno = function(){
        identificacion.apellidoMaterno = identificacion.apellidoMaterno.toUpperCase();
        let materno = identificacion.apellidoMaterno ? identificacion.apellidoMaterno : "";
        if(identificacion.apellidoMaterno){
          localStorageService.set('apellidomaterno', materno.toUpperCase());
        }else{
          localStorageService.set('apellidomaterno', '');
        }
      }


      identificacion.saveGender = function(){ localStorageService.set('gender', identificacion.sexo.toUpperCase()); }
      identificacion.saveFechaNacimiento = function(){
        //identificacion.fechaNacimiento = identificacion.anioNacimientoSeleccionado + '-' + identificacion.mesNacimientoSeleccionado + '-' + identificacion.diaNacimientoSeleccionado;

        localStorageService.set('fechanacimiento', identificacion.fechaNacimiento);
      }


      identificacion.saveNacionalidad = function(value){
        if(value != "saveandretrieve"){
          localStorageService.set('paisDeNacimiento', identificacion.nacionalidad);
        }
        if(identificacion.nacionalidad == 'M'){
          identificacion.chooseEntidad = identificacion.estados;
          let entidad = document.querySelector("#SELECT_entidadNacimiento");
          entidad.options = identificacion.estados;
        }else{
          identificacion.chooseEntidad = identificacion.paises;
          let entidad = document.querySelector("#SELECT_entidadNacimiento");
          entidad.options = identificacion.paises;
        }
      }

      identificacion.disableEntidad = true;
      identificacion.saveNacionalidadNew = function(value){
        identificacion.nacionalidad = value.descripcion;
        if(value.id == '052'){
          identificacion.disableEntidad = false;
          identificacion.chooseEntidad = identificacion.estados;
        }else{
          identificacion.disableEntidad = true;
        }
      }


      identificacion.saveEntidadNacimiento = function(){
        localStorageService.set('entidadnacimiento', identificacion.entidadNacimiento);
      }


      identificacion.saveRolUni = function(){
        //console.log(identificacion.rolUniversitario);
        localStorageService.set('roluniversitario', identificacion.rolUniversitario);
      }

      identificacion.esAdulto = function(nacimiento){
        var esAdulto = false;
        var time = Date.now();
        var today = new Date(time);
        var age = today.getFullYear() - nacimiento.getFullYear();
        if(age >= 18){
          esAdulto = true;
        }
        return esAdulto;
      }

      /* Domicilio */


      identificacion.saveCalle = function(){
        identificacion.calle = identificacion.calle.toUpperCase();
        localStorageService.set('calle', identificacion.calle );
      }

      identificacion.saveExterior = function(){ localStorageService.set('numeroexterior', identificacion.exterior); }

      identificacion.saveInterior = function(){ localStorageService.set('numerointerior', identificacion.interior); }

      identificacion.getAddress = function(zipcode, asentamientos){
        let asentamientoRetrieved = localStorageService.get('retrieveasentamiento');
        angular.forEach(asentamientos, function(asentamiento){
          if(asentamiento.nombre == asentamientoRetrieved){
            console.log(asentamiento);
            identificacion.selectedColonia = asentamiento;
            localStorageService.set('delegacion', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
            localStorageService.set('ciudad', response.data.datos.lstEstado[0].ciudades[0].ciudad);
            localStorageService.set('estado', response.data.datos.lstEstado[0].estado);
            localStorageService.set('colonia', identificacion.cpResponse[identificacion.cpResponse.length -1] );
          }
        });
      }


      identificacion.saveZipCode = function(value){
        if(identificacion.zipcode.length === 5){
          identificacion.loader = true;
          localStorageService.set('zipcode', identificacion.zipcode);
          connection.sendCp(localStorageService.get('zipcode'))
            .then(function(response){
              if(response.data.estatus !== 0){
                showErrorMessage(response.data.estatus, response.data.descripcion);
                identificacion.cpResponse = [];
              }else{
                identificacion.cpResponse = response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;
                if(value != "saveandretrieve"){
                  identificacion.selectedColonia = identificacion.cpResponse[identificacion.cpResponse.length - 1];
                  localStorageService.set('delegacion', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
                  localStorageService.set('ciudad', response.data.datos.lstEstado[0].ciudades[0].ciudad);
                  localStorageService.set('estado', response.data.datos.lstEstado[0].estado);
                  localStorageService.set('colonia', identificacion.cpResponse[identificacion.cpResponse.length -1] );
                }else{
                  let asentamientoRetrieved = localStorageService.get('retrieveasentamiento');
                  angular.forEach(identificacion.cpResponse, function(asentamiento){
                    if(asentamiento.nombre == asentamientoRetrieved){
                      console.log(asentamiento);
                      identificacion.selectedColonia = asentamiento;
                      localStorageService.set('delegacion', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
                      localStorageService.set('ciudad', response.data.datos.lstEstado[0].ciudades[0].ciudad);
                      localStorageService.set('estado', response.data.datos.lstEstado[0].estado);
                      localStorageService.set('colonia', identificacion.cpResponse[identificacion.cpResponse.length -1] );
                    }
                  });
                }

              }
              identificacion.loader = false;
            }, function(response){
              var cuatrocientoscuatro = "Hubo un Error con el servidor.";
              var quinientos = "El servidor tardó en responder";
              var desconocido = "Ocurrió un problema en el servidor.";
              if(response.status == 404){
                showErrorMessage(response.status, cuatrocientoscuatro);
              }else if(response.status == 500){
                showErrorMessage(response.status, quinientos);
              }else{
                showErrorMessage(response.status, desconocido);
              }
              identificacion.cpResponse = [];
              identificacion.loader = false;
            });
        }
      }


      identificacion.savePoblacion = function(){
        localStorageService.set('colonia', identificacion.selectedColonia);
      }


      /* en caso de que exista beneficiaario */
      identificacion.showBeneficiario = function(){
        localStorageService.set('addBeneficiario', identificacion.addBeneficiario);
        if(identificacion.addBeneficiario == 's'){
          identificacion.conBeneficiario = true;
          identificacion.soloCliente = false;
          $scope.beneficiarioIsVisible = true;
          $location.hash('DATA_beneficiarioForm');
          $anchorScroll();
        }else{
          identificacion.soloCliente = true;
          identificacion.conBeneficiario = false;
          identificacion.beneficiarioIsVisible = false;
          setBeneficiarioToEmpty();
          $scope.beneficiarioIsVisible = false;
        }
      }

      var setBeneficiarioToEmpty = function(){
        localStorageService.set('nombrebeneficiario', '');
        localStorageService.set('apellidopaternobeneficiario', '');
        localStorageService.set('apellidomaternobeneficiario', '');
        localStorageService.set('fechanacimientobeneficiario', '');
        localStorageService.set('entidadnacimientobeneficiario', '');
        localStorageService.set('callebeneficiario', '');
        localStorageService.set('exteriorbeneficiario', '');
        localStorageService.set('interiorbeneficiario', '');
        localStorageService.set('zipcodebeneficiario', '');
        localStorageService.set('delegacionbeneficiario', '');
        localStorageService.set('ciudadbeneficiario', '');
        localStorageService.set('estadobeneficiario', '');
        localStorageService.set('coloniabeneficiario', '');
      }



      identificacion.saveNombreBeneficiario = function(){
        identificacion.nombreBeneficiario = identificacion.nombreBeneficiario.toUpperCase();
        localStorageService.set('nombrebeneficiario', identificacion.nombreBeneficiario);
      }
      identificacion.saveApPaternoBeneficiario = function(){
        identificacion.apellidoPaternoBeneficiario = identificacion.apellidoPaternoBeneficiario.toUpperCase();
        localStorageService.set('apellidopaternobeneficiario', identificacion.apellidoPaternoBeneficiario);
      }
      identificacion.saveApMaternoBeneficiario = function(){
        if(identificacion.apellidoMaternoBeneficiario){
          identificacion.apellidoMaternoBeneficiario = identificacion.apellidoMaternoBeneficiario.toUpperCase();
          localStorageService.set('apellidomaternobeneficiario', identificacion.apellidoMaternoBeneficiario);
        }else{
          localStorageService.set('apellidomaternobeneficiario', identificacion.apellidoMaternoBeneficiario);
        }

      }
      identificacion.saveFechaNacimientoBeneficiario = function(){
        localStorageService.set('fechanacimientobeneficiario', $filter('date')(identificacion.fechaNacimientoBeneficiario, 'yyyy-MM-dd'));
      }
      identificacion.saveNacionalidadBeneficiario = function(){

        localStorageService.set('paisDeNacimientoBeneficiario', identificacion.nacionalidadBeneficiario);

        if(identificacion.nacionalidadBeneficiario == 'M'){
          identificacion.chooseEntidadBeneficiario = identificacion.estados;
        }else{
          identificacion.chooseEntidadBeneficiario = identificacion.paises;
        }
      }
      identificacion.saveEntidadNacimientoBeneficiario = function(){
        localStorageService.set('entidadnacimientobeneficiario', identificacion.entidadNacimientoBeneficiario);

      }

      identificacion.addDomicilioBeneficiario = function(){
        if(identificacion.mismoDomicilioBeneficiario == 'n'){
          identificacion.domiciliodiferente = true;
          localStorageService.set('beneficiarioMismoDomicilio', false);
        }else{
          identificacion.domiciliodiferente = false;
          localStorageService.set('beneficiarioMismoDomicilio', true);
          setDomicilioSameAsCliente();
        }
      }

      var setDomicilioSameAsCliente = function(){

      }

      identificacion.showOcupacion = true;
      identificacion.checkilicita = function(){
        console.log(identificacion.ilicita);
        if(identificacion.ilicita.descripcion != "Ninguna de las anteriores"){
          showErrorMessage(1000, 'No podemos continuar con el proceso de contratación, seguimos a sus órdenes')
          .then(function(){

          }, function(){
            $location.path("/");
          });
        }else{
          identificacion.showOcupacion = false;
        }
      }

      identificacion.saveCalleBeneficiario = function(){
        identificacion.callebeneficiario = identificacion.callebeneficiario.toUpperCase();
        localStorageService.set('callebeneficiario', identificacion.callebeneficiario );
      }
      identificacion.saveExteriorBeneficiario = function(){
        localStorageService.set('exteriorbeneficiario', identificacion.exteriorbeneficiario); }
      identificacion.saveInteriorBeneficiario = function(){
        localStorageService.set('interiorbeneficiario', identificacion.interiorbeneficiario); }
      identificacion.saveZipCodeBeneficiario = function(){
        if(identificacion.zipcodebeneficiario.length == 5){
          identificacion.loader = true;
          localStorageService.set('zipcodebeneficiario', identificacion.zipcodebeneficiario);
          connection.sendCp(localStorageService.get('zipcodebeneficiario'))
            .then(function(response){
              if(response.data.estatus !== 0){
                showErrorMessage(response.data.estatus, response.data.descripcion);
                identificacion.cpResponse2 = [];
              }else{
                identificacion.cpResponse2 = response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].asentamientos;
                identificacion.selectedColoniaBeneficiario = identificacion.cpResponse2[identificacion.cpResponse2.length - 1];
                localStorageService.set('delegacionbeneficiario', response.data.datos.lstEstado[0].ciudades[0].delegaciones[0].delegacion);
                localStorageService.set('ciudadbeneficiario', response.data.datos.lstEstado[0].ciudades[0].ciudad);
                localStorageService.set('estadobeneficiario', response.data.datos.lstEstado[0].estado);
                localStorageService.set('coloniabeneficiario', identificacion.cpResponse2[identificacion.cpResponse2.length -1] );
              }
              identificacion.loader = false;
            }, function(response){
              var cuatrocientoscuatro = "Hubo un Error con el servidor.";
              var quinientos = "El servidor tardó en responder";
              var desconocido = "Ocurrió un problema en el servidor.";
              if(response.status == 404){
                showErrorMessage(response.status, cuatrocientoscuatro);
              }else if(response.status == 500){
                showErrorMessage(response.status, quinientos);
              }else{
                showErrorMessage(response.status, desconocido);
              }
              identificacion.cpResponse2 = [];
              identificacion.loader = false;
            });
        }
      }
      identificacion.saveColoniaBeneficiario = function(){
        localStorageService.set('coloniabeneficiario', identificacion.selectedColoniaBeneficiario);
      }


      identificacion.diasNacimiento = [];
      identificacion.mesesNacimiento = [];
      identificacion.aniosNacimiento = [];
      var getDiasNacimiento = function(){
        for(var i = 1; i <= 31; i++){
          identificacion.diasNacimiento.push(i);
        }
      };

      identificacion.getMonths = function(){
        identificacion.disableMonth = false;
        identificacion.mesesNacimiento = [
          { value:'01', name:'Enero', days:'31' },
          { value:'02', name:'Febrero', days:'29' },
          { value:'03', name:'Marzo', days:'31' },
          { value:'04', name:'Abril', days:'30' },
          { value:'05', name:'Mayo', days:'31' },
          { value:'06', name:'Junio', days:'30' },
          { value:'07', name:'Julio', days:'31' },
          { value:'08', name:'Agosto', days:'31' },
          { value:'09', name:'Septiembre', days:'30' },
          { value:'10', name:'Octubre', days:'31' },
          { value:'11', name:'Noviembre', days:'30' },
          { value:'12', name:'Diciembre', days:'31' }
        ];
        console.log(identificacion.diaNacimientoSeleccionado);
        switch(identificacion.diaNacimientoSeleccionado){
          case '31':
            identificacion.mesesNacimiento = $filter('filter')(identificacion.mesesNacimiento, {days: '31'});
            break;
          default:
            break;
        }
      }





      identificacion.getYears = function(){
        var thisYear = new Date().getFullYear();
        var numThisYear = parseInt(thisYear.toString());
        for(i = numThisYear - 18; i >= numThisYear - 60; i--){
          identificacion.aniosNacimiento.push(i)
        }
      }



      getDiasNacimiento();



        connection.getPaises('Nacimiento_.json')
          .then(function(response){
            identificacion.estados = response.data.datos.estados;
            identificacion.paises = response.data.datos.paises;
          }, function(response){
            console.log(response.status);
          });

      // identificacion.estados = [
      //   {
      //     id: "AS",
      //     descripcion: "AGUASCALIENTES"
      //   },
      //   {
      //     id: "BC",
      //     descripcion: "BAJA CALIFORNIA"
      //   },
      //   {
      //     id: "BS",
      //     descripcion: "BAJA CALIFORNIA SUR"
      //   },
      //   {
      //     id: "CC",
      //     descripcion: "CAMPECHE"
      //   },
      //   {
      //     id: "CH",
      //     descripcion: "CHIHUAHUA"
      //   },
      //   {
      //     id: "CL",
      //     descripcion: "COAHUILA"
      //   },
      //   {
      //     id: "CM",
      //     descripcion: "COLIMA"
      //   },
      //   {
      //     id: "CS",
      //     descripcion: "CHIAPAS"
      //   },
      //   {
      //     id: "DF",
      //     descripcion: "DISTRITO FEDERAL"
      //   },
      //   {
      //     id: "DG",
      //     descripcion: "DURANGO"
      //   },
      //   {
      //     id: "GT",
      //     descripcion: "GUANAJUATO"
      //   },
      //   {
      //     id: "HG",
      //     descripcion: "HIDALGO"
      //   },
      //   {
      //     id: "JC",
      //     descripcion: "JALISCO"
      //   },
      //   {
      //     id: "MC",
      //     descripcion: "MEXICO"
      //   },
      //   {
      //     id: "MN",
      //     descripcion: "MICHOACAN"
      //   },
      //   {
      //     id: "MS",
      //     descripcion: "MORELOS"
      //   },
      //   {
      //     id: "CH",
      //     descripcion: "CHIHUAHUA"
      //   },
      //   {
      //     id: "NL",
      //     descripcion: "NUEVO LEON"
      //   },
      //   {
      //     id: "NT",
      //     descripcion: "NAYARIT"
      //   },
      //   {
      //     id: "OC",
      //     descripcion: "OAXACA"
      //   },
      //   {
      //     id: "PL",
      //     descripcion: "PUEBLA"
      //   },
      //   {
      //     id: "QR",
      //     descripcion: "QUINTANA ROO"
      //   },
      //   {
      //     id: "QT",
      //     descripcion: "QUERETARO"
      //   },
      //   {
      //     id: "SL",
      //     descripcion: "SINALOA"
      //   },
      //   {
      //     id: "SP",
      //     descripcion: "SAN LUIS POTOSI"
      //   },
      //   {
      //     id: "SR",
      //     descripcion: "SONORA"
      //   },
      //   {
      //     id: "TC",
      //     descripcion: "TABASCO"
      //   },
      //   {
      //     id: "TL",
      //     descripcion: "TLAXCALA"
      //   },
      //   {
      //     id: "TS",
      //     descripcion: "TAMAULIPAS"
      //   },
      //   {
      //     id: "VZ",
      //     descripcion: "VERACRUZ"
      //   },
      //   {
      //     id: "YN",
      //     descripcion: "YUCATAN"
      //   },
      //   {
      //     id: "ZS",
      //     descripcion: "ZACATECAS"
      //   }
      // ];
      //
      // identificacion.paises = [
      //     {
      //       id: "001",
      //       descripcion: "ESTADOS UNIDOS"
      //     },
      //     {
      //       id: "007",
      //       descripcion: "RUSIA"
      //     },
      //     {
      //       id: "020",
      //       descripcion: "EGIPTO"
      //     },
      //     {
      //       id: "022",
      //       descripcion: "TIMOR-LESTE"
      //     },
      //     {
      //       id: "023",
      //       descripcion: "AFGHANISTAN"
      //     },
      //     {
      //       id: "024",
      //       descripcion: "GIBRALTAR"
      //     },
      //     {
      //       id: "025",
      //       descripcion: "SWAZILAND"
      //     },
      //     {
      //       id: "026",
      //       descripcion: "TAJIKISTAN"
      //     },
      //     {
      //       id: "027",
      //       descripcion: "SUDAFRICA"
      //     },
      //     {
      //       id: "030",
      //       descripcion: "GRECIA"
      //     },
      //     {
      //       id: "031",
      //       descripcion: "HOLANDA -PAISES BAJOS-"
      //     },
      //     {
      //       id: "032",
      //       descripcion: "BELGICA"
      //     },
      //     {
      //       id: "033",
      //       descripcion: "FRANCIA"
      //     },
      //     {
      //       id: "034",
      //       descripcion: "ESPANA"
      //     },
      //     {
      //       id: "036",
      //       descripcion: "HUNGRIA"
      //     },
      //     {
      //       id: "039",
      //       descripcion: "ITALIA"
      //     },
      //     {
      //       id: "040",
      //       descripcion: "RUMANIA"
      //     },
      //     {
      //       id: "041",
      //       descripcion: "SUIZA"
      //     },
      //     {
      //       id: "042",
      //       descripcion: "INGLATERRA"
      //     },
      //     {
      //       id: "043",
      //       descripcion: "AUSTRIA"
      //     },
      //     {
      //       id: "044",
      //       descripcion: "REINO UNIDO"
      //     },
      //     {
      //       id: "045",
      //       descripcion: "DINAMARCA"
      //     },
      //     {
      //       id: "046",
      //       descripcion: "SUECIA"
      //     },
      //     {
      //       id: "047",
      //       descripcion: "NORUEGA"
      //     },
      //     {
      //       id: "048",
      //       descripcion: "POLONIA"
      //     },
      //     {
      //       id: "049",
      //       descripcion: "ALEMANIA"
      //     },
      //     {
      //       id: "050",
      //       descripcion: "NICARAGUA"
      //     },
      //     {
      //       id: "051",
      //       descripcion: "PERU"
      //     },
      //     {
      //       id: "052",
      //       descripcion: "MEXICO"
      //     },
      //     {
      //       id: "053",
      //       descripcion: "CUBA"
      //     },
      //     {
      //       id: "054",
      //       descripcion: "ARGENTINA"
      //     },
      //     {
      //       id: "055",
      //       descripcion: "BRASIL"
      //     },
      //     {
      //       id: "056",
      //       descripcion: "CHILE"
      //     },
      //     {
      //       id: "057",
      //       descripcion: "COLOMBIA"
      //     },
      //     {
      //       id: "058",
      //       descripcion: "REPUBLICA DE VENEZUELA BOLIVARIANA"
      //     },
      //     {
      //       id: "059",
      //       descripcion: "ANTILLAS HOLANDESAS"
      //     },
      //     {
      //       id: "060",
      //       descripcion: "MALASIA"
      //     },
      //     {
      //       id: "061",
      //       descripcion: "AUSTRALIA"
      //     },
      //     {
      //       id: "062",
      //       descripcion: "INDONESIA"
      //     },
      //     {
      //       id: "063",
      //       descripcion: "FILIPINAS"
      //     },
      //     {
      //       id: "064",
      //       descripcion: "NUEVA ZELANDA"
      //     },
      //     {
      //       id: "065",
      //       descripcion: "SINGAPUR"
      //     },
      //     {
      //       id: "066",
      //       descripcion: "TAILANDIA"
      //     },
      //     {
      //       id: "081",
      //       descripcion: "JAPON"
      //     },
      //     {
      //       id: "082",
      //       descripcion: "REPUBLICA DE COREA DEL SUR"
      //     },
      //     {
      //       id: "084",
      //       descripcion: "VIETNAM"
      //     },
      //     {
      //       id: "086",
      //       descripcion: "CHINA"
      //     },
      //     {
      //       id: "089",
      //       descripcion: "BIRMANIA"
      //     },
      //     {
      //       id: "090",
      //       descripcion: "TURQUIA"
      //     },
      //     {
      //       id: "091",
      //       descripcion: "INDIA"
      //     },
      //     {
      //       id: "092",
      //       descripcion: "PAKISTAN"
      //     },
      //     {
      //       id: "094",
      //       descripcion: "SRI LANKA"
      //     },
      //     {
      //       id: "095",
      //       descripcion: "MYANMAR"
      //     },
      //     {
      //       id: "098",
      //       descripcion: "IRAN"
      //     },
      //     {
      //       id: "101",
      //       descripcion: "CANADA"
      //     },
      //     {
      //       id: "107",
      //       descripcion: "KAZAJSTAN"
      //     },
      //     {
      //       id: "122",
      //       descripcion: "BAHAMAS"
      //     },
      //     {
      //       id: "124",
      //       descripcion: "BARBADOS"
      //     },
      //     {
      //       id: "126",
      //       descripcion: "ANGUILLA"
      //     },
      //     {
      //       id: "128",
      //       descripcion: "ANTIGUA Y BARBUDA"
      //     },
      //     {
      //       id: "130",
      //       descripcion: "ISLAS VIRGENES AMERICANAS"
      //     },
      //     {
      //       id: "134",
      //       descripcion: "ISLAS CAIMAN"
      //     },
      //     {
      //       id: "139",
      //       descripcion: "VATICANO"
      //     },
      //     {
      //       id: "141",
      //       descripcion: "LIECHTENSTEIN"
      //     },
      //     {
      //       id: "144",
      //       descripcion: "ESCOCIA"
      //     },
      //     {
      //       id: "162",
      //       descripcion: "ISLA DE NAVIDAD"
      //     },
      //     {
      //       id: "164",
      //       descripcion: "TURKS Y CAICOS"
      //     },
      //     {
      //       id: "166",
      //       descripcion: "ISLAS COCOS O KELLING"
      //     },
      //     {
      //       id: "167",
      //       descripcion: "MONSERRAT"
      //     },
      //     {
      //       id: "169",
      //       descripcion: "COMORES"
      //     },
      //     {
      //       id: "175",
      //       descripcion: "DOMINICA"
      //     },
      //     {
      //       id: "177",
      //       descripcion: "PUERTO RICO"
      //     },
      //     {
      //       id: "178",
      //       descripcion: "SANTA LUCIA"
      //     },
      //     {
      //       id: "180",
      //       descripcion: "GRANADA"
      //     },
      //     {
      //       id: "184",
      //       descripcion: "ISLAS VIRGENES BRITANICAS"
      //     },
      //     {
      //       id: "186",
      //       descripcion: "SAN CRISTOBAL Y NEVIS"
      //     },
      //     {
      //       id: "187",
      //       descripcion: "JAMAICA"
      //     },
      //     {
      //       id: "188",
      //       descripcion: "TRINIDAD Y TOBAGO"
      //     },
      //     {
      //       id: "189",
      //       descripcion: "REPUBLICA DOMINICANA"
      //     },
      //     {
      //       id: "207",
      //       descripcion: "UZBEKISTAN"
      //     },
      //     {
      //       id: "212",
      //       descripcion: "MARRUECOS"
      //     },
      //     {
      //       id: "213",
      //       descripcion: "ARGELIA"
      //     },
      //     {
      //       id: "216",
      //       descripcion: "TUNEZ"
      //     },
      //     {
      //       id: "218",
      //       descripcion: "LIBIA"
      //     },
      //     {
      //       id: "220",
      //       descripcion: "GAMBIA"
      //     },
      //     {
      //       id: "221",
      //       descripcion: "SENEGAL"
      //     },
      //     {
      //       id: "222",
      //       descripcion: "MAURITANIA"
      //     },
      //     {
      //       id: "223",
      //       descripcion: "MALI"
      //     },
      //     {
      //       id: "224",
      //       descripcion: "GUINEA"
      //     },
      //     {
      //       id: "225",
      //       descripcion: "COSTA DE MARFIL"
      //     },
      //     {
      //       id: "226",
      //       descripcion: "BURKINA FASO"
      //     },
      //     {
      //       id: "227",
      //       descripcion: "NIGER"
      //     },
      //     {
      //       id: "228",
      //       descripcion: "TOGO"
      //     },
      //     {
      //       id: "229",
      //       descripcion: "BENIN"
      //     },
      //     {
      //       id: "230",
      //       descripcion: "MAURICIO"
      //     },
      //     {
      //       id: "231",
      //       descripcion: "LIBERIA"
      //     },
      //     {
      //       id: "232",
      //       descripcion: "SIERRA LEONA"
      //     },
      //     {
      //       id: "233",
      //       descripcion: "GHANA"
      //     },
      //     {
      //       id: "234",
      //       descripcion: "NIGERIA"
      //     },
      //     {
      //       id: "235",
      //       descripcion: "CHAD"
      //     },
      //     {
      //       id: "236",
      //       descripcion: "REPUBLICA CENTROAFRICANA"
      //     },
      //     {
      //       id: "237",
      //       descripcion: "CAMERUN"
      //     },
      //     {
      //       id: "238",
      //       descripcion: "CABO VERDE"
      //     },
      //     {
      //       id: "239",
      //       descripcion: "SANTO TOME Y PRINCIPE"
      //     },
      //     {
      //       id: "240",
      //       descripcion: "GUINEA ECUATORIAL"
      //     },
      //     {
      //       id: "241",
      //       descripcion: "GABON"
      //     },
      //     {
      //       id: "242",
      //       descripcion: "REPUBLICA DEL CONGO"
      //     },
      //     {
      //       id: "243",
      //       descripcion: "REPUBLICA DEMOCRATICA DEL CONGO -ZAIRE-"
      //     },
      //     {
      //       id: "244",
      //       descripcion: "ANGOLA"
      //     },
      //     {
      //       id: "246",
      //       descripcion: "DIEGO GARCIA"
      //     },
      //     {
      //       id: "247",
      //       descripcion: "ASCENCION"
      //     },
      //     {
      //       id: "248",
      //       descripcion: "ISLAS SEYCHELLES"
      //     },
      //     {
      //       id: "249",
      //       descripcion: "SUDAN"
      //     },
      //     {
      //       id: "250",
      //       descripcion: "RUANDA"
      //     },
      //     {
      //       id: "251",
      //       descripcion: "ETIOPIA"
      //     },
      //     {
      //       id: "252",
      //       descripcion: "SOMALIA"
      //     },
      //     {
      //       id: "253",
      //       descripcion: "DJIBOUTI"
      //     },
      //     {
      //       id: "254",
      //       descripcion: "KENIA"
      //     },
      //     {
      //       id: "255",
      //       descripcion: "TANZANIA"
      //     },
      //     {
      //       id: "256",
      //       descripcion: "UGANDA"
      //     },
      //     {
      //       id: "257",
      //       descripcion: "BURUNDI"
      //     },
      //     {
      //       id: "258",
      //       descripcion: "MOZAMBIQUE"
      //     },
      //     {
      //       id: "260",
      //       descripcion: "ZAMBIA"
      //     },
      //     {
      //       id: "261",
      //       descripcion: "MADAGASCAR"
      //     },
      //     {
      //       id: "262",
      //       descripcion: "REUNION"
      //     },
      //     {
      //       id: "263",
      //       descripcion: "ZIMBABWE"
      //     },
      //     {
      //       id: "264",
      //       descripcion: "NAMIBIA"
      //     },
      //     {
      //       id: "265",
      //       descripcion: "MALAWI"
      //     },
      //     {
      //       id: "266",
      //       descripcion: "LESOTHO"
      //     },
      //     {
      //       id: "267",
      //       descripcion: "BOTSWANA"
      //     },
      //     {
      //       id: "268",
      //       descripcion: "SWAZILANDIA"
      //     },
      //     {
      //       id: "269",
      //       descripcion: "MAYOTTE"
      //     },
      //     {
      //       id: "290",
      //       descripcion: "SANTA ELENA"
      //     },
      //     {
      //       id: "291",
      //       descripcion: "ERITEA"
      //     },
      //     {
      //       id: "297",
      //       descripcion: "ARUBA"
      //     },
      //     {
      //       id: "298",
      //       descripcion: "ISLAS FEROE"
      //     },
      //     {
      //       id: "299",
      //       descripcion: "GROENLANDIA"
      //     },
      //     {
      //       id: "351",
      //       descripcion: "PORTUGAL"
      //     },
      //     {
      //       id: "352",
      //       descripcion: "LUXEMBURGO"
      //     },
      //     {
      //       id: "353",
      //       descripcion: "IRLANDA"
      //     },
      //     {
      //       id: "354",
      //       descripcion: "ISLANDIA"
      //     },
      //     {
      //       id: "355",
      //       descripcion: "ALBANIA"
      //     },
      //     {
      //       id: "356",
      //       descripcion: "MALTA"
      //     },
      //     {
      //       id: "357",
      //       descripcion: "CHIPRE"
      //     },
      //     {
      //       id: "358",
      //       descripcion: "FINLANDIA"
      //     },
      //     {
      //       id: "359",
      //       descripcion: "BULGARIA"
      //     },
      //     {
      //       id: "370",
      //       descripcion: "LITUANIA"
      //     },
      //     {
      //       id: "371",
      //       descripcion: "LETONIA"
      //     },
      //     {
      //       id: "372",
      //       descripcion: "ESTONIA"
      //     },
      //     {
      //       id: "373",
      //       descripcion: "MOLDAVIA"
      //     },
      //     {
      //       id: "374",
      //       descripcion: "ARMENIA"
      //     },
      //     {
      //       id: "375",
      //       descripcion: "BIELORRUSIA"
      //     },
      //     {
      //       id: "376",
      //       descripcion: "ANDORRA"
      //     },
      //     {
      //       id: "377",
      //       descripcion: "MONACO"
      //     },
      //     {
      //       id: "378",
      //       descripcion: "SAN MARINO"
      //     },
      //     {
      //       id: "380",
      //       descripcion: "UCRANIA"
      //     },
      //     {
      //       id: "381",
      //       descripcion: "SERBIA (YUGOSLAVIA)"
      //     },
      //     {
      //       id: "382",
      //       descripcion: "MONTENEGRO"
      //     },
      //     {
      //       id: "385",
      //       descripcion: "CROACIA"
      //     },
      //     {
      //       id: "386",
      //       descripcion: "ESLOVENIA"
      //     },
      //     {
      //       id: "387",
      //       descripcion: "BOSNIA"
      //     },
      //     {
      //       id: "389",
      //       descripcion: "MACEDONIA"
      //     },
      //     {
      //       id: "420",
      //       descripcion: "REPUBLICA CHECA"
      //     },
      //     {
      //       id: "421",
      //       descripcion: "REPUBLICA ESLOVACA"
      //     },
      //     {
      //       id: "441",
      //       descripcion: "BERMUDAS"
      //     },
      //     {
      //       id: "500",
      //       descripcion: "ISLAS MALVINAS"
      //     },
      //     {
      //       id: "501",
      //       descripcion: "BELICE"
      //     },
      //     {
      //       id: "502",
      //       descripcion: "GUATEMALA"
      //     },
      //     {
      //       id: "503",
      //       descripcion: "EL SALVADOR"
      //     },
      //     {
      //       id: "504",
      //       descripcion: "HONDURAS"
      //     },
      //     {
      //       id: "506",
      //       descripcion: "COSTA RICA"
      //     },
      //     {
      //       id: "507",
      //       descripcion: "PANAMA"
      //     },
      //     {
      //       id: "508",
      //       descripcion: "SAN PEDRO Y MIQUELON"
      //     },
      //     {
      //       id: "509",
      //       descripcion: "HAITI"
      //     },
      //     {
      //       id: "574",
      //       descripcion: "ISLA NORFOLK"
      //     },
      //     {
      //       id: "590",
      //       descripcion: "GUADALUPE"
      //     },
      //     {
      //       id: "591",
      //       descripcion: "ESTADO DE BOLIVIA PLURINACIONAL"
      //     },
      //     {
      //       id: "592",
      //       descripcion: "GUYANA"
      //     },
      //     {
      //       id: "593",
      //       descripcion: "ECUADOR"
      //     },
      //     {
      //       id: "594",
      //       descripcion: "GUAYANA FRANCESA"
      //     },
      //     {
      //       id: "595",
      //       descripcion: "PARAGUAY"
      //     },
      //     {
      //       id: "596",
      //       descripcion: "MARTINICA"
      //     },
      //     {
      //       id: "597",
      //       descripcion: "SURINAM"
      //     },
      //     {
      //       id: "598",
      //       descripcion: "URUGUAY"
      //     },
      //     {
      //       id: "599",
      //       descripcion: "CURAZAO"
      //     },
      //     {
      //       id: "612",
      //       descripcion: "PITCAIRN"
      //     },
      //     {
      //       id: "670",
      //       descripcion: "ISLAS MARIANAS"
      //     },
      //     {
      //       id: "671",
      //       descripcion: "GUAM"
      //     },
      //     {
      //       id: "672",
      //       descripcion: "TERRITORIOS EXT. AUSTRALIA"
      //     },
      //     {
      //       id: "673",
      //       descripcion: "BRUNEI"
      //     },
      //     {
      //       id: "674",
      //       descripcion: "NAURU"
      //     },
      //     {
      //       id: "675",
      //       descripcion: "PAPUA NUEVA GUINEA"
      //     },
      //     {
      //       id: "676",
      //       descripcion: "TONGA"
      //     },
      //     {
      //       id: "677",
      //       descripcion: "SALOMON"
      //     },
      //     {
      //       id: "678",
      //       descripcion: "VANUATO"
      //     },
      //     {
      //       id: "679",
      //       descripcion: "ISLAS FIJI"
      //     },
      //     {
      //       id: "680",
      //       descripcion: "PALAU"
      //     },
      //     {
      //       id: "681",
      //       descripcion: "WALLIS Y FUTUNA"
      //     },
      //     {
      //       id: "682",
      //       descripcion: "ISLAS COOK"
      //     },
      //     {
      //       id: "683",
      //       descripcion: "NIUE"
      //     },
      //     {
      //       id: "684",
      //       descripcion: "SAMOA AMERICANA"
      //     },
      //     {
      //       id: "685",
      //       descripcion: "SAMOA OCCIDENTAL"
      //     },
      //     {
      //       id: "686",
      //       descripcion: "KIRIBATI"
      //     },
      //     {
      //       id: "687",
      //       descripcion: "NUEVA CALEDONIA"
      //     },
      //     {
      //       id: "688",
      //       descripcion: "TUVALO"
      //     },
      //     {
      //       id: "689",
      //       descripcion: "POLINESIA FRANCESA -TAHITI-"
      //     },
      //     {
      //       id: "691",
      //       descripcion: "MICRONESIA"
      //     },
      //     {
      //       id: "692",
      //       descripcion: "ISLAS MARSHALL"
      //     },
      //     {
      //       id: "744",
      //       descripcion: "SVALBARD Y JAN MAYEN"
      //     },
      //     {
      //       id: "772",
      //       descripcion: "TOKELAU"
      //     },
      //     {
      //       id: "809",
      //       descripcion: "SAN VICENTE Y GRANADI"
      //     },
      //     {
      //       id: "850",
      //       descripcion: "REPUBLICA POPULAR DE COREA"
      //     },
      //     {
      //       id: "852",
      //       descripcion: "HONG KONG"
      //     },
      //     {
      //       id: "853",
      //       descripcion: "MACAO"
      //     },
      //     {
      //       id: "855",
      //       descripcion: "CAMBOYA"
      //     },
      //     {
      //       id: "856",
      //       descripcion: "LAOS"
      //     },
      //     {
      //       id: "880",
      //       descripcion: "BANGLADESH"
      //     },
      //     {
      //       id: "886",
      //       descripcion: "TAIWAN"
      //     },
      //     {
      //       id: "960",
      //       descripcion: "MALDIVAS"
      //     },
      //     {
      //       id: "961",
      //       descripcion: "LIBANO"
      //     },
      //     {
      //       id: "962",
      //       descripcion: "JORDANIA"
      //     },
      //     {
      //       id: "963",
      //       descripcion: "SIRIA"
      //     },
      //     {
      //       id: "964",
      //       descripcion: "IRAK"
      //     },
      //     {
      //       id: "965",
      //       descripcion: "KUWAIT"
      //     },
      //     {
      //       id: "966",
      //       descripcion: "ARABIA SAUDITA"
      //     },
      //     {
      //       id: "967",
      //       descripcion: "YEMEN"
      //     },
      //     {
      //       id: "968",
      //       descripcion: "OMAN"
      //     },
      //     {
      //       id: "971",
      //       descripcion: "EMIRATOS ARABES UNIDOS"
      //     },
      //     {
      //       id: "972",
      //       descripcion: "ISRAEL"
      //     },
      //     {
      //       id: "973",
      //       descripcion: "BAHRAIN"
      //     },
      //     {
      //       id: "974",
      //       descripcion: "QATAR"
      //     },
      //     {
      //       id: "975",
      //       descripcion: "BHUTAN"
      //     },
      //     {
      //       id: "976",
      //       descripcion: "MONGOLIA"
      //     },
      //     {
      //       id: "977",
      //       descripcion: "NEPAL"
      //     },
      //     {
      //       id: "992",
      //       descripcion: "ISLAS DEL CANAL"
      //     },
      //     {
      //       id: "993",
      //       descripcion: "TURKMENISTAN"
      //     },
      //     {
      //       id: "994",
      //       descripcion: "AZERBAIYAN"
      //     },
      //     {
      //       id: "995",
      //       descripcion: "GEORGIA"
      //     },
      //     {
      //       id: "996",
      //       descripcion: "KIRGUIZISTAN"
      //     },
      //     {
      //       id: "997",
      //       descripcion: "SAINT MARTIN"
      //     },
      //     {
      //       id: "998",
      //       descripcion: "NO APLICA"
      //     },
      //     {
      //       id: "999",
      //       descripcion: "GENERICO"
      //     }
      //   ];
    }
  }
}]);
