(function(){
  angular.module('isbanWebApp')
    .controller('smsController', smsController);

    function smsController($rootScope, $scope, localStorageService, config, connection, $log, $state, baseConfig){
      let vm = this;
      let smsLog = $log.getInstance('SMS Controller');
      let copies = localStorageService.get('copies');

      let env = baseConfig.ENV.toUpperCase();
      if($rootScope.ENV_VAR == 'pro'){
        switch(localStorageService.get('tipoDeMercado')){
          case '1':
            utag.view({page:'/OBP_CodigoConfirmacion_UNI_'+env});
            break;
          case '2':
            utag.view({page:'/OBP_CodigoConfirmacion_NOM_'+env});
            break;
          case '3':
            utag.view({page:'/OBP_CodigoConfirmacion_MA_'+env});
            break;
          case '4':
              utag.view({page:'/OBP_CodigoConfirmacion_MA_'+env});
              break;
        }
      }

      vm.codigoTitle = copies.ingresarCodigo.title;
      vm.mensajeEnvio = copies.ingresarCodigo.subtitle;
      vm.formVerificationCodeLabel = copies.ingresarCodigo.labelConf;
      vm.smsHint = "Si después de un minuto no ha recibido el SMS, seleccione una opción:";//copies.ingresarCodigo
      vm.formResendLink = copies.ingresarCodigo.reEnviar;
      vm.formSendOtherToken = "Enviar a otro teléfono." //copies.ingresarCodigo.

      $scope.$on('smsVisible', function(){
        vm.numero = localStorageService.get('numeroTelefono') ? localStorageService.get('numeroTelefono') : "";
        vm.pretoken = localStorageService.get('presmsToken') ? localStorageService.get('presmsToken') : "";
        vm.esSr = localStorageService.get('initsr');
      });

      $scope.$on('smschanged', function(){
        vm.numero = localStorageService.get('numeroTelefono') ? localStorageService.get('numeroTelefono') : "";
        vm.pretoken = localStorageService.get('presmsToken') ? localStorageService.get('presmsToken') : "";
      });

      vm.changePhone = () => {
        $rootScope.$broadcast('cambiarTelefono');
      }

      vm.validarInput = () => {
        localStorageService.set('smsToken', vm.token);
        //let flux = localStorageService.get('flux');
        if(localStorageService.get('initsr')){
          connection.sendSmsToken()
            .then(function(response){
              if(response.estatus === 0){
                config.nextStep(1);
              }else{

                let error = {
                  status: response.estatus,
                  descripcion: response.descripcion,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo
                }
                config.generalErrors(error);
              }
            })
        }else{
          connection.sendSmsToken()
            .then(function(response){
              console.log(response);
              if(response.estatus === 0){
                config.nextStep(1);
              }else{
                let error = {
                  status: response.estatus,
                  descripcion: response.descripcion,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo
                }
                config.generalErrors(error);
              }
            });
        }
      }

      vm.resendToken = () => {
        vm.token = "";
        connection.envioMismoTelefono()
          .then(function(response){
            if(response.estatus === 0){
              let message = {
                id: 100
              }
              config.generalMessages(message);
              localStorageService.set('presmsToken', response.datos);
              $rootScope.$broadcast('smschanged');
              vm.pretoken = "";
              smsLog.info('Reenvio correcto');
            }else{
              smsLog.error('Error ', response);
              let error = {
                status: response.estatus,
                descripcion: response.descripcion,
                errorCode: response.codigo,
                tipo: response.tipo,
                titulo: response.titulo
              }
              config.generalErrors(error);
            }
          })
          .catch(function(error){
            config.generalErrors(error);
          });
      }

    }
})();


//
// ['$scope','$state', 'localStorageService', 'connection', 'config', function($scope, $state, localStorageService, connection, config) {
//   var smsCtrl = this;
//   smsCtrl.mensajeEnvio = "Por favor, ingrese el código de verificación que hemos enviado al número ";
//   smsCtrl.formVerificationCodeLabel = "Código de confirmación";
//   smsCtrl.smsHint = "Si después de un minuto no ha recibido el SMS, seleccione una opción:";
//   smsCtrl.formResendLink = "Re-enviar al mismo teléfono";
//   smsCtrl.formSendOtherToken = "Enviar a otro teléfono";
//
//   smsCtrl.validarInput = () => {
//     localStorageService.set('smstoken', smsCtrl.token);
//     connection.validaToken()
//       .then(function(response){
//         if(response.estatus == 1){
//           $state.go('stepthree');
//         }else if (response.estatus == 3) {
//           let error = {
//             status: 8,
//             descripcion: response.descripcion
//           }
//           config.generalErrors(error);
//         }else{
//            let error = {
//              status: 7,
//              descripcion: response.descripcion
//            }
//            config.generalErrors(error);
//         }
//       }, function(response){
//         console.log("Error ", response);
//         config.generalErrors(response);
//       });
//   };
//
//   smsCtrl.resendToken = () => {
//     connection.reenvioToken()
//       .then(function(response){
//         if(response.estatus == 1){
//           let message = {
//             status: 2
//           }
//           config.generalMessages(message);
//         }else if (response.estatus == 3) {
//           let error = {
//             status: 8,
//             descripcion: response.descripcion
//           }
//           config.generalErrors(error);
//         }else{
//           let error = {
//             status: 7,
//             descripcion: response.descripcion
//           }
//           config.generalErrors(error);
//         }
//       }, function(response){
//
//       });
//   };
//
//   smsCtrl.changePhone = () => $state.go('steptwo', { form: "changePhone" });
//
// }]
