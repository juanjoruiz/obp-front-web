(function(){
  angular.module('isbanWebApp')
    .directive('sms', sms);

    function sms(){
      return {
        restrict: 'EA',
        templateUrl: 'js/components/sms/sms.view.html',
        controllerAs: 'smsCtrl',
        controller: 'smsController'
      }
    }
})();
