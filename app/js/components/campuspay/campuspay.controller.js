(function(){
  angular.module('isbanWebApp')
    .controller('campuspayController', campuspayController);

  function campuspayController($scope, $stateParams, localStorageService, connection, $rootScope, $state, $base64, saveandretrieve){
    let data = $stateParams.data;
    //console.log($base64.encode('{"datosBasicos":{"nombrePersona":"JOSE DE JESUS","apellidoPaterno":"GALVAN","apellidoMaterno":"CABELLO","fechaNacimiento":"1999-05-22","sexo":"H","nacionalidad":"052","paisNacimiento":"052","entFedNac":"DF","mail":""},"telefono":{"telefonoLada":"552","telefono":"0727033","ciaTelefono":"TELCEL"}}'));
    let decodedjson = $base64.decode(data);
    if(decodedjson !== undefined){
      let datosFull = JSON.parse(decodedjson);
      let datos = datosFull.datosBasicos;
      let datosTel = datosFull.telefono;

      localStorageService.set('initsrcp', true);
      localStorageService.set('initsr', true);
      localStorageService.set('srdata', datos);

      let lada = datosTel.telefonoLada ? datosTel.telefonoLada : "";
      let telefono = datosTel.telefono ? datosTel.telefono : "";

      if(lada !== "" && telefono !== "")
        localStorageService.set("numeroTelefono", lada + "" + telefono);

      localStorageService.set("nombre", datos.nombrePersona ? datos.nombrePersona : "");
  		localStorageService.set("apellidomaterno", datos.apellidoMaterno ? datos.apellidoMaterno : "");
  		localStorageService.set("apellidopaterno",datos.apellidoPaterno ? datos.apellidoPaterno : "");
  		localStorageService.set("fechanacimiento", datos.fechaNacimiento ? datos.fechaNacimiento : "");
  		localStorageService.set("gender", datos.sexo ? datos.sexo : "");
  		localStorageService.set("paisDeNacimiento", datos.paisNacimiento ? datos.paisNacimiento : "");
  		localStorageService.set("fechanacimiento", datos.fechaNacimiento ? datos.fechaNacimiento : "");
  		localStorageService.set("roluniversitario", { actEspecifica: datos.actEspecifica ? datos.actEspecifica : "" , actGenerica: datos.actGenerica ? datos.actGenerica : "" });
  		localStorageService.set("entidadnacimiento", datos.entFedNac ? datos.entFedNac : "");
  		localStorageService.set("email", datos.mail ? datos.mail : "");
      localStorageService.set('clienteEditable', true);
      localStorageService.set('beneficiarioEditable',true)


      $state.go('beneficios', {tipo: 1});
    }


  }
  campuspayController.$inject = ['$scope', '$stateParams', 'localStorageService', 'connection', '$rootScope', '$state', '$base64', 'saveandretrieve'];
})();
