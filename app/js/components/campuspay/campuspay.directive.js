(function(){
  angular.module('isbanWebApp')
    .directive('campuspay', campuspay);

  function campuspay(){
    return{
      restrict: 'EA',
      templateUrl: 'js/components/campuspay/campuspay.view.html',
      controllerAs: 'campuspayCtrl',
      controller: 'campuspayController'
    }
  }
})();
