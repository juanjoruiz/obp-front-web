(function(){

  angular.module('isbanWebApp')
    .controller('contactoController', contactoController);

    function contactoController($rootScope, $scope, localStorageService, connection, baseConfig, config, $log, saveandretrieve){
      let vm = this;
      let copies = localStorageService.get('copies');
      let contactoLog = $log.getInstance('Contacto controller');

      let env = baseConfig.ENV.toUpperCase();
      if($rootScope.ENV_VAR == 'pro'){
        switch(localStorageService.get('tipoDeMercado')){
          case '1':
            utag.view({page:'/OBP_DatosContacto_UNI_'+env});
            break;
          case '2':
            utag.view({page:'/OBP_DatosContacto_NOM_'+env});
            break;
          case '3':
            utag.view({page:'/OBP_DatosContacto_MA_'+env});
            break;
          case '4':
              utag.view({page:'/OBP_DatosContacto_MA_'+env});
              break;
        }
      }

      vm.emailRegex = /^[a-zA-Z0-9_\-\.~]{4,}@[a-zA-Z0-9_\-\.~]{2,}\.[a-zA-Z]{2,4}$/;
      vm.formTelefonoTitle = copies.ingresarTelefono.title;
      vm.formTelefonoSubtitle = copies.ingresarTelefono.subtitle;
      vm.formTelefonoLabel = copies.ingresarTelefono.labelTelefono;
      vm.formCompanyLabel = copies.ingresarTelefono.labelCompania;
      vm.formEmailLabel = copies.ingresarTelefono.labelEmail;
      vm.formConfirmacionLabel = copies.ingresarTelefono.labelEmailConf;
      vm.mensajePrivacidad = copies.ingresarTelefono.footer;
      vm.confirmarEmail = copies.ingresarTelefono.confirmarEmail;

      vm.smsVisible = false;

      vm.companies = [
        { id: "IUSACELL", company: "AT&T" },
        { id: "NEXTEL", company: "NEXTEL" },
        { id: "TELCEL", company: "TELCEL" },
        { id: "TELEFONICA", company: "TELEFONICA" },
        { id: "UNEFON", company: "UNEFON" }
      ];

      if(localStorageService.get('numeroTelefono'))
        vm.telefono = localStorageService.get('numeroTelefono');

      if(localStorageService.get('email')){
        vm.email = localStorageService.get('email');
        vm.confEmail = localStorageService.get('email');
      }

      vm.saveTelefono = () => localStorageService.set('numeroTelefono', vm.telefono);
      vm.saveCompany = () => localStorageService.set('companiaTelefono', vm.company);
      vm.saveEmail = () => localStorageService.set('email', vm.email);

      function enviarOtroTelefono() {
        connection.envioOtroTelefono()
          .then(function(response){
            if(response.estatus === 0){
              localStorageService.set('presmsToken', response.datos);
              vm.smsVisible = true;
              $rootScope.$broadcast('smsVisible');
            }else{
              let error = {
                status: response.estatus,
                descripcion: response.descripcion,
                errorCode: response.codigo,
                tipo: response.tipo,
                titulo: response.titulo
              }
              config.generalErrors(error);
            }
          })
          .catch(function(error){
            //config.generalErrors(error);
          });
      }

      function contactoCliente() {
        connection.contactoCliente()
          .then(function(response){
            if(response.estatus === 0){
              if(localStorageService.get('initsr') === false || localStorageService.get('initsr') === undefined || localStorageService.get('initsr') === null){
                localStorageService.set('idSolicitud', response.datos.idSolicitud);
                localStorageService.set('idNoCliente', response.datos.idNoCliente);
              }else if(localStorageService.get('nuevaContratacion')){
                localStorageService.set('idSolicitud', response.datos.idSolicitud);
                localStorageService.set('idNoCliente', response.datos.idNoCliente);
              }
              localStorageService.set('presmsToken', response.datos.token);
              vm.smsVisible = true;
              $rootScope.$broadcast('smsVisible');
            }else if (response.estatus == -4) {
              localStorageService.set('initsr', true);
              localStorageService.set('srdata', response.datos);
              let error = {};
              if(!response.datos.questionToShow){
                error = {
                  status: response.estatus,
                  descripcion: null,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo,
                  showOk: true,
                  showBoth: false,
                  showClose: false,
                  actionButtonText: "Ok",
                  closeButtonText: ""
                }
              }else{
                error = {
                  status: response.estatus,
                  descripcion: response.descripcion,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo,
                  showOk: false,
                  showBoth: true,
                  showClose: false,
                  actionButtonText: "Continuar",
                  closeButtonText: "Nuevo"
                }
              }

              config.generalErrors(error)
                .then(function(){
                  contactoLog.info('Se recuperará la solicitud en el paso: ', saveandretrieve.getScreenFromStep(response.datos.paso));
                  saveandretrieve.setData(response.datos);
                  vm.validarInput();
                }, function(){
                  contactoLog.info('Iniciará una nueva contratación');
                  localStorageService.set('nuevaContratacion', true);
                  vm.validarInput();
                });
            }else{
              let error = {
                status: response.estatus,
                descripcion: response.descripcion,
                errorCode: response.codigo,
                tipo: response.tipo,
                titulo: response.titulo
              }
              config.generalErrors(error);
            }
          })
          .catch(function(error){
            //config.generalErrors(error);
          });
      }

      vm.validarInput = () => {
        if($scope.changePhone){
          enviarOtroTelefono();
        }else{
          contactoCliente();
        }
      }

      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          contactoLog.info('Universidades');
          vm.Universidades = true;
          vm.Nomina = false;
          vm.MercadoAbierto = false;
          break;
        case '2':
          contactoLog.info('Nomina');
          vm.Universidades = false;
          vm.Nomina = true;
          vm.MercadoAbierto = false;
          break;
        case '3':
          contactoLog.info('Mercado abierto');
          vm.Universidades = false;
          vm.Nomina = false;
          vm.MercadoAbierto = true;
          break;
        case '4':
          contactoLog.info('Mercado abierto');
          vm.Universidades = false;
          vm.Nomina = false;
          vm.MercadoAbierto = true;
          break;
      }

      $scope.$on('cambiarTelefono', function(){
        vm.smsVisible = false;
        vm.telefono = "";
        vm.company = "";
        $scope.changePhone = true;
      });


    }

    contactoController.$inject = ['$rootScope', '$scope', 'localStorageService', 'connection', 'baseConfig', 'config', '$log', 'saveandretrieve'];

})();
