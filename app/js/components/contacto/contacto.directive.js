(function(){
  angular.module('isbanWebApp')
    .directive('contacto', contacto);

    function contacto(){
      return {
        restrict: 'EA',
        templateUrl: 'js/components/contacto/contacto.view.html',
        controllerAs: 'contactoCtrl',
        controller: 'contactoController'
      }
    }
})();
