(function(){
  var app = angular.module('isbanWebApp');

  app.directive('carrusel', carrusel);

  function carrusel(){
    return{
      restrict:'A',
      templateUrl: 'js/components/carrusel/carrusel.view.html',
      controllerAs: 'carruselCtrl',
      controller: 'CarruselController'
    }
  }

})();
