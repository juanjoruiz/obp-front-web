(function(){
  var app = angular.module('isbanWebApp');

  app.controller('CarruselController', CarruselController);

  function CarruselController($scope, $stateParams, $state){
    var vm = this;
    vm.slides = document.getElementsByClassName("mySlides");
    vm.dots = document.getElementsByClassName("badge");
    vm.buttonTxt = "SIGUIENTE";

    vm.showDivs = function(n) {
      var i;
      if (n > vm.slides.length) {vm.slideIndex = 1}
      if (n < 1) {vm.slideIndex = vm.slides.length}
      for (i = 0; i < vm.slides.length; i++) {
         vm.slides[i].style.display = "none";
      }
      for (i = 0; i < vm.dots.length; i++) {
         vm.dots[i].className = vm.dots[i].className.replace("badge-white", "");
      }
      vm.slides[vm.slideIndex-1].style.display = "block";
      vm.dots[vm.slideIndex-1].className += " badge-white";
    }

    vm.slideIndex = 1;
    vm.showDivs(vm.slideIndex);

    vm.plusDivs = function(n) {
      let args = {
        pantalla: "carrusel",
        opcion: "slide"
      }
      if(vm.slideIndex + n > vm.slides.length) {
        $scope.$emit('CarruselCall', args);
      }
      else if (vm.slideIndex + n === 0)return;
      else vm.showDivs(vm.slideIndex += n);
      if (vm.slideIndex == vm.slides.length) {
        vm.buttonTxt = "ENTENDIDO";
      }
    }

    vm.currentDiv = function(n) {
      vm.showDivs(vm.slideIndex = n);
    }

    vm.backButton = function(){
      let args = {
        pantalla: "carrusel",
        opcion: "volver"
      }
      $scope.$emit('BackToHome', args);
    }

    vm.clearDivs = function(){
      let args = {
        pantalla: "carrusel",
        opcion: "saltar"
      }
      $scope.$emit('CarruselCall', args);
    }



  }

  CarruselController.$inject = ["$scope", "$stateParams", "$state"];
})();
