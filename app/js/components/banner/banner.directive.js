(function(){
  angular.module('isbanWebApp')
    .directive('banner', banner);

  function banner(){
    return{
      restrict: 'EA',
      templateUrl: 'js/components/banner/banner.view.html',
      controllerAs: 'bannerCtrl',
      controller: 'bannerController'
    }
  }
})();
