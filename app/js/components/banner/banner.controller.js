(function(){
  angular.module('isbanWebApp')
    .controller('bannerController', bannerController);

  function bannerController($scope, $stateParams){
    let vm = this;


    let meses = new Array(
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    );



    vm.text = "Aprovecha los beneficios de cuenta SúperDigital con tu credencial universitaria TUI";
    vm.mobileTitle = "SúperDigital";
    let fecha = new Date();
    vm.mobileDate = fecha.getDate() + " de " + meses[fecha.getMonth()] + ", " + fecha.getFullYear();

    switch($stateParams.tipo){
      case '1':
        if($scope.settings.desktop || $scope.settings.tablet){
          vm.banner = 'images/data/banner_universidades.jpg';
          vm.uniText = true;
        }else if ($scope.settings.mobile) {
          vm.banner = 'images/data/banner_universidades_iphone.png'
          vm.uniText = false;
        }
        break;
      case '2':
        if($scope.settings.desktop || $scope.settings.tablet){
          vm.banner = 'images/data/banner-nomina.jpg';
        }else if ($scope.settings.mobile) {
          vm.banner = 'images/data/banner_nomina_iphone.png'
        }
        vm.uniText = false;
        break;
      case '3':
        if($scope.settings.desktop || $scope.settings.tablet){
          vm.banner = 'images/data/banner-ma.png';
        }else if ($scope.settings.mobile) {
          vm.banner = 'images/data/banner-ma-iphone.png'
        }
        vm.uniText = false;
        break;
      case '4':
        if($scope.settings.desktop || $scope.settings.tablet){
          vm.banner = 'images/data/banner-ma.png';
        }else if ($scope.settings.mobile) {
          vm.banner = 'images/data/banner-ma-iphone.png'
        }
        vm.uniText = false;
        break;

    }
  }
})();
