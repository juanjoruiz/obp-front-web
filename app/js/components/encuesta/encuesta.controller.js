(function(){
  angular.module('isbanWebApp')
    .controller('EncuestaController', EncuestaController);

  function EncuestaController($rootScope, localStorageService, connection, config, $window, baseConfig){
    var vm = this;

    let env = baseConfig.ENV.toUpperCase();
    if($rootScope.ENV_VAR == 'pro'){
      switch(localStorageService.get('tipoDeMercado')){
        case '1':
          utag.view({page:'/OBP_Encuesta_UNI_'+env});
          break;
        case '2':
          utag.view({page:'/OBP_Encuesta_NOM_'+env});
          break;
        case '3':
          utag.view({page:'/OBP_Encuesta_MA_'+env});
          break;
        case '4':
            utag.view({page:'/OBP_Encuesta_MA_'+env});
            break;
      }
    }

    vm.focalMessage = "A partir de este momento disfruta de realizar todas tus operaciones desde Supermovil y Supernet";

    vm.rate = 0;
    vm.max = 5;
    vm.isReadonly = false;

    vm.hoveringOver = function(value){
      vm.overStar = value;
      vm.percent = 100 * (value / vm.max);
    };

    vm.saveRate = function(){
      localStorageService.set('rate', vm.rating);
      console.log(vm.rating);
    }

    vm.saveComments = function(){
      localStorageService.set('comments', vm.comentario);
      console.log(vm.comentario);
    }

     vm.ratingStates = [
       {stateOn: 'fa fa-star', stateOff: 'icon-star-empty'}
     ];

    vm.enviarComentarios = () => {
      connection.sendEncuesta()
        .then(function(response){
          localStorageService.clearAll();
          $window.location.href = 'http://www.santander.com.mx/';
        });
    }

  }

  EncuestaController.$inject = ["$rootScope", "localStorageService", "connection", "config", "$window", "baseConfig"];
})();
