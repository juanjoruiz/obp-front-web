(function(){
  angular.module('isbanWebApp')
    .directive('encuesta', encuesta);

  function encuesta(){
    return{
      restrict: 'A',
      templateUrl: 'js/components/encuesta/encuesta.view.html',
      controllerAs: 'calificarCtrl',
      controller: 'EncuestaController'
    }
  }

})();
