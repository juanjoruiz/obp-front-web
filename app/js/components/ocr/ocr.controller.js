(function(){
  var app = angular.module('isbanWebApp');

  app.controller('OcrController', ocrController);

  function ocrController($scope, $http, localStorageService, connection, config, $q, aplicacion, $rootScope){
    //Test assets
    var videoInput = document.getElementById('videoInput');
    var extraccionEncendida = false;
    aplicacion.getConf().then(function(response){
      extraccionEncendida = response.data.datos.extraccion_encendida;
    });
    $scope.documento = {};
    $scope.documento.ine = {};
    $scope.documento.pasaporte = {};
    $scope.startDocumentCapture = false;
    //Detección de navegador para OCR
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    //Detección de tamaño
    function orientation(){
      $scope.mobile = window.innerWidth <= 414;
      $scope.portrait = window.innerHeight > window.innerWidth;
      $scope.landscape = window.innerWidth > window.innerHeight;
    }
    orientation();
    window.onresize = function(event){
      orientation();
      $scope.$apply();
    }
    $scope.showOcr = false;
    $scope.showInstructions = false;

    //Regresar a la pantalla principal con imagen.
    function siguienteImagen(){
      var vistaOcr = document.getElementById('ocr-view').style;
        vistaOcr.position = "";
      $scope.imageRetrivedSuccessfully = false;
      var canvasResult = document.getElementById("resultImage");
      switch($scope.imagenAObtener){
        case 'ineAnverso':
          $scope.documento.ine.anverso64 = canvasResult.toDataURL();
          break;
        case 'ineReverso':
          $scope.documento.ine.reverso64 = canvasResult.toDataURL();
          break;
        case 'hojaDatos':
          $scope.documento.pasaporte.datos64 = canvasResult.toDataURL();
          break;
      }
      //IcarSDK.documentCapture.stop();
    }

    function xmlToJson(xml) {

    	// Create the return object
    	var obj = {};

    	if (xml.nodeType == 1) { // element
    		// do attributes
    		if (xml.attributes.length > 0) {
    		obj["@attributes"] = {};
    			for (var j = 0; j < xml.attributes.length; j++) {
    				var attribute = xml.attributes.item(j);
    				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
    			}
    		}
    	} else if (xml.nodeType == 3) { // text
    		obj = xml.nodeValue;
    	}

    	// do children
    	if (xml.hasChildNodes()) {
    		for(var i = 0; i < xml.childNodes.length; i++) {
    			var item = xml.childNodes.item(i);
    			var nodeName = item.nodeName;
    			if (typeof(obj[nodeName]) == "undefined") {
    				obj[nodeName] = xmlToJson(item);
    			} else {
    				if (typeof(obj[nodeName].push) == "undefined") {
    					var old = obj[nodeName];
    					obj[nodeName] = [];
    					obj[nodeName].push(old);
    				}
    				obj[nodeName].push(xmlToJson(item));
    			}
    		}
    	}
    	return obj;
    }

    var sendDocumento = function(){
      switch($scope.tipoDocumento){
        case 'ine':
          var idFrente = {};//$scope.documento.ine;
          idFrente.base64 = $scope.documento.ine.anverso64.substring(22);
          var idAtras = {};//$scope.documento.ine;
          idAtras.base64 = $scope.documento.ine.reverso64.substring(22);
          $scope.validarIdentificacion(idFrente, idAtras);
          break;
        case 'pasaporte':
          var hoja = $scope.documento.pasaporte;
          hoja.base64 = hoja.datos64.substring(22);
          $scope.validarIdentificacion(hoja, null);
          break;
        default:
          break;
      }
    }

    //Validación de XML
    $scope.validarOCR = function(){
      if(extraccionEncendida){
        //jshint ignore:start
        console.log("ENTRE");
        var type = 'ine';
        var anverso64, reverso64;
        var xmlOCR = `<?xml version="1.0" encoding="utf-8" ?>
          <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <soap:Body>
        <AnalyzeDocumentV2Ex xmlns="http://IdCloud.iCarVision/WS/">
        <Company>00682</Company>
        <User>santander</User>
        <Pwd>santander_prod</Pwd>
        <GetDocumentIdForMerging>true</GetDocumentIdForMerging>
        <DocInV2Ex>
        <Reference>2017052901</Reference>`
        if($scope.tipoDocumento == 'ine'){

          xmlOCR += `<Image1>
          <ImageResolution>200</ImageResolution><Image>` + $scope.documento.ine.anverso64.substring(22) + `</Image>
          <Filetype>jpg</Filetype>
          <DeviceInfo>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTE2IiBzdGFuZGFsb25lPSJubyIgPz48YWNxdWlyZWRJbWFnZSB4bWxucz0iaHR0cDovL2ljYXJ2aXNpb24uY29tL2ljYXJfYWNxdWlzaXRpb25fbGliLzFfMF8wIj48ZGV2aWNlPjMwPC9kZXZpY2U+PHJlc29sdXRpb25YPjIwMDwvcmVzb2x1dGlvblg+PHJlc29sdXRpb25ZPjIwMDwvcmVzb2x1dGlvblk+PC9hY3F1aXJlZEltYWdlPg==</DeviceInfo>
          </Image1>
          <Image2>
          <ImageResolution>200</ImageResolution>
          <Image>` + $scope.documento.ine.reverso64.substring(22) + `</Image>
          <Filetype>jpg</Filetype>
          <DeviceInfo>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTE2IiBzdGFuZGFsb25lPSJubyIgPz48YWNxdWlyZWRJbWFnZSB4bWxucz0iaHR0cDovL2ljYXJ2aXNpb24uY29tL2ljYXJfYWNxdWlzaXRpb25fbGliLzFfMF8wIj48ZGV2aWNlPjMwPC9kZXZpY2U+PHJlc29sdXRpb25YPjIwMDwvcmVzb2x1dGlvblg+PHJlc29sdXRpb25ZPjIwMDwvcmVzb2x1dGlvblk+PC9hY3F1aXJlZEltYWdlPg==</DeviceInfo>
          </Image2>`
        } else {

          xmlOCR += `<Image1>
          <ImageResolution>200</ImageResolution><Image>` + $scope.documento.pasaporte.datos64.substring(22) + `</Image>
          <Filetype>jpg</Filetype>
          <DeviceInfo>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTE2IiBzdGFuZGFsb25lPSJubyIgPz48YWNxdWlyZWRJbWFnZSB4bWxucz0iaHR0cDovL2ljYXJ2aXNpb24uY29tL2ljYXJfYWNxdWlzaXRpb25fbGliLzFfMF8wIj48ZGV2aWNlPjMwPC9kZXZpY2U+PHJlc29sdXRpb25YPjIwMDwvcmVzb2x1dGlvblg+PHJlc29sdXRpb25ZPjIwMDwvcmVzb2x1dGlvblk+PC9hY3F1aXJlZEltYWdlPg==</DeviceInfo>
          </Image1>`
        }
        xmlOCR += `<DocumentIdToMerge />
        </DocInV2Ex>
        </AnalyzeDocumentV2Ex>
        </soap:Body>
        </soap:Envelope>`
        //console.log(xmlOCR);

        //$scope.vista = xmlOCR;

        //$scope.xmlFile = xmlOCR;

        localStorageService.set('isOCR', true);

        let sendData = {
          url: "https://idcloud.icarvision.com:50071/iCarSAAS.WsPublic/WsDocument.asmx?op=AnalyzeDocumentV2Ex",
          //url: "https://demoidcloud.icarvision.com:50061/iCarSAAS.WsPublic/WsDocument.asmx?op=AnalyzeDocumentV2Ex",
          method: "POST",
          data: xmlOCR,
          headers: {
            'Content-Type': 'text/xml; charset=UTF-8'
          }
        }

        $http(sendData).then(function(response){
          let xmlDoc = null;
          if (window.DOMParser) {
              parser = new DOMParser();
              xmlDoc = parser.parseFromString(response.data, "text/xml");
              //$scope.vista = response.data;
          } else {
              xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
              xmlDoc.async = false;
              xmlDoc.loadXML(response.data);
          }
          let myDoc = {};
          var apellidos = [];
          var address = [];
          var calle = "";
          var zipcode = "";

          let preDoc = xmlToJson(xmlDoc);
          let fields = preDoc["soap:Envelope"]["soap:Body"]["AnalyzeDocumentV2ExResponse"]["AnalyzeDocumentV2ExResult"]["Fields"]["Field"];
          let messages = preDoc["soap:Envelope"]["soap:Body"]["AnalyzeDocumentV2ExResponse"]["AnalyzeDocumentV2ExResult"]["Messages"]["Message"];
          console.log(messages);
          if(messages === undefined && fields !== undefined){
            for(i = 0; i < fields.length; i++){
                // SE OBTIENEN LOS VALORES PARA ALMACENAR
                var nodeKey = fields[i]["Code"]["#text"];
                var nodeVal = fields[i]["Value"]["#text"];

                // SE GUARDAN LOS NODOS EN UN JSON OBJECT

                if(nodeKey == "SURNAME")
                  apellidos.push(nodeVal);
                else if (nodeKey == "ADDRESS")
                  address.push(nodeVal);
                else
                  myDoc[nodeKey.trim()] = nodeVal.trim();
            }
            if(address.length == 2){
                zipcode = address[0].substr(-5);
            } else if(address.length > 0) {
                zipcode = address[1].substr(-5);
            }

            myDoc["SURNAME"] = apellidos
            myDoc["ADDRESS"] = address;
            myDoc["ZIPCODE"] = zipcode;
            if(myDoc["SEX"] == "M")
                myDoc["SEX"] = "H"
            else if (myDoc["SEX"] == "F")
                myDoc["SEX"] = "M"

            if ((myDoc["TEST_SIDE_CORRESPONDENCE"] == "OK" && $scope.tipoDocumento == "ine") || $scope.tipoDocumento == "pasaporte") {
              // VALIDA QUE SE HAYAN SUBIDO LOS DOS LADOS
              if (myDoc["TEST_COLOR_IMAGE"] == "OK") {
                // VALIDA QUE NO SEA COPIA EN BLANCO Y NEGRO
                if (parseFloat(myDoc.TEST_GLOBAL_AUTHENTICITY_RATIO) >= 0.8) {
                  if($scope.tipoDocumento == 'ine') localStorageService.set('initsrocr', true);
                  else localStorageService.set('initsrocr', false);
                  //localStorageService.set('srdata', datos);
                  localStorageService.set("nombre", myDoc.NAME ? myDoc.NAME : "");
                  localStorageService.set("apellidopaterno", myDoc.SURNAME[0] ? myDoc.SURNAME[0] : "");
                  localStorageService.set("apellidomaterno", myDoc.SURNAME[1] ? myDoc.SURNAME[1] : "");
                  localStorageService.set("fechanacimiento", myDoc.BIRTHDATE ? myDoc.BIRTHDATE : "");
                  localStorageService.set("gender", myDoc.SEX ? myDoc.SEX : "");
                  localStorageService.set('calle', calle);
                  localStorageService.set('zipcode', zipcode);
                  localStorageService.set('clienteEditable', true);
                  localStorageService.set('beneficiarioEditable', true);
                  localStorageService.set('isOCR', false)
                  // ENVIAR A SERVICIO DE IMAGENES
                  localStorageService.set('typeId', $scope.tipoDocumento);
                  sendDocumento();
                } else {
                  let error = {
                    status: -100,
                    descripcion: "Los datos no pueden ser procesados, intenta de nuevo.",
                    errorCode: response.codigo,
                    tipo: "error",
                    titulo: "Error"
                  }
                  config.generalErrors(error);
                  //alert("RADIO < 0.8");
                  // alertaService.showModal({}, modalOptions)
                  //   .then(function() {
                  //     console.log("RADIO < 0.8");
                  //     clearScreen();
                  //   })
                }
              } else {
                let error = {
                    status: -100,
                    descripcion: "Los datos no pueden ser procesados, intenta de nuevo.",
                    errorCode: response.codigo,
                    tipo: "error",
                    titulo: "Error"
                  }
                  config.generalErrors(error);
                //alert("IMAGEN EN BLANCO Y NEGRO");
                // alertaService.showModal({}, modalOptions)
                //   .then(function() {
                //     console.log("IMAGEN EN BLANCO Y NEGRO");
                //     clearScreen();
                //   })
              }
            } else {
              let error = {
                    status: -100,
                    descripcion: "Los datos no pueden ser procesados, intenta de nuevo.",
                    errorCode: response.codigo,
                    tipo: "error",
                    titulo: "Error"
                  }
                  config.generalErrors(error);
              //alert("NO SE SUBIERON AMBOS LADOS");
              // alertaService.showModal({}, modalOptions)
              //   .then(function() {
              //     console.log("NO SE SUBIERON AMBOS LADOS");
              //     clearScreen();
              //   })
            }



          }

        }, function(response){
          let error = {
                status: -100,
                descripcion: "Los datos no pueden ser procesados, intenta de nuevo.",
                errorCode: response.codigo,
                tipo: "error",
                titulo: "Error"
              }
              config.generalErrors(error);
        });

        //jshint ignore:end
      }else{
        localStorageService.set('typeId', $scope.tipoDocumento);
        sendDocumento();
      }






    }

    function onNumCamsReceivedCallback(numberCams){
			if (numberCams>1){
				// show the button because there are more than 1 camera
				document.getElementById('ChangeCamera').style.visibility = 'visible';
			}
		}
    //
		function askForChangeCameraFunction(){
			IcarSDK.documentCapture.stop(changeCameraFunction)
		}

		function changeCameraFunction(){
			IcarSDK.video.changeCamera();
		}


		function msgFramesFunction(textInfo){
			document.getElementById("fps_message").innerHTML = textInfo;
		}


		// function to do the request of the frame to the video
		//'onFrameReceivedCallback' must to be the next parameters:
		// - imageData: frame of the video
		function requestFrameCallback(onFrameReceivedCallback){
			IcarSDK.video.requestFrame(onFrameReceivedCallback);

      $('canvas')[0].style.marginLeft = "calc(50% - " + $('canvas')[0].getBoundingClientRect().width/2 + "px)";
      $('canvas')[1].style.marginLeft = "calc(50% - " + $('canvas')[1].getBoundingClientRect().width/2 + "px)";

		}
    //
    //
		// // Return function that the process will use when it will finish
		// // params:
		// //  - answerResult: property when is stored the result of the process:
		// //				IcarSDK.ResultProcess.OK
		// //				IcarSDK.ResultProcess.FAIL
		// //              IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED
		// //  - modeCapture: mode of the capture:
		// //              IcarSDK.CaptureMode.MANUAL_TRIGGER
		// //              IcarSDK.CaptureMode.AUTO_TRIGGER
		// //  - imageResult: property where is stored the image captured
		// //  - roiPoints: roi of the document on the image (defined by the frame)
		// //  - hashCode: property that contains the hashcode generated from the image
		// //              result and the private key.
		function onDocumentResultCallback(answerResult, modeCapture, imageResult, roiPoints, hashCode){

			if (answerResult == IcarSDK.ResultProcess.OK){
        $scope.showOcr = false;
        //$scope.show
				var canvasResult = document.getElementById("resultImage")
				canvasResult.className = videoInput.className;
				canvasResult.width = imageResult.width;
				canvasResult.height = imageResult.height;
				var context = canvasResult.getContext('2d');
				context.clearRect(0, 0, canvasResult.width, canvasResult.height);
				context.putImageData(imageResult, 0, 0);
        //console.log(canvasResult.toDataURL());
        //documento.base64 = canvasResult.toDataURL();
        //$scope.image = documento.base64;

        var vistaOcr = document.getElementById('ocr-view').style;
        vistaOcr.position = "absolute";
        vistaOcr.top = 0;
        vistaOcr.left = 0;
        vistaOcr.right = 0;
        vistaOcr.bottom = 0;
        $('canvas')[0].style.marginLeft = 0;
        $scope.imageRetrivedSuccessfully = true;
        IcarSDK.video.cleanUp()

        $scope.$apply();

			}else if(answerResult == IcarSDK.ResultProcess.ATTEMPTS_EXCEEDED){
				//alert("Number attempts exceeded!!\nPlease, try again.")
        let error = {
              status: -100,
              descripcion: "Alcanzaste el número máximo de intentos permitidos. Por favor inténtalo nuevamente.",
              errorCode: response.codigo,
              tipo: "error",
              titulo: "Intentos"
            }
            config.generalErrors(error);
			}else{
				//alert("OHPSSS!!!, Process stopped")
        let error = {
              status: -100,
              descripcion: "Se detuvo el proceso.",
              errorCode: response.codigo,
              tipo: "error",
              titulo: "Error"
            }
            config.generalErrors(error);
			}

		}

    var showVideoScreen = (value) => {
      //var deferred = $q.defer();

        if(value == "FRENTE") {
          $scope.imagenAObtener = 'ineAnverso';
          //deferred.resolve(IcarSDK.documentCapture.start());
        }else if (value == "VUELTA") {
          $scope.imagenAObtener = 'ineReverso';
          //deferred.resolve(IcarSDK.documentCapture.start());
        }else if (value == "HOJA DE DATOS") {
          $scope.imagenAObtener = 'hojaDatos';
          //deferred.resolve(IcarSDK.documentCapture.start());
        }
        IcarSDK.documentCapture.start();



      //return deferred.promise;
    }



    var openOcrModal = function(value){
      $scope.showOcr = true;
      $scope.showInstructions = true;
      $scope.needsHelp = false;
      var vistaOcr = document.getElementById('ocr-view').style;
        vistaOcr.position = "";


      //navigator.getUserMedia({video:true}, startVideo(), reloadPage());

        $scope.imageRetrivedSuccessfully = false;
        var OPTIONS_VIDEO = {
  				modeCapture: IcarSDK.MODE_CAPTURE.DOCUMENT
  			}
  			IcarSDK.video.initialize(videoInput, OPTIONS_VIDEO);
  			//IcarSDK.video.getNumberOfCameras(onNumCamsReceivedCallback);

        var OPTIONS_DOCUMENTS = {
          //width_document: 105.6,
          //height_document: 53.98,
          type_document: IcarSDK.TYPE_DOC.IDCARD,
          marginPercent_frame: 0.1,
          messageFpsFunction:  msgFramesFunction,
          MSG_PLACE_DOC_INSIDE: "Coloca la credencial dentro.",
          MSG_DOC_OK: "Manten la credencial en este lugar.",
          colorBackground:"#000000",
          alphaBackground:0.36,
          MAX_ATTEMPTS: -1
        }

        if($scope.actualImage == "FRENTE") OPTIONS_DOCUMENTS.path_template_img = "images/icons/bgFrenteElector.png";
        else if ($scope.actualImage == "VUELTA") OPTIONS_DOCUMENTS.path_template_img = "images/icons/bgVueltaElector.png";
        else if ($scope.actualImage == "HOJA DE DATOS") OPTIONS_DOCUMENTS.path_template_img = "images/icons/bgPasaporte.png";

        IcarSDK.documentCapture.create(videoInput, requestFrameCallback, onDocumentResultCallback, OPTIONS_DOCUMENTS);

    }

    $scope.startCapture = function(){
      $scope.showInstructions = false;
      showVideoScreen($scope.actualImage);
    }

    $scope.volverACapturar = function(){
      IcarSDK.video.cleanUp();
      var vistaOcr = document.getElementById('ocr-view').style;
        vistaOcr.position = "";

      $scope.imageRetrivedSuccessfully = false;
      var image = openOcrModal($scope.actualImage);
    }



    $scope.closeHelp = function(){
      $scope.showInstructions = false;
    }

    $scope.takeScreenFront = function(){
      $scope.actualImage = "FRENTE";
      var image = openOcrModal("FRENTE");
    }

    $scope.takeScreenBack = function(){
      $scope.actualImage = "VUELTA";
      var image = openOcrModal("VUELTA");
    }

    $scope.takeScreenData = function(){
      $scope.actualImage = "HOJA DE DATOS";
      var image = openOcrModal("HOJA DE DATOS");
    }

    $scope.continuarDoc = function(){
      if($scope.tipoDocumento == 'ine'){
        if($scope.documento.ine.anverso64 === undefined || $scope.documento.ine.reverso64 === undefined){
          siguienteImagen();
        }
      }else if ($scope.tipoDocumento == 'pasaporte') {
        siguienteImagen();
      }
      var vistaOcr = document.getElementById('ocr-view').style;
        vistaOcr.position = "";

      $scope.imageRetrivedSuccessfully = false;

    }

    $scope.validarIdentificacion = function(idFrente, idAtras) {
      connection.sendIdentification(idFrente, idAtras)
        .then(function(response){
          if(response.estatus === 0){
            config.nextStep(1);
          }else{
            let error = {
              status: response.estatus,
              descripcion: response.descripcion,
              errorCode: response.codigo,
              tipo: response.tipo,
              titulo: response.titulo
            }
            config.generalErrors(error);
          }
        })

    }

  }

  ocrController.$inject = ["$scope", "$http", "localStorageService", "connection", "config", "$q", "aplicacion", "$rootScope"];
})();
