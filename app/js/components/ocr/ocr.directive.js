(function(){
  var app = angular.module('isbanWebApp');

  app.directive('ocr', ocrDirective);

  function ocrDirective(){
    return{
      restrict: 'EA',
      templateUrl: 'js/components/ocr/ocr.view.html',
      controllerAs: 'ocrCtrl',
      controller: 'OcrController'
    }
  }
})();
