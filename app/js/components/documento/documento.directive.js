var app = angular.module('isbanWebApp');

app.directive('documento', function(){
  return{
    restrict: 'A',
    templateUrl: 'js/components/documento/documento.view.html',
    controllerAs: 'documentoCtrl',
    controller: 'DocumentoController'
  }
});
