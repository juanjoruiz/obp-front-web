(function(){
  angular.module('isbanWebApp')
    .controller('DocumentoController', DocumentoController);

    function DocumentoController($rootScope, $scope, $attrs, $location, $anchorScroll, connection, $filter, modalService, $q, imageProcessor, alertaService, $crypto, localStorageService, $state, config, baseConfig,ocrCaptureService){

      let vm = this;
      let copies = localStorageService.get('copies');

      let env = baseConfig.ENV.toUpperCase();
      if($rootScope.ENV_VAR == 'pro'){
        switch(localStorageService.get('tipoDeMercado')){
          case '1':
            utag.view({page:'/OBP_RegistraId_UNI_'+env});
            break;
          case '2':
            utag.view({page:'/OBP_RegistraId_NOM_'+env});
            break;
          case '3':
            utag.view({page:'/OBP_RegistraId_MA_'+env});
            break;
          case '4':
            utag.view({page:'/OBP_RegistraId_MA_'+env});
              break;
        }
      }

      vm.ine = {};
      vm.pasaporte = {};
      vm.frontImageLoaded = false;

      vm.identificacionTitle = copies.seleccionarCredencial.title;
      vm.identificacionSubtitle = copies.seleccionarCredencial.mensaje;
      vm.identificacionIneLabel = copies.seleccionarCredencial.btnINE;
      vm.identificacionPasaporteLabel = copies.seleccionarCredencial.btnPasaporte;
      vm.identificacionIneTextoFrente = copies.subirINE.frontInput;
      vm.identificacionIneTextoReverso = copies.subirINE.backInput;
      vm.identificacionPasaporteTextoFrente = copies.subirPasaporte.Input;


      vm.showIne = (item) => {
        localStorageService.set('typeId', 'ine');
        vm.documentoSeleccionado = 'ine';
        vm.ineSelected = true;
        vm.pasaporteSelected = false;
      }

      vm.showPass = () => {
        localStorageService.set('typeId', 'pasaporte');
        vm.documentoSeleccionado = 'pass';
        vm.ineSelected = false;
        vm.pasaporteSelected = true;
      }

      vm.resizeImage = function ( file, base64_object ) {

          var resized = {};
          var deferred = $q.defer();
          var img = document.createElement("img");
          var reader = new FileReader();
          reader.onload = function(e) {
               // resize the picture
               img.src = e.target.result;
               img.onload = function(){
                 console.log(this.width);
                 var canvas = document.createElement("canvas");


                 var MAX_WIDTH = 1024;//1366;//1920;//1024;
                 var MAX_HEIGHT = 1024;//720;//1080; //1316;
                 var width = this.width;
                 var height = this.height;
                 if (width > height) {
                   console.log('width ' +  width);
                   if (width > MAX_WIDTH) {
                     height *= MAX_WIDTH / width;
                     width = MAX_WIDTH;
                   }
                 } else {
                   if (height > MAX_HEIGHT) {
                     console.log('height ' +  height);
                     width *= MAX_HEIGHT / height;
                     height = MAX_HEIGHT;
                   }else{
                     console.log('width ' +  width + ' height '+ height);
                     width = width;
                     height = height;
                   }
                 }
                  canvas.width = width;
                  canvas.height = height;
                  console.log(width);
                  console.log(height);
                  //let xFactor = width <= MAX_WIDTH ? 1 : MAX_WIDTH / width;
                  //let yFactor = height <= MAX_HEIGHT ? 1 : MAX_HEIGHT / height;
                  var ctx = canvas.getContext("2d");
                 ctx.drawImage(img, 0, 0, width, height);
                 //ctx.scale(xFactor, yFactor);
                 console.log(ctx);

                 resized.base64 = canvas.toDataURL("image/png", 0.1);
                 deferred.resolve(resized);
               }
          };
          reader.readAsDataURL(file, "UTF-8");
          return deferred.promise;
        }

        vm.onLoadStartAnverso = function (e, reader, file, fileList, fileOjects, fileObj) {
          vm.loadingImageFront=true;
        };

        // SIRVE PARA VALIDAR SI YA SE HA CARGADO LA IMAGEN DEL FRENTE
        $scope.getImageReady = function(){
          return vm.frontImageLoaded;
        }

        // GUARDA IMAGEN DELANTERA DE INE U HOJA DE DATOS DE PASAPORTE
        $scope.setFrontImage = function(value) {
          if(localStorageService.get('typeId') == 'ine'){
            vm.ine.anverso = {};
            vm.ine.anverso.base64 = value;
            localStorageService.set('ineAnverso', vm.ine.anverso);
            vm.frontImageLoaded = true;
          } else {
            vm.pasaporte.datos = {};
            vm.pasaporte.datos.base64 = value;
            localStorageService.set('pasaporte', vm.pasaporte.datos);
            //vm.savePasaporte();
          }
        }
        // GUARDA IMAGEN TRASERA DE INE
        $scope.setBackImage = function(value) {
          vm.ine.reverso = {};
          vm.ine.reverso.base64 = value;
          localStorageService.set('ineReverso', vm.ine.reverso);
          //vm.saveIneReverso();
        }

        vm.saveIneFrente = function(){
          vm.ine.anverso.base64 = vm.ine.anverso.base64.substring(22);
          localStorageService.set('ineAnverso', vm.ine.anverso);
        }

        vm.saveIneReverso = function(){
          vm.ine.reverso.base64 = vm.ine.reverso.base64.substring(22);
          localStorageService.set('ineReverso', vm.ine.reverso);
        }

        vm.savePasaporte = function(){
          vm.pasaporte.datos.base64 = vm.pasaporte.datos.base64.substring(22);
          localStorageService.set('pasaporte', vm.pasaporte.datos);
        }

        vm.cambioTexto = function(item, image){
          item = "Volver a seleccionar";
          switch(image){
            case 'anverso':
              vm.saveIneFrente();
              vm.ine.textoFrente = item;
              break;
            case 'reverso':
              vm.saveIneReverso();
              vm.ine.textoReverso = item;
              break;
            case 'pasaporte':
              vm.savePasaporte();
              vm.pasaporte.textoFrente = item;
              break;
            default:
              break;
          }
        }

        vm.disableIdButton = function(){
          var disable = true;
          switch(vm.documentoSeleccionado){
            case 'ine':
              if(vm.ine.anverso && vm.ine.reverso) disable = false;
              break;
            case 'pass':
              if(vm.pasaporte.datos) disable = false;
              break;
            default:
              disable = true;
              break;
          }
          return disable;
        }

        vm.prevalidarIdentificacion = function(){
          switch(vm.documentoSeleccionado){
            case 'ine':
              vm.validarIdentificacion(vm.ine.anverso, vm.ine.reverso);
              break;
            case 'pass':
              vm.validarIdentificacion(vm.pasaporte.datos, null);
              break;
            default:
              break;
          }
        }

        vm.validarIdentificacion = function(idFrente, idAtras) {
          connection.sendIdentification(idFrente, idAtras)
            .then(function(response){
              console.log(response);
              if(response.estatus === 0){
                config.nextStep(1);
              }else{
                let error = {
                  status: response.estatus,
                  descripcion: response.descripcion,
                  errorCode: response.codigo,
                  tipo: response.tipo,
                  titulo: response.titulo
                }
                config.generalErrors(error);
              }
            })

        }
        vm.captureOCR = function(){
          var modalOptions = {
            actionButtonText: 'Aceptar',
            closeButtonText: 'Cancelar',
            showBoth: true,
            headerText: 'Captura de imagen',
            modalIcon: 'images/icons/iconoInfo.png',
            bodyText: ' '
          };
          ocrCaptureService.showModal({},modalOptions);
        }
    }

    DocumentoController.$inject = ['$rootScope', '$scope', '$attrs', '$location', '$anchorScroll', 'connection', '$filter', 'modalService', '$q', 'imageProcessor', 'alertaService', '$crypto', 'localStorageService', '$state', "config", "baseConfig", 'ocrCaptureService'];

})();
