angular.module('mdo-angular-cryptography', [])
    .provider('$crypto', function CryptoKeyProvider() {

        function s(x) { return x.charCodeAt(0); }

        function hash(password, length){
          var hashed = CryptoJS.SHA256(password).toString();
          if(length < hashed.length) hashed = hashed.substring(0,length);
          return hashed;
        }
        var cryptoKey;

        this.setCryptographyKey = function(value) {
            this.cryptoKey = value;
        }

        this.$get = [function(){
            return {
                cryptoKey: this.cryptoKey,
                encrypt: function(message, key) {
                    if (key === undefined)
                        key = hash(this.cryptoKey, 32);
                    var iv = '9ff13b51db5353b3';
                    message = CryptoJS.enc.Utf8.parse(message);
                    key = CryptoJS.enc.Utf8.parse(key);
                    iv  = CryptoJS.enc.Utf8.parse(iv);
                    result = CryptoJS.AES.encrypt(message, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                    return result;
                },

                decrypt: function(message, key) {
                    if (key === undefined)
                        key = hash(this.cryptoKey, 32);
                    var iv = '9ff13b51db5353b3';
                    //message = CryptoJS.enc.Base64.parse(message);
                    key = CryptoJS.enc.Utf8.parse(key);
                    iv  = CryptoJS.enc.Utf8.parse(iv);
                    return CryptoJS.AES.decrypt(message, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }).toString(CryptoJS.enc.Utf8);
                }
            }
        }];
    });
