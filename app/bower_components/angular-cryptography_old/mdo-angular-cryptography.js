angular.module('mdo-angular-cryptography', [])
    .provider('$crypto', function CryptoKeyProvider() {
        var cryptoKey;

        this.setCryptographyKey = function(value) {
            this.cryptoKey = value;
        }

        function hash(password, length){
          // var keyBytes = [];
          // password = password.split('');
          // console.log(password);
          // for(var x = 0; x < password.length; x++){
          //   password[x] = password[x].charCodeAt();
          // }
          // console.log(password);
          var hashed = CryptoJS.SHA256(password).toString();

          // hashed = CryptoJS.enc.Base64.stringify(hashed);
          // console.log(hashed.length);
          if(length < hashed.length) hashed = hashed.substring(0,length);

          return hashed;
        }

        function hex2a(hexx) {
          var hex = hexx.toString();//force conversion
          var str = '';
          for (var i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
          return str;
        }

        function vector(){
          //iv = CryptoJS.enc.Hex.parse('9ff13b51db5353b3');
          iv = '9ff13b51db5353b3';
          //iv = iv.split('');
          //for(var x = 0; x < iv.length; x++) iv[x] = iv[x].charCodeAt();
          return iv;
        }

        this.$get = [function(){
            return {
                cryptoKey: this.cryptoKey,

                encrypt: function(message, key) {
                  if (key === undefined) key = this.cryptoKey;
                  var key = hash(key, 32);
                  var iv = vector();
                  var encrypted = CryptoJS.AES.encrypt(message, key, { iv: iv, mode: CryptoJS.mode.CBC, keySize: 256/32, padding: CryptoJS.pad.Pkcs7 });
                  return encrypted.ciphertext.toString();
                },

                decrypt: function(message, key) {
                    if (key === undefined) key = this.cryptoKey;
                    key = hash(key, 32);
                    var iv = vector();
                    var decrypted = CryptoJS.AES.decrypt(message, key, { iv: iv, mode: CryptoJS.mode.CBC, keySize: 256/32, padding: CryptoJS.pad.Pkcs7 });
                        console.log(decrypted.words.toString(CryptoJS.enc.Utf8));
                    return hex2a(decrypted);
                }
            }
        }];
    });
