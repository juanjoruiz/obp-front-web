var gulp = require('gulp');
var sass = require('gulp-sass');
var rsass = require('gulp-ruby-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var bower = require('gulp-bower');
var notify = require('gulp-notify');
var del = require('del');
var cache = require('gulp-cache');
var runSequence = require('run-sequence');
var path = require('path');
var Server = require('karma').Server;
var embedTemplates = require('gulp-angular-embed-templates');
var obfuscate = require('gulp-obfuscate');
var gutil = require('gulp-util');
var babel = require('gulp-babel');
var versioning = require('gulp-version-number');


var config = {
  appPath : path.join(__dirname,'app'),
  sassPath : path.join(__dirname, 'app/scss'),
  bowerDir : path.join(__dirname,'app/bower_components')
}

var versionConfig = {
  'value': '%MDS%',
  'append': {
    'key': 'v',
    'to':['css', 'js']
  }
}

gulp.task('js:build', function () {
    gulp.src('pre/js/**/*.js')
        .pipe(embedTemplates({
          basePath: config.appPath
        }))
        .pipe(gulp.dest('pre/js'))
});

gulp.task('test', function (done) {
  return new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('lint', function() {
  gulp.src(['./app/js/**/*.js', '!./app/bower_components/**'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'))
    .on('error', notify.onError(function(error){
      return "JS Error: " + error.message;
    }))
});

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});

gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*')
        .pipe(gulp.dest('app/fonts'));
});

// sass to css task
gulp.task('sass', function(){
  return gulp.src('app/scss/**/*.scss')
    .pipe(sass({
      includePaths: [
        config.bowerDir + '/bootstrap-sass/assets/stylesheets',
        config.bowerDir + '/font-awesome/scss',
        config.bowerDir + '/angular-material'
      ]
    }).on('error', notify.onError(function(error){
      return 'Error: ' + error.message;
    })))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// watcher task
gulp.task('watch',['browserSync', 'sass'], function(){
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('browserSync', function() {
  browserSync.init({
    browser: 'default',
    server: {
      baseDir: 'app'
    },
  })
});

gulp.task('js-pass', function(){
  return gulp.src('app/js/**/*.js')
    .pipe(gulp.dest('dist/personas/js'));
});

gulp.task('todo-pre', function(){
  return gulp.src('app/**/**/**/*')
    .pipe(gulp.dest('pre'))
});


gulp.task('css-pass', function(){
  return gulp.src('app/css/**/*.css')
    .pipe(gulp.dest('pre/css'))
});

gulp.task('useref:before', function(){
  return gulp.src('app/*.html')
    .pipe(gulp.dest('pre'))
});

gulp.task('babel', function(){
  return gulp.src('app/js/**/**/*.js')
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('pre/js'))
});

gulp.task('useref', function(){
  return gulp.src('pre/*.html')
    .pipe(useref())
     .pipe(gulpIf('*.js', uglify({mangle:false}).on('error', gutil.log)))
     .pipe(gulpIf('*.css', cssnano()))
     .pipe(versioning(versionConfig))
    .pipe(gulp.dest('dist/personas'))
});

gulp.task('getpdf', function(){
  return gulp.src('app/images/**/*.pdf')
    .pipe(gulp.dest('dist/personas/images'))
});

gulp.task('images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin({
      interlaced: true
    })))
    .pipe(gulp.dest('dist/personas/images'))
});

gulp.task('img', function(){
  return gulp.src('app/img/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(gulp.dest('dist/personas/img'))
});


gulp.task('json', function(){
  return gulp.src('app/js/services/*.json')
    .pipe(gulp.dest('dist/personas/js/services/'));
});

gulp.task('fonts', function(){
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/personas/fonts'))
});

gulp.task('faWebfonts', function () {
  return gulp.src('app/bower_components/font-awesome/fonts/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest('app/fonts/'));
});

gulp.task('glyphFonts', function(){
  return gulp.src('app/bower_components/bootstrap-sass/assets/fonts/bootstrap/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest('app/fonts/bootstrap/'));
});

gulp.task('clean:pre', function(){
  return del.sync('pre');
});

gulp.task('clean:dist', function(){
  return del.sync('dist');
});

gulp.task('cache:clear', function(){
  return cache.clearAll(callback)
});

gulp.task('obfuscate', function () {
    return gulp.src('dist/js/*.js')
        .pipe(obfuscate())
        .pipe(gulp.dest('dist/js/'))
});

gulp.task('adrum', function(){
  return gulp.src('app/js/services/adrum.js')
        .pipe(gulp.dest('dist/personas/js/services/'))
});
gulp.task('icarVideo', function(){
  return gulp.src('app/js/icarSDK.js')
        .pipe(gulp.dest('dist/personas/js/'))
});
gulp.task('icar', function(){
  return gulp.src('app/js/icarDocumentCapture.js')
        .pipe(gulp.dest('dist/personas/js/'))
});
gulp.task('branch_ext_js', function(){
  return gulp.src('app/js/branch_main.min.js')
        .pipe(gulp.dest('dist/personas/js/'))
});
gulp.task('branch_ext_css', function(){
  return gulp.src('app/css/branch_styles.min.css')
        .pipe(gulp.dest('dist/personas/css/'))
});

gulp.task('pre:build', function(callback){
  runSequence('clean:pre', 'todo-pre',
  ['babel'],
  ['js:build'],
  callback
  )
});

gulp.task('build', function(callback){
  runSequence('clean:dist', 'sass', 'faWebfonts', 'glyphFonts',
    ['lint','useref', 'getpdf', 'images', 'fonts', 'json', 'adrum','icarVideo','icar'],
    ['img'],
    ['branch_ext_js','branch_ext_css'],
    callback
  )
});

gulp.task('start', function(callback){
  runSequence(['sass', 'browserSync', 'watch'], callback)
});

gulp.task('default', function(callback){
  runSequence(['pre:build'],['build'], callback)
});
