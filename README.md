# Aplicación de OnBoarding particulares

> Aplicación para creación de cuentas Universitarias, de Nómina y Mercado Abierto

Aplicación de Santander Lighthouse, creada por el equipo de On Boarding particulares.

La aplicación funciona con los siguientes frameworks:

* Angular 1.5.8
* Angular animate 1.5.8
* Angular UI Router 0.3.1
* Angular Material 1.0.9
* Angular local storage 0.3.0
* Angular UI Bootstrap 2.1.0
* MomentJS 2.15.1
* ComboDate 1.1.0
* Bootstrap (bootstrap-sass) 3.3.7
* Fontawesome 4.6.5
* CryptoLibJS 1.0.3

# Versión
## 2.0.0
La versión 2.0.0 contiene los siguientes cambios:

* Refactorización de la aplicación.
	* Separación de lógica por componentes.
	* Separación de controlador de las directivas.
		* Estructura de componente (Directiva, Controlador, Vista).
	* Separación de componentes del core de la aplicación.
* Nuevo set de pasos.
* Pasos dinámicos.
* Archivo de configuración de flujos (json).
* Nuevo mensaje en componente Branch locator cuando no hay sucursales a 2km.
* Actualización de página inicial "home" con las imagenes de cada producto.
* Bullets de contraseña dinámicos.
* Actualización de opciones para Nacionalidad D.F. a Ciudad de México.


## Introducción
Este documento cuenta con la información necesaria para ambientar el equipo de desarrollo, así como las características de la aplicación. Es importante seguir las instrucciones aquí mencionadas. 

>En caso de encontrar un error o una mejora favor de comunicarse con el Equipo de LightHouse On Boarding Particulares así como los involucrados en el desarrollo y mantenimiento del Front end de ésta aplicación.

## ¿Qué contiene este documento?

1. Pre requisitos.
2. Gulp - Tareas de Gulp.
3. Estructura de la aplicación.
4. Pruebas automáticas.
5. Estrategia de Branching.
6. Flujo de trabajo ( Dev-Pre-Pro ).
7. Liberación de artefacto ( Producción ).

# Pre requisitos

Para hacer uso de éste código y poder realizar mejoras y cambios sobre el mismo, es necesario contar con herramientas de ambiente previamente instaladas en la computadora. A continuación vamos a revisar que herramientas son necesarias y las versiones que fueron utilizadas.

## Ambientación de sistema

### Instalación de NodeJS y NPM
NodeJS y Node Package Manager o NPM nos serán útiles para la configuración y gestión de la aplicación, el motivo de usar NPM como gestor de paquetes es: tener todas las dependencias de ambiente listas para trabajar.

1. Instalación de [NodeJS](https://nodejs.org/es/) en el sistema.
2. Revisión de versión de NPM

```sh
$ npm -v
3.10.3
```
### Instalación de Gulp 
Gulp es un orquestrador de tareas que nos permitirá "compilar" nuestro código de trabajo en paquetes sencillos de deployment, las tareas las ocupará [Jenkins](https://jenkins.io/doc/) para el deployment en [NGINX](https://www.nginx.com/resources/wiki/)

```sh
$ sudo npm install -g gulp
```

```sh
$ gulp -v
[10:17:02] CLI version 3.9.1
[10:17:02] Local version 3.9.1
```

Para el uso de las tareas ya programadas de gulp, ver más adelante.

### Instalación de Bower para estilos y dependencias de JS
Bower es un gestor de paquetes que separará las dependencias de Frontend, de las dependencias de ambiente, las cuales gestionaremos con NPM.

```sh
$ sudo npm install -g bower
```

```sh
$ bower -v
1.8.0
```

## Instalación de la aplicación en desarrollo

#### Clonar el repositorio de GIT y entrar a la carpeta clonada:

```sh
$ cd obp-web-desktop
```
#### bajo la carpeta de trabajo revisar la existencia del archivo 'package.json' mediante el comando:

```sh
$ find package.json
package.json
```
#### realizar la instalación de los paquetes de la aplicación:

```sh
$ npm install
```
#### revisar la existencia del archivo 'bower.json' mediante el comando:

```sh
$ find bower.json
bower.json
```
#### realizar la instalación de los paquetes de bower para la aplicación:

```sh
$ bower install
```

# Gulp - Tareas de gulp
En la aplicación contamos con tareas generadas en gulpjs para orquestrar una serie de actividades manuales y disparar acciones como comprimir código e imagenes. 

A continuación se enlistan las tareas que están en el archivo y que función realizan:

#### Configuración de GULP
contamos con una variable inicial de configuración, donde ponemos las rutas a diferentes assets de la aplicación:

```js
var config = {
  appPath : path.join(__dirname,'app'),
  sassPath : path.join(__dirname, 'app/scss'),
  bowerDir : path.join(__dirname,'app/bower_components')
}
```
Esta variable cuenta con la información de acceso a las carpetas que usaremos en las tareas de gulp.

La primer variable
```
appPath
```
contiene la información "root" de la aplicación, es decir donde se encuentra todo el código de la aplicación. Después contamos con la variable
```
sassPath
```
la cual cuenta con la información de los archivos de estilo escritos en SASS. La variable 
```
bowerDir
```
contiene la ruta de acceso a las dependencias instaladas en la aplicación, como AngularJS y Bootstrap.

#### Tarea de construcción de JS templates
Dentro de gulp contamos con la tarea:
```
$ gulp js:build
```

Esta tarea tomará las directivas de javascript y leerá las referencias a las vistas escritas en HTML, tomará todo el HTML y lo integrará en la directiva de JS. Esto nos permite tener un Workspace limpio con la vista separada del controlador y posteriormente en un release que se comprima en un único archivo de directiva. El resultado es depositado bajo la carpeta de "pre".

#### Tarea de Errores de sintáxis JS
Dentro de gulp contamos con la tarea:
```
$ gulp lint
```

Esta tarea revisa la sintáxis de cada archivo de javascript creado por nosotros bajo la carpeta de 
```
app/js
```
si encuentra un error no permitirá terminar el proceso de reducción y compresión.

#### Tarea de iconografía
Dentro de gulp contamos con la tarea:
```
$ gulp icons
```

Esta tarea nos ayuda a recuperar los fonts de Fontawesome (descargados mediante bower) y cargarlos dentro del asset pipeline de nuestra aplicación, el cual sería 
```
app/fonts
```

#### Tarea de compilación de estilos
Dentro de gulp contamos con la tarea:
```
$ gulp sass
```

Esta tarea tomará todos los archivos dentro de 
```
app/scss
```
, 
```
app/bower_components/bootstrap-sass/assets/stylesheets
```
,
```
app/bower_components/font-awesome/scss
```
 y
```
app/bower_components/angular-material
```
. Realizará una revisión de la sintáxis de SASS y lo compilará en CSS depositandolo dentro de la carpeta 
```
app/css
```
#### Tarea de sincronización de navegadores
Dentro de gulp contamos con la tarea:
```
$ gulp browserSync
```

Esta tarea configura un micro servidor en la computadora, el cual permitirá correr la aplicación en diferentes navegadores además de sincronizar las vistas, es decir en caso de estar trabajando en dispositivos diferentes, cuando realicemos una acción en una de las pantallas en todos los navegadores que estemos trabajando se realizará la misma acción.


#### Tarea de cambios automáticos
Dentro de gulp contamos con la tarea:
```
$ gulp watch
```

Esta tarea está en espera constante de cambios dentro de los archivos de SASS, HTML y JS, en caso de existir un cambio en alguno de los archivos, <dentro del alcance del watcher> éste reiniciará el micro servidor configurado por browser sync.

